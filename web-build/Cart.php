<?php
class Cart extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Kolkata');
        // Your own constructor code
        $this->load->model('CartModel','cartmodel');
        $this->load->model('ProductModel','productmodel');
        // $this->load->model('UserModel','usermodel');
        $checkType = array(
            "add" => "POST",
            "delete" => "GET",
            "itemdelete" => "GET",
            "index" => "GET",
            "update" => "POST",
            "updateAccessoriesCart" => "POST",
            "getCartAmount"=>"GET"
        );
        checkType($checkType);
        
        $methodsArr=array(
            "add"=>ADD_CART,
            "index"=>LIST_CART,
            "delete"=>DELETE_CART,
            "update"=>UPDATE_CART,
            "itemdelete"=>DELETE_ACCESSORY_ITEM,
            "updateAccessoriesCart" => UPDATE_CART_ACCESSORIES,
            "getCartAmount"=>LIST_CART,
            
        );
        
        authGuard($methodsArr);
    }

    public function checkIsInstant($pid,$ptype=null)
    {
        $isInstant = $this->productmodel->checkIsInstantDeliver($pid,$ptype);
        if($isInstant[0]['is_instant_deliver']==1)
        {
            return true;
        }else
        {
            return false;
        }
    }

    public function checkValidSlot($instant,$delivry_date,$slot)
    {
        //12 min diffr
        // { value: '1', label: '8 PM - 10 PM'}, slot 1
        // { value: '2', label: '10 PM - 12 AM'}, slot 2
        // { value: '3', label: '1 PM - 4 PM' }, slot 3
        // { value: '4', label: '5 PM - 8 PM' }, slot 4
        // { value: '5', label: '9 PM - 12 AM' } slot 5

        $isValidSlot=false;
        $todayDate = date("Y-m-d");
       
        $currentTime = time();
        
        $todayDecl = strtotime($todayDate);
        $deliverDecl =  strtotime($delivry_date);
        //$currentTimeDecl = strtotime($currentTime);

        $fivepm ="17:00:00";
        $twelvepm ="12:00:00";
        
        // 1. Check is Yesterday
        if($deliverDecl < $todayDecl)
        {
            $isValidSlot=false;
        }

        //2. check today
        if($deliverDecl == $todayDecl)
        {
           
            // check can this deliver today
            if($instant)
            {
                  if(time() >= strtotime($fivepm))
                  {
                     $isValidSlot=false;
                  }else if( time() >= strtotime($twelvepm) && time() <= strtotime($fivepm))
                  {
                     if($slot==2)
                     {
                        $isValidSlot=true;
                     }else
                     {
                        $isValidSlot=false;
                     }  

                  }else if(time() <= strtotime($twelvepm))
                  {
                     if($slot==1 || $slot==2)
                     {
                        $isValidSlot=true;
                     }else
                     {
                        $isValidSlot=false;
                     }
                  }
                  
            }else
            {
                $isValidSlot=false;
            }
        }

        //2. check tomarrow
        $abc = date_create($delivry_date);
        $xyz = date_create($todayDate);
        $diff=date_diff($abc,$xyz);
        $daysdiff = $diff->format("%a");
    
        if(($deliverDecl > $todayDecl) && intval($daysdiff)==1)  //pakka tomarrow
        {
           
           if(time() <= strtotime($twelvepm)) 
           {
               if($slot==3 || $slot==4 ||  $slot==5)
               {
                  $isValidSlot=true;
               }else
               {
                  $isValidSlot=false;
               }
           }else if(time() >= strtotime($twelvepm) && time() <= strtotime($fivepm))
           {
                if($slot==4 || $slot==5)
                {
                   $isValidSlot=true;
                }else
                {
                   $isValidSlot=false;
                }
           }else if(time() >= strtotime($fivepm))
           {
                if($slot==1 || $slot==2)
                {
                    $isValidSlot=true;
                }
           }else
           {
                $isValidSlot=false;
           }

        }

        if($deliverDecl > $todayDecl && $daysdiff > 1)  //pakka tomarrow ke baad
        {
           
            $isValidSlot=true;
        }  
       
        return $isValidSlot;

    }

    public function getCartItems()
    {
        
        
        $id=$GLOBALS['USER_ID'];
        //$id=1;
       
        $list= $this->cartmodel->getList($_GET,$id);
        $newlist=array();
        if(isset($list) && !empty($list))
        {
            
            foreach ($list as $value) {

                $checkInstant = $this->checkIsInstant($value['product_id'],$value['product_type']);
                $isValidItem = $this->checkValidSlot($checkInstant,$value['deliver_date'],$value['slot']);
                
                if($isValidItem)
                {
                    $cart_id = $value['id'];
                    $accessorylist = $this->cartmodel->getCartAccessories($cart_id);
                    if(empty($value['product_id'])) //in case not valid product
                    continue;

                    $billable_value = calculateCakePrice($value['base_amount'],$value['discount_type'],$value['discount_amount'],
                    $value['available_in_type'],$value['available_in_shape'],$value['egg_type_id'],$value['shape_id'],
                    $value['eggless_extra_base_price'],$value['eggless_extra_discount_type'],$value['eggless_extra_discount_price'],
                    $value['extra_shape_basic_amount'],$value['extra_shape_discount_type'],$value['extra_shape_discount_amount']);

                    $value['billable_amount'] = $billable_value;
                    $value['quantity']=(int)$value['quantity'];
                    $value['weight_id']=(int)$value['weight_id'];
                    $value['egg_type_id']=(int)$value['egg_type_id'];
                    $value['shape_id']=(int)$value['shape_id'];
                    $value['message']=$value['message'];
                    $value['deliver_date']=$value['deliver_date'];
                    $value['slot']=(int)$value['slot'];
                    $value['name']=$value['name'];
                    $value['is_active']=(int)$value['is_active'];
                    $value['product_image']=$value['product_image'];
                    $value['accessories']= $accessorylist;
                    array_push($newlist,$value);
                }    
    
            }
        }
        
        return $newlist;
    }

    public function index()
    {
        $id=$GLOBALS['USER_ID'];
        //$id=1;
        if(empty($id)) {

            response([],404,"User not found");  
        }
       
        $list = $this->getCartItems();
        
        $cart = calculateOrderAmount($list);
        response($cart,200,"list fetch successfull",count($list));
    

    }

    public function getCartAmount()
    {
        // $id=$GLOBALS['USER_ID'];
        // response($id,200,"list fetch successfull");
        // //$id=1;
        // if(empty($id)) {
        //   response([],404,"User not found");  
        // }  
        $list = $this->getCartItems();
        $totalPrice = getCartTotal($list); 
        //$value['totalPrice'] = $totalPrice;
        response([$totalPrice],200,"list fetch successfull");

    }
    
    public function add() 
    {
        $id=$GLOBALS['USER_ID'];
        
        if(!empty($id) && !empty($_POST))
        {
            //$this->form_validation->set_rules('product_type', 'Name', 'trim|alpha|required');
            $this->form_validation->set_rules('product_id', 'Product Id', 'trim|required');
            $this->form_validation->set_rules('weight_id', 'Weight Id', 'trim|integer|required');
            $this->form_validation->set_rules('egg_type_id', 'Egg Type Id', 'trim|integer|required');
            $this->form_validation->set_rules('shape_id', 'Shape Id', 'trim|integer|required');
            $this->form_validation->set_rules('deliver_date', 'Deliver Date', 'trim|required');
            try{
            if ($this->form_validation->run() == TRUE) 
            {
                $getdata = $this->input->post();
                
                $login_id = $this->cartmodel->add_product($id,$this->input->post());
                $acc_array = $getdata['accessories'];
                if($login_id>0)
                {
                    if(!empty($acc_array))
                    {
                        foreach($acc_array as $val)
                        {
                            $assecory_detail[]=array(
                                'cart_id' => $login_id,
                                'accessory_id' => $val['accessory_id'],
                                'accessory_name' => $val['accessory_name'],
                                'quantity' => $val['quantity'],
                            );
                        
                        }
                        $resp = $this->cartmodel->addAccesoryCart($assecory_detail);
                        if($resp > 0)
                        {
                            response([],200,"Added successfully ");  
                        }else { 
                            response([],500,"Unable to add accessory");  
                        }
                    }
                    response([],200,"Added successfully");  
                    
                }else {

                    response([],500,"Unable to add");  

                }  
       
            } else {

                    $errors = $this->form_validation->error_array();
                    response([$errors],400,"Please provide proper information"); 

            }
        }catch (\Throwable $th) {
            response([$th->getMessage()],404,"error");  

         }


        } else if(empty($id)) {

            response([],404,"User not found");  


        } else {
            
            response([],400,"Please provide proper information"); 

        }
    }

    public function updateAccessoriesCart()
    {
        $id=$GLOBALS['USER_ID'];
        //$id=1;
       
        
        if(!empty($id) && !empty($_POST))
        {
            $this->form_validation->set_rules('cart_id', 'cart_id', 'trim|integer|required');
            $this->form_validation->set_rules('access_cart_id', 'access_cart_id', 'trim|integer|required');
            $this->form_validation->set_rules('access_id', 'access_id', 'trim|integer|required');
            $this->form_validation->set_rules('quantity', 'Quantity  ', 'trim|integer|required');
            
            $getdata = $this->input->post();
            $cart_id = $getdata['cart_id'];
            $access_id = $getdata['access_id'];
            $access_cart_id = $getdata['access_cart_id'];
            $new_quantity = $getdata['quantity'];

            $isValidCart=$this->cartmodel->checkValidCart($cart_id,$id);
            if(count($isValidCart) > 0)
            {
                $is_updated = $this->cartmodel->updateAccessoriesCartQuantity($cart_id,$access_cart_id,$access_id,$new_quantity);
                if($is_updated)
                {
                    $list = $this->getCartItems();
                    $cart = calculateOrderAmount($list);
                    response($cart,200,"updated successfully",count($list));
                }
                else
                {
                    response([],400,"You can not update accessory quantity");
                }
            }
            else
            {
                response([],400,"No valid cart");
            }
           
        }else
        {
            $errors = $this->form_validation->error_array();
            response([$errors],400,"Please provide proper information"); 
        }






    }

    public function update() 
    {
        $id=$GLOBALS['USER_ID'];
        //$id=1;
        if(!empty($id) && !empty($_POST))
        {
            $this->form_validation->set_rules('id', 'Paroduct cart  id ', 'trim|integer|required');
            $this->form_validation->set_rules('quantity', 'Quantity  ', 'trim|integer|required');


            if ($this->form_validation->run() == TRUE) 
            {
                $product_id = $this->input->post('id');
                $new_quantity = $this->input->post('quantity');
                
                $cart_arr = $this->userCartList($id);

                $can_update=$this->cartmodel->validate_product($id,$product_id,$new_quantity);
                if($can_update['success']==false)
                {
                    response([],403,$can_update['msg']);  
                }

                if(isset($can_update['status']) && $can_update['status'])
                {
                    response([$cart_arr['data']],200,"Updated successfully",$cart_arr['count']);
                }

                
                $login_id = $this->cartmodel->update_product($id,$this->input->post());

                if($login_id)
                {
                    $cart_arr = $this->userCartList($id);
                    response([$cart_arr['data']],200,"Updated successfully",$cart_arr['count']);    
                } else {

                    response([],500,"Unable to Update");  
               
                }   
            } else {

                    $errors = $this->form_validation->error_array();
                    response([$errors],400,"Please provide proper information"); 

            }


        } else if(empty($id)) {

            response([],404,"User not found");  


        } else {
            
            response([],400,"Please provide proper information"); 

        }
    }

    public function itemdelete()
    {
        $id=$GLOBALS['USER_ID'];
        //$id=1;

        if(!empty($id) && !empty($_GET))
        {
            $this->form_validation->set_rules('cart_id', 'Cart  id', 'trim|integer|required');
            $this->form_validation->set_rules('access_cart_id', 'Accessory Cart id', 'trim|integer|required');
            $this->form_validation->set_rules('access_id', 'Accessory id', 'trim|integer|required');

            if(empty($_GET['cart_id']) && empty($_GET['access_id'])) { 

                response([],400,"Please provide proper cart id & accessory id"); 
            }

            $deleteacc = $this->cartmodel->deleteCartAcce($_GET['cart_id'],$_GET['access_cart_id'],$_GET['access_id']);
            if($deleteacc > 0)
            {
                $list = $this->getCartItems();
                $cart = calculateOrderAmount($list);
                response($cart,200,"Deleted successfully",count($list));
            }else {

                response([],400,"Unable to Delete");  

            } 
            
        }else if(empty($id)) {

            response([],404,"User not found");  
        } else {
            
            response([],400,"Please provide proper information"); 

        }

    }

    public function delete() 
    {
      
        $id=$GLOBALS['USER_ID'];
        //$id=1;

        if(!empty($id) && !empty($_GET))
        {

            if(empty($_GET['cart_id'])) { 

                response([],400,"Please provide id"); 

            }
            $login_id = $this->cartmodel->deleteCart($id,$_GET['cart_id']);
            if($login_id)
            {
                $deleteacc = $this->cartmodel->deleteCartAcce($_GET['cart_id'],null);
                $list = $this->getCartItems();
                $cart = calculateOrderAmount($list);
                response($cart,200,"Deleted successfully",count($list));

                //$list = $this->getCartItems();
                //response($list,200,"Deleted successfully ",count($list));  
                
            } else {

                response([],400,"Unable to Delete");  

            }   

        } else if(empty($id)) {

            response([],404,"User not found");  


        } else {
            
            response([],400,"Please provide proper information"); 

        }
    }
  
    public function userCartList($id){

        $data_arr=array();
        $list= $this->cartmodel->getList($_GET,$id);
        $newlist=array();
        foreach ($list as &$value) {

            if(empty($value['product_id'])) //in case not valid product
            continue;

            $value['stock']=(int)$value['stock'];
            $value['price']=(double) $value['price'];
            $value['stock_alert_at']=(int) $value['stock_alert_at'];
            $value['discount']=(double) $value['discount'];
            $value['discount_type']=(int) $value['discount_type'];
            $value['quantity']=(int) $value['quantity'];
            
            $value['product_type']=PRODUCT_TYPE[$value['product_type']];

            $value['cover_image']=$value['cover_image'];
            try {
                $value['images']= json_decode($value['images']);
            }
            catch(Exception $e) {

                $value['images']= [];
               
                }
                $value['images']=$value['images']?$value['images']:[];
                array_push($newlist,$value);

        }
        $count= $this->cartmodel->getListCount($_GET,$id); 
        $data_arr['data'] =$newlist; 
        $data_arr['count'] =$count; 
        return $data_arr;
    
    }

}
?>