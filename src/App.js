import React, { Component } from 'react';
import { View, StyleSheet, Text, Platform } from 'react-native';
import { Router, Switch, Route, Link } from '../src/Route/routing';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Home from './Components/Home';
import CakeListing from './Components/CakeListing';
import CakeInfo from './Components/CakeInfo';
import NotFound from './Components/NotFound';
import Cart from './Components/Cart';
import PlaceOrder from './Components/PlaceOrder';
import Orders from './Components/Orders';
import Profile from './Components/Profile';
import SavedAddress from './Components/SavedAddress';
import AddAddress from './Components/AddAddress';
import EditAddress from './Components/EditAddress';
import PrivateRoute from '../src/Route/PrivateRoute';
import PublicRoute from '../src/Route/PublicRoute';
import Signup from './Components/Signup';
import Login from './Components/Login';
import ProfileEdit from './Components/ProfileEdit';
import GridExample from './Components/GridExmaple';
import ModalExample from './Components/ModalExample';
import ProductInformation from './Components/ProductInformation';
import AccordianExample from './Components/AccordianExample';
import AccessoriesList from './Components/AccessoriesList';
import Checkout from './Components/Checkout';
import Test from './Components/Test';
import { Provider, useDispatch, useSelector } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from "./store/store";
import TermsOfUse from './Components/TermsOfUse';
import PrivacyPolicy from './Components/PrivacyPolicy';
import Cupcake from './Components/Cupcake';
import ExpressDeliver from './Components/ExpressDeliver';
import ContactUs from './Components/ContactUs';
import Faq from './Components/Faq';
import AboutUS from './Components/AboutUs';

const isWeb = Platform.OS === 'web';
class App extends Component {
  
  render() {
    return (
      <Provider store={store}>
         
        <PersistGate loading={null} persistor={persistor}>
           <View style={styles.container}>
            <Router>
              <Switch>
              <Route exact path="/test" render={(props) => <Test />} />
                <Route exact path="/" render={(props) => <Home />} />
                <Route path="/cakelisting" component={CakeListing} />
                <Route path="/expressdelivery" component={ExpressDeliver} />
                {/* <Route path="/cakeinfo/:cakeurl" render={(props) => <CakeInfo />} /> */}
                <Route path="/productinformation/:cakeurl" component={ProductInformation}/>
                {/* <Route path="/cart" render={(props) => <ModalExample />} /> */}
                <PrivateRoute path="/cart" component={Cart} />
                <PrivateRoute path="/placeorder" component={PlaceOrder} />
                <PrivateRoute path="/orders" component={Orders} />
                <PrivateRoute path="/profile" component={Profile} />
                <PrivateRoute path="/edit/profile" component={ProfileEdit} />
                <PrivateRoute path="/addresses" component={SavedAddress} />
                <PrivateRoute path="/addaddress" component={AddAddress} />                
                <PrivateRoute
                  path="/edit/address/:addressid"
                  component={EditAddress}
                />
                <PublicRoute path="/login" component={Login} />
                <PublicRoute path="/signup" component={Signup} />
                <PublicRoute path="/grid" component={GridExample} />
                <Route path="/accordainexp" component={AccordianExample} />
                <Route path="/accessorieslist" component={AccessoriesList} />
                <PublicRoute path="/checkout" component={Checkout} />
                <Route path="/termsofuse" component={TermsOfUse} />
                <Route path="/privacypolicy" component={PrivacyPolicy} />
                <Route path="/cupcake" component={Cupcake} />
                <Route path="/contactus" component={ContactUs} />
                <Route path="/aboutus" component={AboutUS} />
                <Route path="/faq" component={Faq} />
                <Route render={(props) => <NotFound  
                   heading={'We couldn’t find any matches!'}
                   imgsrc={'https://i1.wp.com/saedx.com/blog/wp-content/uploads/2019/01/saedx-blog-featured-70.jpg?fit=1200%2C500&ssl=1'}
                   passurl={'/'}

                  />} />
              </Switch>
            </Router>
          </View>
      </PersistGate>
   
      </Provider>
    );
  }
}

const base = {

  container: {
    flex: 1,
    //backgroundColor: '#f9f9f9',
    backgroundColor: 'rgb(255, 255, 255)',
    marginTop: 0,
    //backgroundColor: '#f9f9f9',
    // backgroundColor: 'white',
    // marginTop: 25,
    // padding: 10,
  },
  
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {

      container: {
        marginTop: isWeb?0:25,
      },
     
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      container: {
        marginTop: isWeb?0:25,
      },
    })
  )
);

export default App;
