import { createBottomTabNavigator} from 'react-navigation-tabs';
import * as React from 'react';
import Home from "../Components/Home";
import CakeListing from "../Components/CakeListing";
import {createAppContainer} from 'react-navigation';
import {createBrowserApp} from '@react-navigation/web';
import {Platform} from "react-native";
import { FontAwesome } from '@expo/vector-icons'; 

const isWeb = Platform.OS === 'web';

const BottomTabNavigaton = createBottomTabNavigator(
    {
        Home: Home,
        listing: CakeListing,
    },
    {
        defaultNavigationOptions: ({ navigation }) => ({
          tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state;
            console.log(routeName);
            let iconName;
            if (routeName === 'Home') {
              iconName = focused
                ? 'home'
                : 'home';
            } else if (routeName === 'listing') {
              iconName = focused ? 'birthday-cake' : 'birthday-cake';
            }
            return <FontAwesome name={iconName} size={25} color={tintColor} />
          },
        }),
        tabBarOptions: {
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        },
      }
  );

  const container = isWeb ? createBrowserApp(BottomTabNavigaton): createAppContainer(BottomTabNavigaton);

  export default container