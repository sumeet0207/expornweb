import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import Home from '../Components/Home';
import Listing from '../Components/CakeListing';
import Cart from '../Components/Cart';
import Profile from '../Components/Profile';
import ProductInformation from '../Components/ProductInformation';
import { createBrowserApp } from '@react-navigation/web';
import { Platform } from 'react-native';

const isWeb = Platform.OS === 'web';

const StackNavigation = createStackNavigator(
  {
    Home: Home,
    Listing: Listing,
  },
  {
    navigationOptions: {
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: '#000',
      },
    },
  }
);

const container = isWeb
  ? createBrowserApp(StackNavigation)
  : createAppContainer(StackNavigation);

export default container;
