export {
    NativeRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useLocation ,
    useHistory 
  } from 'react-router-native';


  