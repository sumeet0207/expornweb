import React from 'react';
import { View, Text, StyleSheet, Button, Platform } from 'react-native';
import { Link } from '@react-navigation/web';
const isWeb = Platform.OS === 'web';

export default class Cake extends React.Component {
  static path = 'listing';
  render() {
    return (
      <View style={styles.container}>
        <Text>This Is Cake listing page</Text>
        {!isWeb ? (
          <Button
            title="Cake Info"
            onPress={() => this.props.navigation.navigate('cakeinfo')}
          />
        ) : (
          <Link routeName="cakeinfo">View cake detail</Link>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
