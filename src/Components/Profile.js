import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View,ScrollView,TouchableHighlight,TouchableOpacity,Image, 
  SafeAreaView,BackHandler, Platform } from 'react-native';
import ProfileBox from './atom/ProfileBox';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth,maxWidth } from 'react-native-media-queries';
import { useHistory,Link } from "../Route/routing";
import { getAsyncStorageFunction } from "./services/CookieModule";
import { GetRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import { LogoutFunction } from "./services/LogoutModule";
import { useShow } from "./services/AlertHook";
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { setCartCount } from "../store/action/cartAction";
import { useDispatch, useSelector } from 'react-redux';
import { setProfileName } from "../store/action/profileAction";
import * as GoogleSignIn from 'expo-google-sign-in';

const isWeb = Platform.OS === 'web';

const theme = {
  smallFont:12,
  middleFont:12,
  largeFont:15,
  backColor:'white',
  primaryFontColor:"white",
  primaryRed:"#FF1E1E",
}

export default function Profile() {
  const history = useHistory();
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [contact, setContact] = useState('');
  const { show, error, success } = useShow();
  const dispatch = useDispatch();

  useEffect(() => {
    getBasicInformation();
    handleBackHnadler();
  },[]);

  let profileSrc='https://img.icons8.com/color/452/user-female-circle.png';
  var imgsrc = 'https://www.jing.fm/clipimg/full/345-3451071_delivery-icon-clip-art-delivery-order-icon-png.png';
  var loginsrc = 'https://cdn0.iconfinder.com/data/icons/security-double-color-red-and-black-vol-2/52/lock__safety__security__secure-512.png';
  var locationsrc = 'https://www.starpng.com/public/uploads/preview/red-png-location-big-image-png-101576854749s3tlyyu9lh.png';

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }

  const signOutAsync = async () => {
            await GoogleSignIn.signOutAsync();
                setGoogleLogin(false)
  };
  
  function logoutNow()
  {
    let logout = LogoutFunction(history);
    dispatch(setCartCount(0));
    dispatch(setProfileName({}));
    signOutAsync();
  }

  async function getBasicInformation()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    if(mixdelight_token)
    {
      let basicInfo = null;
      let header = { Token: mixdelight_token };
      let basicInfoUrl = getApiurl("loginBasicInfo");
      let getInfo = await GetRequestFunction(basicInfoUrl, header, {});
      if(getInfo.status)
      {
        basicInfo = getInfo.data[0];
        let fullname = `${basicInfo.fname} ${basicInfo.lname}`;
        setName(fullname);
        setEmail(basicInfo.email_id);
        setContact(basicInfo.mobile_no);

      }
    
    }else
    {
      history.push('/signup');
    }  
  }

    return (
      <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View>
            <View style={{width:'100%',backgroundColor:'black',height:220,paddingTop:30}}>
                <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}}  onPress={()=>history.push('/edit/profile')}>
                  <Image source={{uri: profileSrc}} style={{width:110,height:110,borderRadius:100}}/>
                  <Text style={{fontSize:15,fontWeight:'bold',padding:10,color:'#faf7c7'}}>{name}
                  </Text>
                    <Text style={{fontSize:12,fontWeight:'bold',color:'#faf7c7'}}>{contact > 0?contact:''}</Text>
                    <Text style={{fontSize:12,fontWeight:'bold',color:'#faf7c7'}}>{email}</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.myaccountBox}>
                  <Row>
                    <Col xs={12} sm={12} md={4} lg={4}>
                      <ProfileBox title="Your Order" subtitle="Track or buy things again" image={imgsrc} redirectTo="/orders"/>
                    </Col>
                    <Col xs={12} sm={12} md={4} lg={4}>
                      <ProfileBox title="Login & Security" subtitle="Edit login, name and mobile number " image={loginsrc} redirectTo="/edit/profile"/>
                    </Col>
                    <Col xs={12} sm={12} md={4} lg={4}>
                      <ProfileBox title="Your Address" subtitle="Edit address for orders" image={locationsrc} redirectTo="/addresses"/>
                    </Col>
                  </Row>
                  <View style={[styles.doCenter]}>
                    <TouchableOpacity 
                      style={[styles.cabinetButton,styles.cabinetBack]} onPress={logoutNow}>
                      <Text style={[styles.cabinetButtonText,styles.callnowtext]}>
                        LOGOUT
                      </Text>
                    </TouchableOpacity>
                  </View>
            </View>
          </View>
        </ScrollView>
      </View>
        <Footer />
      <MobileFooter />
      </View> 
      );
  }

  const base = {
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    myaccountBox:{
      padding:15,
      marginLeft:50,
      marginRight:50,
    
    },
    cabinetButton:{
      alignItems:'center',
      marginTop:20,
      marginBottom:40,
      height:50,
      width:"40%",
      paddingLeft:15,
      paddingVertical:15,
      justifyContent:'center'
  },
    doCenter:{
      alignItems:'center',
    },
    cabinetButtonText: {
    fontSize: 15,
    fontWeight:"bold",
    justifyContent:"center",
    color:"white"
  },
  cabinetBack:{
      backgroundColor: theme.primaryRed,
    },
    cabinetButtonDis:{
      display:"none"
    },
    pageArea :{
      flexGrow: 1,
      zIndex:-15,
      marginVertical: isWeb? 0:60,
    }
  };

  const styles = createStyles(
    base,
    minWidth(100, maxWidth(1024, {
        myaccountBox: {
          marginLeft:0,
          marginRight:0
      },
      cabinetButton:{
          display:'flex',
          width:"100%",
      }
    }))
  );
