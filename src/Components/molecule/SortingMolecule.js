import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform
} from 'react-native';
import { theme } from '../../Theme/theme';
import { Octicons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useDispatch, useSelector } from 'react-redux';
import { setSortingOption } from "../../store/action/sortAction";
import CloseModal from '../atom/CloseModal';
const { width, height } = Dimensions.get('window');

const isWeb = Platform.OS === 'web';

const getStyle = (props) => {
  const styles = StyleSheet.create({
    sortyByTxt:{
      fontSize: theme.size16,
      color: theme.darkbg,
      fontFamily: 'MontserratSemiBold',
      paddingLeft: 20,
      paddingRight:10,
      paddingVertical: 12,
      // borderBottomWidth: 0.5,
      // borderColor: '#ccc'
    },
    sortItem :{
      flexDirection: 'row',
      alignItems: 'center',
      paddingVertical: 10,
      paddingHorizontal:15,
    },
    sortTxt :{
      fontSize: theme.size14,
      color: theme.darkgrey,
      fontFamily: 'MontserratRegular',
      paddingHorizontal: 25
    },
    sortHightlightTxt :{
      fontSize: theme.size14,
      color: theme.orange,
      fontFamily: 'MontserratRegular',
      paddingHorizontal: 25
    },
    borderClass :{
      borderBottomWidth: 0.5,
      borderColor: '#ccc',
      width:'100%'
    },
    imgstyle: {
      height: '100%',
      width: '100%'
    },
     imgWrap: {
      height: 20, 
      right:  5,
      top: 5,
      width: 20,
      position: 'absolute',
      alignItems:'center'
    },
    
  });
  return styles;
};

const SortingMolecule = (props) => {
  const styles = getStyle(props);
  const dispatch = useDispatch();
  const reduxSort = useSelector(state => state.sortReducer.sortOpt)

  function setSortingcake(getsort)
  { 
    props.getSortingData(getsort);
    dispatch(setSortingOption(getsort));
    props.closemodal();
  }

  return (
    <View>
      <View style={{display:'flex', justifyContent:'space-between', flexDirection:'row'}}>
        <Text style={styles.sortyByTxt}>SORT BY</Text>
        {/* {isWeb? <CloseModal closeTop={14} closeRight={12} onPress={()=>setSortingcake('cake_weight_price__base_amount___desc')} /> :
        <CloseModal closeTop={14} closeRight={12} onPress={()=>setSortingcake('cake_weight_price__base_amount___desc')} />
        } */}
        
      </View>
      <View style={styles.borderClass}></View>
      <View>

        <TouchableOpacity onPress={()=>setSortingcake('cake_weight_price__base_amount___desc')}>
          <View style={styles.sortItem}>
            <MaterialCommunityIcons name="sort-descending" size={24} 
             color={reduxSort=="cake_weight_price__base_amount___desc"?theme.orange:theme.darkgrey} />
            <Text style={styles.sortTxt}>Price: High to Low</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>setSortingcake('cake_weight_price__base_amount___asc')}>
          <View style={styles.sortItem}>
            <MaterialCommunityIcons name="sort-ascending" size={24} 
             color={reduxSort=="cake_weight_price__base_amount___asc"?theme.orange:theme.darkgrey} />
            <Text style={styles.sortTxt}>Price: Low to High</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>setSortingcake('rating_desc')}>
          <View style={styles.sortItem}>
            <Entypo name="star-outlined" size={24} 
             color={reduxSort=="rating_desc"?theme.orange:theme.darkgrey} />
            <Text style={styles.sortTxt}>Rating: High to Low</Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={()=>setSortingcake('rating_asc')}>
          <View style={styles.sortItem}>
            <Entypo name="star-outlined" size={24} 
             color={reduxSort=="rating_asc"?theme.orange:theme.darkgrey} />
            <Text style={styles.sortTxt}>Rating: Low to High</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SortingMolecule;
