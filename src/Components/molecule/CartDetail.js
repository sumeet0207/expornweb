import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { theme, setFontStyle } from '../../Theme/theme';
import SubmitButton from '../atom/SubmitButton';
import TitleText from '../atom/TitleText';
import CartTotal from '../atom/CartTotal';
import Price from '../atom/Price';
import {useParams,useHistory } from '../../Route/routing';

const { width, height } = Dimensions.get('window');

const CartDetail = (props) => {
  let history = useHistory();
  return (
    <View>
      <View style={styles.detailWrap}>
        {/* <View
          style={{
            backgroundColor: '#cccccc26',
            padding: 10,
            height: 70,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontSize: theme.size36 }}>30</Text>
            <View
              style={{
                flexDirection: 'column',
                paddingTop: 5,
                paddingLeft: 7,
              }}
            >
              <Text style={{ fontSize: theme.size16 }}>SEPTEMBER</Text>
              <Text style={{ fontSize: theme.size16 }}>SUNDAY</Text>
            </View>
          </View>

          <View>
            <View style={{ flexDirection: 'row', paddingTop: 5 }}>
              <TitleText
                title="Earliest Delivery"
                txtColor={theme.darkbg}
                titleAlign={'left'}
                titleSize={theme.size16}
              />
              <View style={{ paddingLeft: 10 }}>
                <Price
                  price={49}
                  titleSize={theme.size16}
                  mb_titleSize={theme.size16}
                  txtColor={theme.green}
                />
              </View>
            </View>
            <View>
              <Text style={{ fontSize: theme.size16 }}>14:00 - 18:00 Hrs</Text>
            </View>
          </View>
        </View> */}

        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 20,
            }}
          >
            <View>
              <TitleText
                title="CART TOTAL"
                txtColor={theme.darkbg}
                titleAlign={'left'}
              />
            </View>
            <View>
              <CartTotal total={props.cartAmount} />
            </View>
          </View>
        </View>

        <View>
         <SubmitButton btnTxt="Proceed to Checkout" onPress={()=>history.push('/checkout')}/>
        
        </View>

        {/* <View style={{ marginTop: 30 }}>
          <TitleText title="Explore more option" txtColor={theme.darkbg} />

          <SubmitButton
            btnTxt="Continue Shopping"
            Colorbg={theme.lightergrey}
            subBtncolor={theme.darkbg}
            sbtnWidth="60%"
            sbtntxtSiz={theme.size13}
          />
        </View> */}

      </View>
    </View>
  );
};

const base = {
  detailWrap: {
    minHeight: 150,
    backgroundColor: theme.white,
    borderColor: theme.lightgrey,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 20,
    // backgroundColor: theme.white,
    // borderColor: theme.lightgrey,
    // elevation: 8,
    // shadowOffset: { width: 1, height: 1 },
    // shadowOpacity: 0.3,
    // shadowRadius: 3,
    // borderRadius: 5,
    // paddingHorizontal: 35,
    // paddingVertical: 20,
    // marginHorizontal: 5,
    // marginBottom: 20,
  },
  totalClass: {
    fontSize: theme.size14,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      detailWrap: {
        minHeight: 135,
        paddingHorizontal: 25,
        paddingVertical: 10,
      },
    })
  )
);

export default CartDetail;
