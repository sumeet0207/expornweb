import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import ProductImg from '../atom/ProductImg';
import { FontAwesome } from '@expo/vector-icons';
import ProductName from '../atom/ProductName';
import AmountDisc from '../atom/AmountDisc';
import Price from '../atom/Price';
import MainItemMolecule from './MainItemMolecule';
import ExtraItemMolecule from './ExtraItemMolecule';
import { GetRequestFunction } from "../../api/ApiHelper";
import { getApiurl } from "../../api/ApiKeys";
import {getAsyncStorageFunction} from "../services/CookieModule";
import { setCartCount } from "../../store/action/cartAction";
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { mix_delight_const } from '../constant';
const { width, height } = Dimensions.get('window');
import { useShow } from "../services/AlertHook";

const CartItemMolecule = (props) => {
  const {product,setCartCake,setorderAmount,setlocalCartCount,cart_id,accessories} = props;
  //console.log(product)
  //console.log(mix_delight_const.slot[1])
  const dispatch = useDispatch();
  const { show, error, success } = useShow();
  const [date, setDate] = useState('0');
  const [month, setMonth] = useState('');
  const [day, setDay] = useState('');
  const [showSlot, setSlot] = useState('');

  useEffect(() => {
    getSlotInfo();
  },[]);

 

  async function getSlotInfo()
  {
    let deliver_date = product.deliver_date;
    let slot = product.slot;
    var d = moment(deliver_date);
    setDate(d.format('DD'));
    setMonth(d.format('MMMM'));
    setDay(d.format('dddd'));
    setSlot(mix_delight_const.slot[slot]);
  }

  async function deleteCartCake()
  {
    
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    //console.log(mixdelight_token)
    if(mixdelight_token)
    {
      let header = { Token: mixdelight_token };
      let param = {  cart_id: parseInt(product.id)}
      
      let cartDeleteUrl = getApiurl("cartDelete");
      let updatecart = await GetRequestFunction(cartDeleteUrl,header, param);
      //console.log(updatecart);
      if(updatecart.status)
      {
        let data = updatecart.data;
        setCartData(updatecart.count,data.orderProduct,data.calculated_amount);
      }else
      {
        error(updatecart.message)
      }

    }
  }

  function setCartData(count,orderProduct,calculated_amount)
  {
    dispatch(setCartCount(count));
    setlocalCartCount(count)
    setCartCake(orderProduct);
    setorderAmount(calculated_amount);
  }

  return (
    <View style={styles.cartWrap}>
      <View>
     <MainItemMolecule cake={product} deleteCartCake={deleteCartCake}/>
      </View>

      <View>
      {/* {product.accessory.map((item) => {
          <ExtraItemMolecule />
      })} */}


        <FlatList
          data={product.accessory?product.accessory:[]}
          scrollEnabled={false}
          initialNumToRender={10}
          numColumns={1}
          keyExtractor={item => item.id}
          renderItem={({ item, index, separators }) => (
            <ExtraItemMolecule 
              accessories={item} 
              setCartCake={setCartCake}
              setorderAmount={setorderAmount}
              setlocalCartCount={setlocalCartCount}
              cart_id={product.id}
              />
          )}
        /> 
         
      </View>

      {/* Delivery Date start */}
      <View>
        <View
          style={{
            backgroundColor: theme.lightergrey,
            padding: 10,
            height: 70,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
         <View style={{ flexDirection: 'row' }}>
          <Text style={{ fontSize: theme.size36, color:theme.ogBlack }}>{date}</Text>
            <View
              style={{
                flexDirection: 'column',
                paddingTop: 5,
                paddingLeft: 7,
              }}
            >
            <Text style={{ fontSize: theme.size16,color:theme.ogBlack }}>{month}</Text>
            <Text style={{ fontSize: theme.size16, color:theme.ogBlack }}>{day}</Text>
            </View>
          </View>

          <View style={{ alignItems: 'flex-end' }}>
            <View style={{ flexDirection: 'row' }}>
              <TitleText
                title="Delivery Slot"
                txtColor={theme.ogBlack}
                titleAlign={'left'}
                titleSize={theme.size16}
                titleBotm={1}
              />
            </View>  
            <View>
            <Text style={{ fontSize: theme.size16, color:theme.ogBlack }}>{showSlot}</Text>
            </View>
          </View>
        </View>
      </View>
      {/* Delivery Date End */}
    </View>
  );
};

const base = {
  cartWrap: {
    minHeight: 200,
    backgroundColor: theme.white,
    borderColor: theme.lightgrey,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginBottom: 20,
  },
};

const styles = createStyles(base, minWidth(100, maxWidth(991, {})));

export default CartItemMolecule;
