import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import CartTotal from '../atom/CartTotal';
import Price from '../atom/Price';

const { width, height } = Dimensions.get('window');

const OrderTablesMolecule = (props) => {
  const { isTableShow = false,cakeAmount=0,accessoryAmount=0,billingAmount=0 } = props;
  // console.log(cakeAmount)
  return (
    <View>
      {isTableShow && (
        <View style={{ marginVertical: 7 }}>
          <Row>
            <Col xs={6} sm={6} md={4} lg={4} xl={4}>
              <View style={{ alignItems: 'flex-start' }}>
                <TitleText
                  title={'CAKE SUMMARY'}
                  titleAlign={'left'}
                  titleSize={theme.size13}
                  mb_titleSize={theme.size13}
                  titleFont={'MontserratSemiBold'}
                />
                <View
                  style={{
                    flexDirection: 'row',
                  }}
                >
                  {/* <View>
                    <Text style={styles.tbtxt}>Cake</Text>
                  </View>
                  <View>
                    <Text style={[styles.tbtxt, { paddingLeft: 20 }]}>
                      ₹450
                    </Text>
                  </View> */}

                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}
                >
                  <View>
                    <Text
                      style={[
                        styles.tbtxt,
                        { fontFamily: 'MontserratSemiBold' },
                      ]}
                    >
                      Total
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={[
                        styles.tbtxt,
                        { paddingLeft: 20, fontFamily: 'MontserratSemiBold' },
                      ]}
                    >
                      {`₹${cakeAmount}`}
                    </Text>
                  </View>
                </View>
              </View>
            </Col>
            <Col xs={6} sm={6} md={4} lg={4} xl={4}>
              <View style={{ alignItems: 'flex-start' }}>
                <TitleText
                  title={'EXTRA ITEMS SUMMARY'}
                  titleAlign={'left'}
                  titleSize={theme.size13}
                  mb_titleSize={theme.size13}
                  titleFont={'MontserratSemiBold'}
                />
                <View
                  style={{
                    flexDirection: 'row',
                  }}
                >
                  {/* <View>
                    <Text style={styles.tbtxt}>Cake</Text>
                  </View>
                  <View>
                    <Text style={[styles.tbtxt, { paddingLeft: 20 }]}>
                      ₹450
                    </Text>
                  </View> */}

                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}
                >
                  <View>
                    <Text
                      style={[
                        styles.tbtxt,
                        { fontFamily: 'MontserratSemiBold' },
                      ]}
                    >
                      Total
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={[
                        styles.tbtxt,
                        { paddingLeft: 20, fontFamily: 'MontserratSemiBold' },
                      ]}
                    >
                      {`₹${accessoryAmount}`}
                    </Text>
                  </View>
                </View>
              </View>
            </Col>
            <Col xs={6} sm={6} md={4} lg={4} xl={4}>
              <View style={[styles.mb_summry, { alignItems: 'flex-start' }]}>
                <TitleText
                  title={'TOTAL ORDER SUMMARY'}
                  titleAlign={'left'}
                  titleSize={theme.size13}
                  mb_titleSize={theme.size13}
                  titleFont={'MontserratSemiBold'}
                />
                <View
                  style={{
                    flexDirection: 'row',
                  }}
                >
                  {/* <View>
                    <Text style={styles.tbtxt}>Cake</Text>
                  </View>
                  <View>
                    <Text style={[styles.tbtxt, { paddingLeft: 20 }]}>
                      ₹450
                    </Text>
                  </View> */}

                </View>
                <View
                  style={{
                    flexDirection: 'row',
                  }}
                >
                  <View>
                    <Text
                      style={[
                        styles.tbtxt,
                        { fontFamily: 'MontserratSemiBold' },
                      ]}
                    >
                      Total
                    </Text>
                  </View>
                  <View>
                    <Text
                      style={[
                        styles.tbtxt,
                        { paddingLeft: 20, fontFamily: 'MontserratSemiBold' },
                      ]}
                    >
                       {`₹${billingAmount}`}
                    </Text>
                  </View>
                </View>
              </View>
            </Col>
          </Row>
        </View>
      )}
    </View>
  );
};

const base = {
  tbtxt: {
    fontSize: theme.size12,
    color: theme.darkbg,
    fontFamily: 'MontserratRegular',
  },
  borderBotm100blck: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
    marginTop: 5,
  },
  mb_summry: {
    marginTop: 0,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      detailWrap: {
        minHeight: 135,
        paddingHorizontal: 25,
        paddingVertical: 10,
      },
      tbtxt: {
        fontSize: theme.size11,
      },
      mb_summry: {
        marginTop: 15,
      },
    })
  )
);

export default OrderTablesMolecule;
