import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import AmountDisc from '../atom/AmountDisc';
import ProductImg from '../atom/ProductImg';
import { FontAwesome } from '@expo/vector-icons';
import ProductName from '../atom/ProductName';
import OrderNumber from '../atom/OrderNumber';
import Price from '../atom/Price';
import MoreItemsMolecule from '../molecule/MoreItemsMolecule';
import OrderTablesMolecule from '../molecule/OrderTablesMolecule';

const { width, height } = Dimensions.get('window');

const OrderItemMolecule = (props) => {
  const { heading, desc, name, rdate,orderinfo } = props;
  const [isDetails, setIsDetails] = React.useState(false);
  const [isItems, setIsItems] = React.useState(false);

  return (
    <View>
      <View>
        <OrderNumber orderno={orderinfo.order_token || ''} />
        <View style={styles.borderBotm100blck}></View>
      </View>
      <View style={{ flexDirection: 'row', marginBottom: 5 }}>
        <View style={styles.pimg}>
          <ProductImg ImgHeight={100} mb_ImgHeight={70} />
        </View>
        <View style={styles.leftPdng}>
          <ProductName
            pname="Pink Roses with Cake"
            pnameSize={theme.size16}
            mb_pnameSize={theme.size13}
            pnameBotm={1}
            pnameAlign={'left'}
          />
          <View>
            <Text style={styles.normalText}>
              Qty: 1 | Shape : Round | Type : Eggless
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <View>
              <Price
                price={1100}
                titleAlign={'left'}
                titleSize={theme.size15}
                mb_titleSize={theme.size12}
                titleFont={'MontserratSemiBold'}
              />
            </View>
            <View style={{ paddingHorizontal: 10 }}>
              <AmountDisc
                amount={1299}
                disc={20}
                titleAlign={'flex-start'}
                amountSize={theme.size13}
                discSize={theme.size13}
                mb_amountSize={theme.size10}
                mb_discSize={theme.size10}
              />
            </View>
          </View>
          <View style={styles.pTop}>
            <Text style={styles.ordrStatus}>
              Delivered{' '}
              <Text style={{ fontFamily: 'MontserratRegular' }}>
                (Nov 22, 2019)
              </Text>
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.borderBotm100blck}></View>

      <TouchableOpacity
        onPress={() => {
          setIsItems(!isItems);
        }}
      >
        {isItems ? (
          <View style={styles.blueLink}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.blueLink}>More Items </Text>
              <FontAwesome
                name="angle-down"
                size={18}
                color={theme.blue}
                style={{ marginLeft: 5 }}
              />
            </View>
            <View>
              <MoreItemsMolecule isItemShow />
            </View>
          </View>
        ) : (
          <View style={styles.blueLink}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.blueLink}>More Items </Text>
              <FontAwesome
                name="angle-right"
                size={18}
                color={theme.blue}
                style={{ marginLeft: 5 }}
              />
            </View>
            <View>
              <MoreItemsMolecule />
            </View>
          </View>
        )}
      </TouchableOpacity>

      <View style={styles.borderBotm100blck}></View>
      <TouchableOpacity
        onPress={() => {
          setIsDetails(!isDetails);
        }}
      >
        {isDetails ? (
          <View style={styles.blueLink}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.blueLink}>Order Deatils </Text>
              <FontAwesome
                name="angle-down"
                size={18}
                color={theme.blue}
                style={{ marginLeft: 5 }}
              />
            </View>
            <View>
              <OrderTablesMolecule isTableShow />
            </View>
          </View>
        ) : (
          <View style={styles.blueLink}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.blueLink}>Order Deatils </Text>
              <FontAwesome
                name="angle-right"
                size={18}
                color={theme.blue}
                style={{ marginLeft: 5 }}
              />
            </View>
            <View>
              <OrderTablesMolecule />
            </View>
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

const base = {
  pimg: {
    width: 100,
  },
  cicon: {
    fontSize: 24,
  },
  borderBotm100blck: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
    marginTop: 5,
  },
  leftPdng: {
    paddingLeft: 20,
  },
  normalText: {
    fontSize: theme.size14,
    color: theme.Hash666,
    fontFamily: 'MontserratRegular',
  },
  ordrStatus: {
    fontSize: theme.size14,
    color: theme.darkbg,
    fontFamily: 'MontserratSemiBold',
  },
  pTop: {
    paddingTop: 10,
  },
  blueLink: {
    color: theme.blue,
    fontSize: theme.size14,
    fontFamily: 'MontserratRegular',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      pimg: {
        width: 70,
      },
      cicon: {
        fontSize: 15,
      },
      reviewWrap: {
        padding: 10,
      },
      leftPdng: {
        paddingLeft: 10,
      },
      pTop: {
        paddingTop: 2,
      },
      normalText: {
        fontSize: theme.size11,
      },
      ordrStatus: {
        fontSize: theme.size12,
      },
      blueLink: {
        fontSize: theme.size12,
      },
    })
  )
);

export default OrderItemMolecule;
