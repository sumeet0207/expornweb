import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  Modal
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link, useHistory } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import AmountDisc from '../atom/AmountDisc';
import ProductImg from '../atom/ProductImg';
import { FontAwesome } from '@expo/vector-icons';
import ProductName from '../atom/ProductName';
import OrderNumber from '../atom/OrderNumber';
import Price from '../atom/Price';
import MoreItemsMolecule from '../molecule/MoreItemsMolecule';
import OrderTablesMolecule from '../molecule/OrderTablesMolecule';
import WebModal from 'react-modal';
import { PostRequestFunction } from "../../api/ApiHelper";
import { getApiurl } from "../../api/ApiKeys";
import { getAsyncStorageFunction } from "../services/CookieModule";
import CancleOrderMolecule from './CancleOrderMolecule';
import moment from 'moment';
import CloseModal from '../atom/CloseModal';

const { width, height } = Dimensions.get('window');

const isWeb = Platform.OS === 'web';
isWeb?WebModal.setAppElement('#root'):null;

const OrderItemMolecule = (props) => {
  const history  = useHistory();
  const { heading, desc, name, rdate,orderinfo } = props;
  const [isDetails, setIsDetails] = React.useState(false);
  const [isItems, setIsItems] = React.useState(true);
  const [modalIsOpen,setIsOpen] = useState(false);
  const [canCancel,setCanCanel] = useState(true);

  useEffect(() => {
    checkOrderCancel();
   
  },[]);


  function checkOrderCancel()
  {
    let order_place_at = orderinfo.order_place_at;
    let order_status_int = orderinfo.order_status_int || 0;
    //console.log(order_place_at);
    let currentTime= moment();
    var a = moment(currentTime);
    var b = moment(order_place_at);
    //console.log(a.diff(b, 'hours'));
    //console.log(hourdiff)

    if(a.diff(b, 'hours')==0 && order_status_int===1)
    {
      setCanCanel(true);
    }
    else
    {
      setCanCanel(false);
    }

  }
  const customStyles = {
    content : {
      height:'60%',
      top                   : '2%',
      left                  : '2%',
      right                 : '2%',
      bottom                : '2%',
      padding:'1%'
    }
  };

  

  return (
    <View>

      <View>
        <View style={{flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems:'center'}}>
          <OrderNumber orderno={orderinfo.order_token || ''} />

          {canCancel?<TouchableOpacity style={styles.otpButtonContainer}
            onPress={()=>setIsOpen(true)}
            >
              <Text style={styles.HeadingTxt}> 
                  Cancel Order
              </Text>
          </TouchableOpacity>:null}
          </View>
        <View style={styles.borderBotm100blck}></View>
      </View>

      <View>
          {orderinfo.order_product_details.map((prodInfo) =>
           <View key={prodInfo.product_order_id}>

              <View style={{ flexDirection: 'row', marginBottom: 5 }}>
                <View style={styles.pimg}>
                <Link to={`/productinformation/${prodInfo.product_id}`}>

               
                  <ProductImg ImgHeight={100} mb_ImgHeight={70} 
                   imgsrc={prodInfo.image || ''}
                  />
                  </Link>
                </View>
                <View style={styles.leftPdng}>
                <TouchableOpacity onPress={()=>history.push(`/productinformation/${prodInfo.product_id}`)}>
                    <ProductName
                      pname={prodInfo.product_name || ''}
                      pnameSize={theme.size16}
                      mb_pnameSize={theme.size13}
                      pnameBotm={1}
                      pnameAlign={'left'}
                    />
                  </TouchableOpacity>
                  <View>
                    <Text style={styles.normalText}>
                      {`Weight: ${prodInfo.weight || 0} | Shape : ${prodInfo.shape || ''} | Type : ${prodInfo.Type || ''}`}
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <View>
                      <Price
                        price={prodInfo.product_amount || ''}
                        titleAlign={'left'}
                        titleSize={theme.size15}
                        mb_titleSize={theme.size12}
                        titleFont={'MontserratSemiBold'}
                      />
                    </View>
                    {/* <View style={{ paddingHorizontal: 10 }}>
                      <AmountDisc
                        amount={1299}
                        disc={20}
                        titleAlign={'flex-start'}
                        amountSize={theme.size13}
                        discSize={theme.size13}
                        mb_amountSize={theme.size10}
                        mb_discSize={theme.size10}
                      />
                    </View> */}
                  </View>
                  <View style={styles.pTop}>
                    <Text style={styles.ordrStatus}>
                      {prodInfo.order_status || ''}{' '}
                      <Text style={{ fontFamily: 'MontserratRegular' }}>
                        {`(${prodInfo.deliver_date})`}
                       
                      </Text>
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.borderBotm100blck}></View>
            {prodInfo.accessories.length > 0 ?
            <View>
            <TouchableOpacity
              // onPress={() => {
              //   setIsItems(!isItems);
              // }}
               onPress={null}
            >
              {isItems ? (
                <View style={styles.blueLink}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={styles.blueLink}>More Items </Text>
                    {/* <FontAwesome
                      name="angle-down"
                      size={18}
                      color={theme.blue}
                      style={{ marginLeft: 5 }}
                    /> */}
                  </View>
                  <View>
                    <MoreItemsMolecule isItemShow 
                     accessItems={prodInfo.accessories || []}/>
                  </View>
                </View>
              ) : (
                <View style={styles.blueLink}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                    }}
                  >
                    <Text style={styles.blueLink}>More Items </Text>
                    <FontAwesome
                      name="angle-right"
                      size={18}
                      color={theme.blue}
                      style={{ marginLeft: 5 }}
                    />
                  </View>
                  <View>
                    <MoreItemsMolecule />
                  </View>
                </View>
              )}
            </TouchableOpacity>
            <View style={styles.borderBotm100blck}></View>
            </View>:null}
      </View> 
      )}
      </View>
      {/* <View style={styles.borderBotm100blck}></View> */}
      <TouchableOpacity
        onPress={() => {
          setIsDetails(!isDetails);
        }}
      >
        {isDetails ? (
          <View style={styles.blueLink}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.blueLink}>Order Deatils </Text>
              <FontAwesome
                name="angle-down"
                size={18}
                color={theme.blue}
                style={{ marginLeft: 5 }}
              />
            </View>
            <View>
              <OrderTablesMolecule 
               isTableShow 
               cakeAmount = {orderinfo.cake_amount || 0}
               accessoryAmount = {orderinfo.accessory_amount || 0}
               billingAmount = {orderinfo.billing_amount || 0}
               />
            </View>
          </View>
        ) : (
          <View style={styles.blueLink}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <Text style={styles.blueLink}>Order Deatils </Text>
              <FontAwesome
                name="angle-right"
                size={18}
                color={theme.blue}
                style={{ marginLeft: 5 }}
              />
            </View>
            <View>
              <OrderTablesMolecule 
               cakeAmount = {orderinfo.cake_amount || 0}
               accessoryAmount = {orderinfo.accessory_amount || 0}
               billingAmount = {orderinfo.billing_amount || 0}
               
              />
            </View>
          </View>
        )}
      </TouchableOpacity>
      {/* {canCancel?<TouchableOpacity style={styles.otpButtonContainer}
          onPress={()=>setIsOpen(true)}
          //onPress={cancleOrder}
          >
            <Text style={styles.HeadingTxt}> 
                Cancel Order
            </Text>
        </TouchableOpacity>:null} */}
      <View>
          {isWeb?<WebModal isOpen={modalIsOpen} style={customStyles}>
          <CloseModal closeTop={1} closeRight={2} onPress={()=>setIsOpen(false)}/>
              
              <CancleOrderMolecule 
                orderToken={orderinfo.order_token || ''} 
                order_id={orderinfo.order_id || 0}/>
          </WebModal>:<Modal visible={modalIsOpen} animationType="slide">
          <CloseModal closeTop={10} closeRight={20} onPress={()=>setIsOpen(false)}/>
              <View style={{marginTop:15}}>
              <CancleOrderMolecule  orderToken={orderinfo.order_token || ''} 
                order_id={orderinfo.order_id || 0}/>
              </View>
             
          </Modal>}
        </View>
    </View>
  );
};

const base = {
  pimg: {
    width: 100,
  },
  cicon: {
    fontSize: 24,
  },
  borderBotm100blck: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
    marginTop: 5,
  },
  leftPdng: {
    paddingLeft: 20,
  },
  normalText: {
    fontSize: theme.size14,
    color: theme.Hash666,
    fontFamily: 'MontserratRegular',
    flex: 1, 
    flexWrap: 'wrap'
  },
  ordrStatus: {
    fontSize: theme.size14,
    color: theme.darkbg,
    fontFamily: 'MontserratSemiBold',
  },
  pTop: {
    paddingTop: 10,
  },
  blueLink: {
    color: theme.blue,
    fontSize: theme.size14,
    fontFamily: 'MontserratRegular',
  },
  otpButtonContainer: {
    //width:150,
    // marginTop:20,
    backgroundColor: 'transprant',
    height:25,
    alignItems:'center',
    justifyContent:'center',
    borderWidth:1,
    borderRadius:4,
    marginBottom:4,
    borderColor:theme.blue

  },
  HeadingTxt:{
    // marginLeft:15,
    padding:10,
    fontSize:12,
    color:theme.blue,
    fontWeight: "bold"
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      pimg: {
        width: 70,
      },
      cicon: {
        fontSize: 15,
      },
      reviewWrap: {
        padding: 10,
      },
      leftPdng: {
        paddingLeft: 10,
      },
      pTop: {
        paddingTop: 2,
      },
      normalText: {
        fontSize: theme.size11,
      },
      ordrStatus: {
        fontSize: theme.size12,
      },
      blueLink: {
        fontSize: theme.size12,
      },
    })
  )
);

export default OrderItemMolecule;
