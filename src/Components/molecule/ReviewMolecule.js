import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import Rating from '../atom/Rating';

const { width, height } = Dimensions.get('window');

const ReviewMolecule = (props) => {
  const { reviewdata } = props;
  return (
    <View style={styles.reviewWrap}>
      <Rating rate={reviewdata.rating?reviewdata.rating:0} />
      <View style={{ marginTop: 8 }}>
        <TitleText
          title="Feedback  delivery birthday"
          txtColor={theme.darkgrey}
          titleAlign={'left'}
        />
      </View>
      <View style={{ marginTop: 5 }}>
        <TitleText
          title={reviewdata.comment?reviewdata.comment:''}
          titleSize={theme.size13}
          txtColor={theme.darkgrey}
          titleAlign={'left'}
        />
      </View>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.availFlex}>
          <TitleText
            title={reviewdata.fname?`${reviewdata.fname} ${reviewdata.lname}`:''}
            titleSize={theme.size12}
            txtColor={theme.grey2}
            titleAlign={'left'}
          />
        </View>
        <View style={styles.directionFlex}>
          <TitleText
            title="jun 16, 2020"
            titleSize={theme.size12}
            txtColor={theme.grey2}
            titleAlign={'left'}
          />
        </View>
      </View>
    </View>
  );
};

const base = {
  reviewWrap: {
    paddingBottom: 10,
    marginTop: 7,
    backgroundColor: theme.white,
    borderRadius: 2,
    borderWidth: 1,
    paddingRight: 10,
    paddingLeft: 10,
    backgroundColor: theme.white,
    borderColor: theme.lightgrey,
    elevation: 1,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    borderRadius: 8,
  },
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      prodWrap: {
        marginTop: 0,
        borderRadius: 2,
        borderWidth: 0.7,
        padding: 0,
        margin: 0,
      },
    })
  )
);

export default ReviewMolecule;
