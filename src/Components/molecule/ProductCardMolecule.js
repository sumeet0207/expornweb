import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Link } from '../../Route/routing';
import { theme, setFontStyle } from '../../Theme/theme';
import ProductImg from '../atom/ProductImg';
import TitleText from '../atom/TitleText';
import ProductName from '../atom/ProductName';
import AmountDisc from '../atom/AmountDisc';
import Price from '../atom/Price';
import Rating from '../atom/Rating';
const { width, height } = Dimensions.get('window');
import {valueByPercentage,valueByPrice} from '../services/DiscountService';

const ProductCardMolecule = (props) => {
  const [actualAmount, setactualAmount] = useState(0);
  let cakedata = props.cakedata;
  let discount_type = cakedata.discount_type;
  //let url = `productinformation/${cakedata.name}-${cakedata.id}`;
  let url = `productinformation/${cakedata.id}`;

  useEffect(() => {
    calculate()
  },[]);

  function calculate()
  {
    if(discount_type && discount_type!==0)
    {
       if(discount_type==1)
       {
          let amountShow = valueByPrice(cakedata.base_amount,cakedata.discount_amount);
          if(amountShow)
          {
            setactualAmount(amountShow);
          }
        }
       else
       {
          let amountShow = valueByPrice(cakedata.base_amount,cakedata.discount_amount);
          if(amountShow)
          {
            setactualAmount(amountShow);
          }
       }
    }else
    {
      setactualAmount(cakedata.base_amount);
    }
  }
  return (
    <View style={styles.prodWrap}>
      {cakedata.rating?<Rating rate={cakedata.rating?cakedata.rating:0} />:null}

      <View>
        <Link to={url}>
          <ProductImg imgsrc={cakedata.images[0] || ''}/>
        </Link>
        <View style={{ marginTop: 10 }}>
          <ProductName pname={cakedata.name?cakedata.name:''} />
        </View>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
          <TitleText title="Earliest Delivery: " titleSize={12} mb_titleSize={13} />
          <TitleText title={cakedata.is_instant_deliver==1?'Today':'Tomarrow'} titleSize={12} mb_titleSize={13} txtColor={theme.green} />
        </View>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            display: 'flex',
            justifyContent: 'center',
          }}
        >
        <View style={styles.priceMargin}>
          <Price price={actualAmount?actualAmount:0} />
        </View>
        {cakedata.discount_type!=0?
         <AmountDisc distype={cakedata.discount_type?cakedata.discount_type:0}
           amount={cakedata.base_amount} disc={cakedata.discount_amount?cakedata.discount_amount:0} />:null}
           </View>
      </View>
    </View>
  );
};

const base = {
  prodWrap: {
    paddingBottom: 10,
    marginTop: 7,
    backgroundColor: theme.white,
    borderRadius: 2,
    borderWidth: 1,
    padding: 5,
    backgroundColor: theme.white,
    borderColor: theme.lightgrey,
    //alignItems: 'center',
  },
  priceMargin:{
      marginRight: 10
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(400, {
      prodWrap: {
        marginTop: 0,
        borderRadius: 2,
        borderWidth: 0.7,
        padding: 0,
        margin: 0,
      },
      priceMargin:{
        marginRight: 5
    }
    })
  )
);

export default ProductCardMolecule;
