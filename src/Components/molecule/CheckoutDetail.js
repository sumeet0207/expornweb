import React, { useState, useEffect,useCallback  } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator,
  Platform,TouchableHighlight
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Link } from '../../Route/routing';
import { theme, setFontStyle } from '../../Theme/theme';
import SubmitButton from '../atom/SubmitButton';
import TitleText from '../atom/TitleText';
import { GetRequestFunction } from "../../api/ApiHelper";
import { getApiurl } from "../../api/ApiKeys";
import { getAsyncStorageFunction } from "../services/CookieModule";
import RadioComp from '../atom/RadioComp';
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import ButtonSpinner from 'react-native-button-spinner';
import { PaymentsStripe as Stripe } from 'expo-payments-stripe';
var stripec = require('stripe-client')('pk_live_51IMUvFHIEYOsP9krabdXv7ogTBo8LTwZ46WaxQOnIy2Wdv2R6FNJwW5wYR8pfSJJtkycGafYgzRfcdtpLm6RLhkg00ZLkPjN01');


const { width, height } = Dimensions.get('window');
const isWeb = Platform.OS === 'web';

const CheckoutDetail = (props) => {
  const [cartAmount, setcartAmount] = useState(0);
  const [cakeAmount, secakeAmount] = useState(0);
  const [accessAmount, setAccessAmount] = useState(0);
  const [onlyonline, setOnlyOnline] = useState(true);
  const [isloading, setIsLoading] = useState(false);

  useEffect(() => {

    getAmountCart();
    //getCartDetails();
   
  },[]);

  async function getCartDetails()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
   
      if(mixdelight_token)
      {
        let header = { Token: mixdelight_token };
        let cartUrl = getApiurl("getCartDetails");
        let cartList = await GetRequestFunction(cartUrl,header, {});
        console.log(cartList)
        if(cartList.status)
        {
            let weight  = 0;
            let data = cartList.data;
            let orp = data.orderProduct;
            orp.forEach(element => {
                  weight = weight + parseFloat(element.weight);
            });

            if(weight >= 1)
            {
               setOnlyOnline(false)
            }
            
        }else
        {
          error(cartList.message)
          
        }
      }else
      {
        console.log("Must login")
      }
  }

  async function getAmountCart()
  {
      let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
      if(mixdelight_token)
      {
        let header = { Token: mixdelight_token };
        let cartAmountUrl = getApiurl("getCartAmount");
        let resp = await GetRequestFunction(cartAmountUrl,header, {});
        if(resp.status)
        {
            let data = resp.data[0];
            setcartAmount(data.totalAmount)
            secakeAmount(data.cakeAmount);
            setAccessAmount(data.accessoryAmount);
         }
      }else
      {
        // /history.push('/');
      }
  }

  async function strip(){

    let information = {
        card: {
          number: '4591150037090222',
          exp_month: '06',
          exp_year: '21',
          cvc: '647',
          name: 'Omkar'
        }
    }
   
    var card = await stripec.createToken(information);
    console.log(card)
    if(!card.error){
      var token = card.id;
      console.log(token)
      alert(`Token: ${token}`)
    }else{
      alert(`Error: ${card.error.code}`)
    }

  }


  return (
    <View>
      <View style={styles.detailWrap}>
        <View>
      
          <View>
            <TitleText
              title="PRICE DETAILS"
              txtColor={theme.darkbg}
              titleAlign={'left'}
              titleFont="MontserratSemiBold"
            />
          </View>
          <View style={styles.spacebtwn}>
            <View>
              <Text style={styles.priceTable}>Cake Amount</Text>
            </View>
            <View>
              <Text style={styles.priceTable}>{`₹ ${cakeAmount}`}</Text>
            </View>
          </View>
          <View style={styles.spacebtwn}>
            <View>
              <Text style={styles.priceTable}>Accessory Amount</Text>
            </View>
            <View>
              <Text style={styles.priceTable}>{`₹ ${accessAmount}`}</Text>
            </View>
          </View>
  
          <View style={styles.spacebtwn}>
            <View>
              <Text style={styles.priceTable}>Platform Handling Fee</Text>
            </View>
            <View>
              <Text style={[styles.priceTable, { color: theme.green }]}>
                FREE
              </Text>
            </View>
          </View>
          <View style={{ paddingVertical: 5 }}>
            <View style={styles.borderBotm100}></View>
          </View>
          <View style={styles.spacebtwn}>
            <View>
              <Text
                style={[
                  styles.priceTable,
                  { fontFamily: 'MontserratSemiBold' },
                ]}
              >
                Total Amount
              </Text>
            </View>
            <View>
              <Text style={[  styles.priceTable, { fontFamily: 'MontserratSemiBold' },
                ]}>{`₹ ${cartAmount}`}</Text>
            </View>
          </View>
        </View>

        <View>
              <TouchableOpacity onPress={()=>props.getGetway(1)}>
              <View style={styles.rowClass}>
                  {props.gateway== 1?
                  <FontAwesome name="dot-circle-o" style={styles.radioCheck} />:
                  <Feather name={'circle'} style={styles.radioUncheck} />}
                  <Text style={{paddingLeft:5}}>Online Payment</Text>
                </View>
              </TouchableOpacity>
             
              {/* {onlyonline?<TouchableOpacity onPress={()=>props.getGetway(2)}>
                {props.gateway== 2?
                <FontAwesome name="dot-circle-o" style={styles.radioCheck} />:
                <Feather name={'circle'} style={styles.radioUncheck} />}
                <Text>Cash on Delivery</Text>
              </TouchableOpacity>:null} */}
              
              <TouchableOpacity onPress={()=>props.getGetway(2)}>
                <View style={styles.rowClass}>
                  {props.gateway== 2?
                  <FontAwesome name="dot-circle-o" style={styles.radioCheck} />:
                  <Feather name={'circle'} style={styles.radioUncheck} />}
                  <Text style={{paddingLeft:5}}>Cash on Delivery</Text>
                </View>
              </TouchableOpacity>

        
       
        </View>

        <View style={{ marginTop: 10 }}>
          {/* {cartAmount > 0 ?
            <SubmitButton btnTxt="continue" onPress={strip}/>:
            // <SubmitButton btnTxt={<ActivityIndicator size="small" color="white" />} onPress={null}/>
            <SubmitButton btnTxt={isWeb?<ActivityIndicator size="small" color="white" />:'Loading...'} onPress={null}/>
          } */}

          {cartAmount > 0 ?
            <SubmitButton btnTxt="continue" onPress={props.placeOrder}/>:
            // <SubmitButton btnTxt={<ActivityIndicator size="small" color="white" />} onPress={null}/>
            <SubmitButton btnTxt={isWeb?<ActivityIndicator size="small" color="white" />:'Loading...'} onPress={null}/>
          }
          
         </View>
      </View>
    </View>
  );
};

const base = {
  detailWrap: {
    minHeight: 150,
    backgroundColor: 'white',
    borderColor: theme.lightgrey,
    // elevation: 8,
    // shadowOffset: { width: 1, height: 1 },
    // shadowOpacity: 0.3,
    // shadowRadius: 3,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    // marginHorizontal: 5,
    marginBottom: 20,
    marginTop: 10,
  },
  totalClass: {
    fontSize: theme.size14,
  },
  priceTable: {
    fontSize: theme.size13,
    fontFamily: 'MontserratRegular',
    paddingHorizontal: 0,
  },
  spacebtwn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginTop: 5,
  },
  radioUncheck: {
    fontSize: 20
  },
  radioCheck: {
    fontSize: 23
  },
  rowClass:{
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical:5
  }

};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      detailWrap: {
        minHeight: 135,
        paddingHorizontal: 10,
        // paddingVertical: 0,
        // borderWidth: 0,
        // borderColor: '#ccc',
        // borderRadius: 0,
      },
      radioUncheck: {
        fontSize: 20
      },
      radioCheck: {
        fontSize: 23
      },
    })
  )
);

export default CheckoutDetail;
