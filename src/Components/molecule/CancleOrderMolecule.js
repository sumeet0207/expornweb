import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform
} from "react-native";
import { getApiurl } from "../../api/ApiKeys";
import { getAsyncStorageFunction } from "../services/CookieModule";
import {useParams,useHistory } from '../../Route/routing';
import { PostRequestFunction } from "../../api/ApiHelper";
import { useShow } from "../services/AlertHook";

const isWeb = Platform.OS === 'web';
const { height, width } = Dimensions.get('window');

const theme = {
   smallFont:10,
   largeFont:12,
   checkboxHW:13,
   backColor:'white',
   primaryFontColor:"white",
   primaryGrey: '#A5A7AB',
   primaryBlack:"black",
   primaryRed:"#FF1E1E",
}

export default function CancleOrderMolecule(props){
        const {orderToken='',order_id} = props;
        let history = useHistory();
        const [reason, setReason] = useState('');
        const [comment, setComment] = useState('');
        const [reasonError, setReasonError] = useState(false);
        const [commentError, setCommentError] = useState(false);
        const { show, error, success } = useShow();


        async function cancleOrder()
        {

            let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");

            if(mixdelight_token && order_id > 0)
            {
                let header = { Token: mixdelight_token };
                let cancelOrderUrl = getApiurl("cancelOrder");

                if(reason=='')
                {
                    setReasonError(true)
                }else
                {
                    setReasonError(false)
                }

                if(comment=='')
                {
                    setCommentError(true)
                }else
                {
                    setCommentError(false)
                }

                if(reasonError && commentError)
                {
                    return false;
                }

                let param = {
                "order_id":order_id,
                "reason":reason,
                "comment":comment
                }
                let ordercancelResp = await PostRequestFunction(cancelOrderUrl,header, param);
                console.log(ordercancelResp)

                if(ordercancelResp.status)
                {
                   success('Cancle order successfully !')
                   history.push('/orders')
                }else
                {
                   error(ordercancelResp.message)
                   history.push('/orders')
                }
            }else
            {
                history.push('/')
            }
           
        }

        return (
            <ScrollView style={styles.SafeAreaViewstyle}>
                <View style={styles.formView}>
                    <View>
                        <TouchableOpacity
                            style={styles.otpButtonContainer}
                            onPress={null}
                        >
                        <Text style={styles.HeadingTxt}>
                            {`Cancel Order (ORDER NO : ${orderToken})`}
                        </Text>
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.formViewBox,styles.borderWidthStyle]}>
                        <TextInput style={[styles.input,styles.borderWidthStyle]}
                            placeholder="Reason"
                            maxLength={100}
                            onChangeText={(text=>setReason(text))} value={reason}/>
                        {reasonError && <Text style={styles.errorText}>
                        {reasonError?'This is required.':''}</Text>}
                        <TextInput
                            style={{
                            borderColor: theme.primaryGrey,
                            borderWidth: 1,
                            width: null,
                            borderRadius: 5,
                            padding: 5,
                            marginBottom: 20,
                            }}
                            multiline
                            numberOfLines={4}
                            editable
                            maxLength={500}
                            placeholder="Comment"
                            onChangeText={(text=>setComment(text))} value={comment}
                        />
                        {commentError && <Text style={styles.errorText}>{commentError?'This is required.':''}</Text>}
                    </View>
                    <TouchableOpacity style={[styles.cabinetButton,styles.cabinetBack]}
                        onPress={cancleOrder}>
                        <Text style={[styles.cabinetButtonText,styles.callnowtext]}>
                          Cancel Order
                        </Text>
                    </TouchableOpacity>
                </View>
               
               
             
            </ScrollView>
        );
   
}

const styles = StyleSheet.create({
    viewWidth:{
        width:300
    },
    formView:{
      paddingLeft:15,
      paddingRight:15,
      marginBottom:45
    },
    borderWidthStyle:{
        borderWidth:1,
        borderStyle:'solid',
        borderColor:theme.primaryGrey
    },
    formViewBox:{
        padding:12,
        marginBottom:20,
    },
    SafeAreaViewstyle:{
        padding:10,
        marginTop:20,
        backgroundColor : theme.backColor,
    },
    input: {
        borderRadius: 4,
        height:40,
        marginBottom:10,
        paddingLeft:10
      },
    pickerBox: {
        paddingTop:0,
        borderRadius: 4,
        height:50,
        marginBottom:10,
    },      
    otpButtonContainer: {
        backgroundColor: theme.primaryBlack,
        height:45,
        borderTopLeftRadius:6,
        borderTopRightRadius:6,
        alignItems:'center',
        justifyContent:'center',
        paddingBottom:isWeb?0:20
        

    },
    HeadingTxt:{
        marginLeft:15,
        top:12,
        fontSize:14,
        color:theme.backColor,
        fontWeight: "bold"
    },
    instrHeading:{
        marginTop:5,
        fontWeight:"bold",
        color:theme.primaryBlack,
        fontSize:15
    },
    instrDesc:{
        fontSize:15,
        color:'red',

    },
    cabinetButton:{
        height:45,
        width:"100%",
        paddingVertical: 12,
        paddingLeft:15
     },
      cabinetButtonText: {
      fontSize: 15,
      fontWeight:"bold",
      justifyContent:"center",
      color:"white"
    },
    cabinetBack:{
        backgroundColor: theme.primaryRed,
      },
    errorText:{
        color:theme.primaryRed,
        marginBottom:5
    }  
  

});