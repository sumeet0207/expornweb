import React, { useState, useEffect,useCallback  } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
  FlatList
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link,useHistory } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import SubmitButton from '../atom/SubmitButton';
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import AddressComp from '../atom/AddressComp';
import { GetRequestFunction } from '../../api/ApiHelper';
import { getApiurl } from "../../api/ApiKeys";
import {getAsyncStorageFunction} from "../services/CookieModule";
import {CheckValidPincode} from "../services/CommonModule";
import { useShow } from "../services/AlertHook";


const { width, height } = Dimensions.get('window');

const isWeb = Platform.OS === 'web';

const CheckoutAddress = (props) => {
  const { showw, error, success } = useShow();
  const [toggleRadio, setToggleRadio] = useState(true);
  
  const [addressRadio, setAddressRadio] = useState(0);
  const [allAddress, setAllAddress] = useState([]);
  const history = useHistory();

  useEffect(() => {
    getAllAdddress();
  },[]);

  async function getAllAdddress()
  {
      let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
      if(mixdelight_token)
      {
        let header = { Token: mixdelight_token };
        let getAddressUrl = getApiurl("getShipping");
        let addressList = await GetRequestFunction(getAddressUrl,header, {});
        // /console.log(addressList)
        if(addressList.status)
        {
          let data = addressList.data;
          //console.log(data)
          setAllAddress(data);
         
        }
      }else
      {
        //history.push('/');
      }
  }

  function addArress()
  {
    history.push('addaddress')
  }
  
  async function selectAddress(add_id,pincodeval)
  {
    let checkRes = await CheckValidPincode(pincodeval);
    if(checkRes)
    {
      setAddressRadio(add_id)
      props.getShippingAddress(add_id)
    }else
    {
      error('Currently we are not available there !!')
    }
    
  }

  return (
    
    <View style={styles.reviewWrap}>
      <View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginBottom: 10,
            alignItems: 'center',
          }}
        >
          <TitleText
            title="Select Delivery Address"
            titleSize={theme.size20}
            mb_titleSize={theme.size15}
            titleAlign={'left'}
            titleFont="MontserratSemiBold"
          />
          <View>
            <SubmitButton
              btnTxt="ADD NEW ADDRESS"
              Colorbg={theme.white}
              sbtnbrdrColor={theme.grey2}
              subBtncolor={theme.Hash666}
              sbtntxtSiz={theme.size14}
              mb_sbtnWidth={'100%'}
              mb_sbtnHeight={30}
              mb_sbtntxtSiz={theme.size11}
              onPress={addArress}
            />
          </View>
        </View>
    
        <View>
          <View style={{ paddingLeft: 5, marginTop: 10 }}>
            {allAddress.length > 0?<TitleText
              title="ADDRESS"
              titleSize={theme.size16}
              mb_titleSize={theme.size15}
              titleAlign={'left'}
            />:null}
          </View>
          
          <View>
          {allAddress.map(item => (
            <View style={styles.mainbox}>
                <View style={styles.radioWrap}>
                  <TouchableOpacity onPress={()=>selectAddress(item.id,item.pincode)}>
                    {addressRadio==item.id?
                    <FontAwesome name="dot-circle-o" style={styles.radioCheck} />:
                    <Feather name={'circle'} style={styles.radioUncheck} />}
                  </TouchableOpacity>
                </View>
                <View style={styles.addressWrap}>
                  <AddressComp
                    isHide={true}
                    id={item.id?item.id:''}
                    title={item.name?item.name:''}
                    address={item.address?item.address:''}
                    city={item.city?item.city:''}
                    pincode={item.pincode?item.pincode:''}
                    number={item.number?item.number:''}
                  />
                </View>
             </View>
            ))}
            
          </View>
        </View>
      </View>
    </View>
  );
};

const base = {
  reviewWrap: {
    minHeight: 150,
    backgroundColor: 'white',
    borderColor: theme.lightgrey,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginHorizontal: 10,
    marginBottom: 20,
    marginTop: 10,
  },
  mainbox: {
    paddingBottom: 0,
    marginRight: 4,
    marginLeft: 4,
    marginBottom: 15,
    borderRadius: 3,
    borderColor: '#ccc',
    borderWidth: 1,
    flexDirection: 'row',
  },
  radioWrap: {
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  radioUncheck: {
    fontSize: 21,
    color: theme.green,
  },
  radioCheck: {
    fontSize: 24,
    color: theme.green,
  },
  addressWrap: {
    paddingVertical: 15,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      reviewWrap: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        marginBottom: 7,
        marginHorizontal: 0,
        // borderWidth: 0,
        // borderColor: '#ccc',
        // borderRadius: 0,
      },
      radioWrap: {
        paddingHorizontal: 8,
        paddingVertical: 9,
        paddingRight: 0,
      },
      radioUncheck: {
        fontSize: 20
      },
      radioCheck: {
        fontSize: 23
      },
      addressWrap: {
        paddingVertical: 7,
        marginBottom: 10,
      },
    })
  )
);

export default CheckoutAddress;
