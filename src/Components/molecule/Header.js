import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
// import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import NavItem from '../atom/NavItem';
import SignUpButton from '../atom/SignUpButton';
import { FontAwesome } from '@expo/vector-icons';
import { ThemeColors } from 'react-navigation';
import CartCount from '../atom/CartCount';
import {useParams,useHistory } from '../../Route/routing';
import { useDispatch, useSelector } from 'react-redux';
import AlertTemplate from '../atom/AlertComponent';


const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    mbHeaderWrap: {
      // marginBottom: 10,
      flexDirection: 'row',
      // justifyContent: 'space-between',
      // paddingVertical: 15,
      backgroundColor: theme.mbHFColor,
      // width: width,
      height: 80,
      paddingHorizontal: 20,
      alignItems: 'center',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.5)',
      // background:
      //   'linear-gradient(-187.71deg, rgb(103 23 49) 62%, rgb(247, 120, 0) 98%)',
      // background: 'linear-gradient(-187.71deg, #ff005c 62%, #f77800 98%)',
      backgroundColor: '#ffffff',
    },
    innerWRap: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    brandName: {
      color: '#f77800',
      fontSize: theme.size22,
      fontFamily: 'YellowMagician',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    brandNameText: {
      color: '#f77800',
      fontSize: theme.size16,
      fontFamily: 'YellowMagician',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    header: {
      display: 'flex',
      zIndex:-10
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        header: {
          display: 'none',
        },
        mbHeaderWrap: {
          paddingHorizontal: 10,
        },
      })
    )
  );
  return pstyles;
};



const Header = (props) => {
  const pstyles = getStyle(props);
  let history = useHistory();
  const rediscartQuantity = useSelector(state => state.cartReducer.cartQuantity)
  const redisFname = useSelector(state => state.profileReducer.fname)
  const { isAlert, text, options, type } = useSelector(state => state.alertReducer);
  
  function goToSignup()
  {
    history.push('signup');
  }
  
  return (
    <View style={pstyles.header}>
      <View style={pstyles.mbHeaderWrap}>
        <View style={{ flexGrow: 1 }}>
        <TouchableOpacity onPress={()=>history.push('/')}> 
          <View style={pstyles.innerWRap}>
          
            <View style={{ width: 60, height: 60 }}>
              <Image
                source={require('../../assets/img/logoicon.png')}
                resizeMode="contain"
                style={{ width: '100%', height: '100%' }}
              />
            </View>
           
              {/* {isAlert === true ?
               <AlertTemplate text={text} options={options} type={type} props={props}/> : null} */}
          
            
            
            {/* <View
              style={{
                width: 160,
                height: 80,
                marginHorizontal: 6,
                marginTop: 30,
              }}
            >
              <Image
                source={require('../../assets/img/logotext.png')}
                resizeMode="contain"
                style={{ width: '100%', height: '100%' }}
              />
            </View> */}
            <View style={{ alignItems: 'center', marginTop: 5 }}>
              <Text style={pstyles.brandName}>MIX DELIGHT</Text>
              <Text style={pstyles.brandNameText}>CAKES & MORE</Text>
            </View>
          </View>
          </TouchableOpacity>
        </View>

        {isAlert === true ?
               <AlertTemplate text={text} options={options} type={type} props={props}/> : null}

        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <NavItem navtitle={'Cake'} onPress={()=>history.push('/cakelisting')}/>
          <NavItem navtitle={'Same day Delivery'} onPress={()=>history.push('/expressdelivery')}/>
          <NavItem navtitle={'About Us'} onPress={()=>history.push('/aboutus')}/>
          <NavItem navtitle={'Contact Us'} onPress={()=>history.push('/contactus')}/>
          {/* <NavItem navtitle={'Contcat us'} /> */}
          <View style={{ paddingRight: 30 }}>
            {redisFname?<SignUpButton 
             onClick={()=>history.push('/profile')}
             navText={redisFname} upBtnBg={theme.white} 
             paddprop={0}
             btnWidth={0}
             upBtnColr={theme.mbHFColor} />
            :
            <SignUpButton navText={'Sign Up'} onClick={goToSignup}/>}
          </View>
          {/* <View style={{ paddingRight: 30 }}>
            
          </View> */}
          <TouchableOpacity onPress={()=>history.push('/cart')}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ alignItems: 'center' }}>
                <Entypo
                  name="shopping-cart"
                  size={24}
                  color={theme.HFColorText}
                  
                />
              </View>
              <View style={{ paddingLeft: 0 }}>
                <CartCount
                  count={rediscartQuantity}
                  countSize={theme.size12}
                  countFont={'MontserratSemiBold'}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Header;
