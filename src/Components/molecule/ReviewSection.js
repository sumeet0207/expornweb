import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  Dimensions,
  Platform
} from 'react-native';
import { theme } from '../../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import TitleText from '../atom/TitleText';
import ReviewMolecule from './ReviewMolecule';
import SubmitButton from '../atom/SubmitButton';
import GiveRating from '../atom/GiveRating';
import { Input, Image } from 'react-native-elements';
import { Rating } from 'react-native-ratings';

const isWeb = Platform.OS === 'web';
export default function ReviewSection(props) {

  function ratingCompleted(rating) {
    console.log("Rating is: " + rating)
  }
  
  return (
    <View>
      <View style={styles.borderBotm100}></View>

      <Row>
        <Col xs={12} sm={12} md={4} lg={6} xl={6}>
          <TitleText
            title="Write Review"
            titleSize={theme.size25}
            txtColor={theme.darkgrey}
            titleAlign={'left'}
          />
          {/* overallRating start*/}
          <View style={{ alignItems: 'center', paddingVertical: 10 }}>
            <View>
              <View
                style={{
                  padding: 10,
                  borderWidth: 5,
                  borderColor: theme.green,
                  width: 120,
                  height: 120,
                  borderRadius: 120 / 2,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
              >
                <TitleText
                  title={props.avgRating?props.avgRating:0}
                  titleSize={theme.size25}
                  txtColor={theme.darkgrey}
                  titleAlign={'left'}
                />
              </View>
              <View style={{ paddingVertical: 10 }}>
                <TitleText
                  title={props.allreview.length >0?`Based on ${props.allreview.length} ratings`:
                  `Give first review`}
                  titleSize={theme.size14}
                  txtColor={theme.darkgrey}
                  titleAlign={'left'}
                />
              </View>
            </View>
          </View>
          {/* overallRating end*/}
        </Col>
        <Col xs={12} sm={12} md={4} lg={6} xl={6}>
          <View style={{}}>
            <TitleText
              title="Already tried it?"
              txtColor={theme.black}
              titleAlign={'left'}
            />
            <TitleText
              title="Share your views with other customers"
              txtColor={theme.black}
              titleAlign={'left'}
            />
            <Row>
              <Col xs={12} sm={12} md={4} lg={8} xl={8}>
                <View style={{ paddingVertical: 20 }}>
                  <View>
                    <TextInput
                      style={{
                        borderColor: '#ccc',
                        borderWidth: 1,
                        width: null,
                        borderRadius: 5,
                        padding: 5,
                        marginBottom: 10,
                      }}
                      multiline
                      numberOfLines={4}
                      editable
                      maxLength={400}
                      placeholder="Write your review......"
                    />
                  </View>

                  <Row>
                  <Rating
                    type='custom'
                    ratingColor='orange'
                    ratingCount={5}
                    imageSize={20}
                    style={{ paddingVertical: 10}}
                    onFinishRating={ratingCompleted}
                  />
                    {/* <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                      <GiveRating />
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                      <GiveRating />
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                      <GiveRating />
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                      <GiveRating />
                    </Col>
                    <Col xs={1} sm={1} md={1} lg={1} xl={1}>
                      <GiveRating />
                    </Col> */}
                  </Row>
                  <View
                    style={{
                      width: 75,
                      height: 30,
                      alignSelf: 'flex-end',
                      marginBottom: 10,
                    }}
                  >
                    <SubmitButton
                      btnTxt="submit"
                      Colorbg={theme.green}
                    ></SubmitButton>
                  </View>
                </View>
              </Col>
            </Row>
          </View>
        </Col>
      </Row>

      {/* <View style={styles.borderBotm100}></View> */}
      {props.allreview.length > 0?
      <Row>
        {props.allreview.map((item,index)=>
          <Col xs={12} sm={12} md={4} lg={3} xl={3}>
            <ReviewMolecule reviewdata={item} key={index}/>
          </Col>)
        }
      </Row>:null}
    </View>
  );
}

const base = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 50,
    zIndex: -15,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 0,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
    })
  )
);
