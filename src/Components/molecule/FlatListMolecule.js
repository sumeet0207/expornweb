import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';
import { theme } from '../../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import TitleText from '../atom/TitleText';

// const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    cabinetBackg: {
      backgroundColor: theme.yellow,
      height: 50,
      padding: 10,
    },
  };

  const pstyles = createStyles(base, minWidth(100, maxWidth(991, {})));
  return pstyles;
};

const FlatListMolecule = (props) => {
  const pstyles = getStyle(props);

  return (
    <View style={{ position: 'sticky', bottom: 0, zindex: 5 }}>
      <View style={pstyles.cabinetBackg}>
        <Text>Testing Sticky</Text>
      </View>
    </View>
  );
};

export default FlatListMolecule;
