import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link,useHistory } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import ProductImg from '../atom/ProductImg';
import { FontAwesome } from '@expo/vector-icons';
import ProductName from '../atom/ProductName';
import AmountDisc from '../atom/AmountDisc';
import Price from '../atom/Price';

const { width, height } = Dimensions.get('window');

const MainItemMolecule = (props) => {
  const {cake,deleteCartCake } = props;
  let history=useHistory();
  return (
    <View>
      <View style={{ flexDirection: 'row', marginBottom: 15 }}>
        <View style={styles.pimg}>
          <TouchableOpacity onPress={()=>history.push(`/productinformation/${cake.product_id}`)}>
           <ProductImg ImgHeight={100} 
            mb_ImgHeight={70} imgsrc={cake.images || ''}/>
           </TouchableOpacity>
        </View>
        <View style={styles.leftPdng}>
          <TouchableOpacity onPress={()=>history.push(`/productinformation/${cake.product_id}`)}>
           <ProductName
              pname={cake.name}
              pnameSize={theme.size18} //13
              pnameAlign={'left'}
              pnameBotm={1}
           />
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
            }}
          >
            <View style={styles.pTop}>
              <Price
                price={cake.product_amount}
                titleAlign={'left'}
                titleSize={theme.size20}
                mb_titleSize={theme.size14}
              />
               <Text style={styles.normalText}>
                      {`Weight: ${cake.weight || 0} | Shape : ${cake.shape_id?cake.shape_id==1?'Round':'Heart':''} | Type : ${cake.type_id?cake.type_id==1?'Egg':'Eggless':''}`}
                </Text>
            </View>
            {/* <View style={{ paddingHorizontal: 10 }}>
              <AmountDisc
                amount={1299}
                disc={20}
                titleAlign={'flex-start'}
                mb_amountSize={theme.size12}
                mb_discSize={theme.size12}
              />
            </View> */}
          </View>
        </View>
        <View style={{ position: 'absolute', top: 0, right: 0 }}>
          <TouchableOpacity onPress={deleteCartCake}>
            <FontAwesome
              name="trash-o"
              style={styles.cicon}
              color={theme.Hash666}
            />
          </TouchableOpacity>
        </View>
      </View>

      {/* <View style={{ paddingVertical: 10 }}>
        <View style={styles.borderBotm100blck}></View>
      </View> */}
    </View>
  );
};

const base = {
  pimg: {
    width: 100,
  },
  cicon: {
    fontSize: 24,
  },
  normalText: {
    fontSize: theme.size14,
    color: theme.Hash666,
    fontFamily: 'MontserratRegular',
  },
  borderBotm100blck: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  leftPdng: {
    paddingLeft: 20,
  },
  pTop: {
    marginTop: 5,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      pimg: {
        width: 70,
      },
      cicon: {
        fontSize: 15,
      },
      reviewWrap: {
        padding: 10,
      },
      leftPdng: {
        paddingLeft: 10,
      },
      pTop: {
        marginTop: 2,
      },
      normalText: {
        fontSize: theme.size12,
      },
    })
  )
);

export default MainItemMolecule;
