import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  Image,
  TouchableOpacity,
  Button,
  TextInput,
} from 'react-native';

import { Link } from '../../Route/routing';

import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
// import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import NavItem from '../atom/NavItem';
import SignUpButton from '../atom/SignUpButton';
import { ThemeColors } from 'react-navigation';
import CartCount from '../atom/CartCount';

import { Entypo } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import SubmitButton from '../atom/SubmitButton';


const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    topFooter: {
      flexDirection: 'row',
      backgroundColor: theme.darkbg,
      height: 150,
      paddingHorizontal: 20,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      marginTop: 40,
    },
    bottomFooter: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: 25,
      paddingVertical: 10,
    },
    socialicon: {
      color: theme.white,
      fontSize: 25,
      paddingHorizontal: 12,
    },
    copytext: {
      color: theme.darkgrey,
      fontSize: theme.size12,
      fontFamily: 'MontserratRegular',
    },
    footer: {
      display: 'flex',
      backgroundColor: 'white',
      zIndex: -10
      //backgroundColor: '#f9f9f9'
    },
    innerWRap: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    brandName: {
      color: '#f77800',
      fontSize: theme.size22,
      fontFamily: 'YellowMagician',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    brandNameText: {
      color: '#f77800',
      fontSize: theme.size16,
      fontFamily: 'YellowMagician',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    connctwithus: {
      color: theme.white,
      fontSize: theme.size16,
      fontFamily: 'MontserratRegular',
      paddingLeft: 10,
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        footer: {
          display: 'none',
        },
        mbHeaderWrap: {
          paddingHorizontal: 10,
        },
      })
    )
  );
  return pstyles;
};

const Footer = (props) => {
  const pstyles = getStyle(props);

  function openfb()
  {
    window.open("https://www.facebook.com/mix.delightt");
  }

  function openInsta()
  {
    window.open("https://www.instagram.com/mix.delightt");
  }

  function openTwit()
  {
    window.open(" https://twitter.com/delight_mix");
  }

  return (
    <View style={pstyles.footer}>
      <View style={pstyles.topFooter}>
        <View style={pstyles.innerWRap}>
          <View style={[pstyles.innerWRap, { marginRight: 20 }]}>
            <View style={{ width: 60, height: 60 }}>
              <Image
                source={require('../../assets/img/logoicon.png')}
                resizeMode="contain"
                style={{ width: '100%', height: '100%' }}
              />
            </View>
            <View style={{ alignItems: 'center', marginTop: 5 }}>
              <Text style={pstyles.brandName}>MIX DELIGHT</Text>
              <Text style={pstyles.brandNameText}>CAKES & MORE</Text>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            {/* <View>
              <Text>Ruba</Text>
            </View> */}
            <TextInput
              style={{
                height: 50,
                borderColor: '#ccc',
                backgroundColor: theme.white,
                borderWidth: 1,
                width: null,
                borderBottomLeftRadius: 5,
                borderTopLeftRadius: 5,
                padding: 15,
                width: 500,
              }}
              placeholder="Enter Email Address"
            />
            <View>
              <SubmitButton
                btnTxt="Subscribe"
                Colorbg={theme.HFColorText}
                sbtnHeight={50}
                topleftradius={1}
                botmleftradius={1}
              />
            </View>
          </View>
        </View>
        <View>
          <View style={{}}>
            <Text style={pstyles.connctwithus}>Connect with Us</Text>
          </View>
          <View style={{ flexDirection: 'row', marginVertical: 10 }}>
            <TouchableOpacity onPress={openTwit}>
              <View style={{ alignItems: 'center' }}>
                {/* <Entypo name="twitter" style={pstyles.socialicon} /> */}
                <FontAwesome name="twitter-square" style={pstyles.socialicon} />
              </View>
            </TouchableOpacity> 

            <TouchableOpacity onPress={openInsta}>
              <View style={{ alignItems: 'center' }}>
                <FontAwesome name="instagram" style={pstyles.socialicon} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={openfb}>
              <View style={{ alignItems: 'center' }}>
                {/* <FontAwesome name="facebook" style={pstyles.socialicon} /> */}
                <FontAwesome
                  name="facebook-square"
                  style={pstyles.socialicon}
                />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ width: 150, height: 50 }}>
            <Image
              source={require('../../assets/img/app-store-android.png')}
              resizeMode="contain"
              style={{ width: '100%', height: '100%' }}
            />
          </View>
        </View>
      </View>
      <View style={pstyles.bottomFooter}>
        <View style={{ flexDirection: 'row' }}>
          <Link to="/privacypolicy">
            <TouchableOpacity>
              <Text style={pstyles.copytext}>Privacy Policy</Text>
            </TouchableOpacity>
          </Link>
          <Text style={[pstyles.copytext, { paddingHorizontal: 5 }]}>-</Text>
          <Link to="/termsofuse">
            <TouchableOpacity>  
              <Text style={pstyles.copytext}>Terms and Conditions</Text>
            </TouchableOpacity>
          </Link>
          {/* <Text style={[pstyles.copytext, { paddingHorizontal: 5 }]}>-</Text> */}
          {/* <Link to="/faq">
            <TouchableOpacity>  
              <Text style={pstyles.copytext}>FAQ</Text>
            </TouchableOpacity>
          </Link> */}
        </View>
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity>
            <View style={{ alignItems: 'center' }}>
              <Entypo name="twitter" style={pstyles.socialicon} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{ alignItems: 'center' }}>
              <FontAwesome name="instagram" style={pstyles.socialicon} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={{ alignItems: 'center' }}>
              <FontAwesome name="facebook" style={pstyles.socialicon} />
            </View>
          </TouchableOpacity>
        </View>
        <View>
          <Text style={pstyles.copytext}>Copyright 2020 © Mix Delight</Text>
        </View>
      </View>
    </View>
  );
};

export default Footer;
