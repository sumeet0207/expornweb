import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import ProductImg from '../atom/ProductImg';
import { FontAwesome } from '@expo/vector-icons';
import ProductName from '../atom/ProductName';
import AmountDisc from '../atom/AmountDisc';
import Price from '../atom/Price';
import Quantity from '../atom/Quantity';
import { AntDesign } from '@expo/vector-icons';
import {getAsyncStorageFunction} from "../services/CookieModule";
import { PostRequestFunction,GetRequestFunction } from "../../api/ApiHelper";
import { getApiurl } from "../../api/ApiKeys";
import { setCartCount } from "../../store/action/cartAction";
import { useDispatch, useSelector } from 'react-redux';
import { useShow } from "../services/AlertHook";

const { width, height } = Dimensions.get('window');

const ExtraItemMolecule = (props) => {
  const { show, error, success } = useShow();
  const { heading, desc, name, rdate,accessories,cart_id } = props;
  const dispatch = useDispatch();
  
  async function updateAccessoriesQuantity(newQuat)
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    //console.log(mixdelight_token)
    if(mixdelight_token)
    {
      let header = { Token: mixdelight_token };
      let param = { 
         cart_id: parseInt(cart_id),
         access_cart_id : parseInt(accessories.id), 
         access_id: parseInt(accessories.accessory_id),
         quantity:newQuat
      }
      let updateAccessoriesCartUrl = getApiurl("updateAccessoriesCart");
      let updatecart = await PostRequestFunction(updateAccessoriesCartUrl,header, param);
      if(updatecart.status)
      {
        let data = updatecart.data;
        setCartData(updatecart.count,data.orderProduct,data.calculated_amount);
      }else
      {
        error(updatecart.message)
      }

    }
  }

  async function deleteAccessory()
  {
    
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    //console.log(mixdelight_token)
    if(mixdelight_token)
    {
      let header = { Token: mixdelight_token };
      let param = { 
         cart_id: parseInt(cart_id),
         access_cart_id : parseInt(accessories.id), 
         access_id: parseInt(accessories.accessory_id)
      }
      console.log(param);
      let itemDeleteUrl = getApiurl("itemdelete");
      let updatecart = await GetRequestFunction(itemDeleteUrl,header, param);
      console.log(updatecart);
      if(updatecart.status)
      {
        let data = updatecart.data;
        setCartData(updatecart.count,data.orderProduct,data.calculated_amount);
      }else
      {
        error(updatecart.message)
      }
    }
  }

  // async function deleteCartCake()
  // {
    
  //   let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
  //   console.log(mixdelight_token)
  //   if(mixdelight_token)
  //   {
  //     let header = { Token: mixdelight_token };
  //     let param = {  cart_id: parseInt(cart_id)}
  //     let cartDeleteUrl = getApiurl("cartDelete");
  //     let updatecart = await GetRequestFunction(cartDeleteUrl,header, param);
  //     console.log(updatecart);
  //     if(updatecart.status)
  //     {
  //       let data = updatecart.data;
  //       setCartData(updatecart.count,data.orderProduct,data.calculated_amount);
  //     }
  //   }
  // }

  function setCartData(count,orderProduct,calculated_amount)
  {
    dispatch(setCartCount(count));
    props.setlocalCartCount(count)
    props.setCartCake(orderProduct);
    props.setorderAmount(calculated_amount);
  }




  return (
    <View>
      <View style={{ flexDirection: 'row', marginBottom: 15 }}>
        <View style={styles.pimg}>
          <ProductImg ImgHeight={100} mb_ImgHeight={70} imgsrc={accessories.image || ''}/>
        </View>
        <View style={styles.leftPdng}>
          <ProductName
            pname={accessories.name}
            pnameSize={theme.size18}
            pnameAlign={'left'}
            pnameBotm={0}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginTop:-8
            }}
          >
            <Price
              price={parseInt(accessories.price) * parseInt(accessories.quantity)}
              titleAlign={'left'}
              titleSize={theme.size20}
              mb_titleSize={theme.size14}
            />
          </View>
          <View style={styles.qtyBox}>
            <Quantity quatCount={parseInt(accessories.quantity)}
             iscart={true}
             updateAccessoriesQuantity={updateAccessoriesQuantity}/>
          </View>
        </View>
        <View style={{ position: 'absolute', top: 0, right: 0 }}>
          <TouchableOpacity onPress={deleteAccessory}>
            <FontAwesome
              name="trash-o"
              style={styles.cicon}
              color={theme.Hash666}
            />
          </TouchableOpacity>
        </View>
      </View>

      {/* <View style={{ paddingVertical: 10 }}>
        <View style={styles.borderBotm100blck}></View>
      </View> */}
    </View>
  );
};

const base = {
  pimg: {
    width: 100,
  },
  cicon: {
    fontSize: 24,
  },
  borderBotm100blck: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  leftPdng: {
    paddingLeft: 20,
  },
  qtyBox: {
    // backgroundColor: theme.white,
    // width: 60,
    position: 'absolute',
    bottom: 0,
    left: 10
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      pimg: {
        width: 70,
      },
      cicon: {
        fontSize: 15,
      },
      reviewWrap: {
        padding: 10,
      },
      leftPdng: {
        paddingLeft: 10,
      },
      qtyBox: {
        backgroundColor: theme.white,
        width: 50,
      },
    })
  )
);

export default ExtraItemMolecule;
