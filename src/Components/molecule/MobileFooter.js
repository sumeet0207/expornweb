import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme } from '../../Theme/theme';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import CartCount from '../atom/CartCount';
import {useHistory,Link } from '../../Route/routing';
import { MaterialIcons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';


const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const base = {
    mbFooterWrap: {
      // marginTop: 20,
      flexDirection: 'row',
      fex: 1,
      backgroundColor: theme.mbHFColor,
      width: width,
      height: 60,
      paddingHorizontal: 10,
      alignItems: 'center',
    },
    innerWRap: {
      flex: 0.25,
      alignItems: 'center',
    },
    fName: {
      color: theme.white,
      fontSize: theme.size9,
      fontFamily: 'MontserratRegular',
      textTransform: 'uppercase',
    },
    ficon: {
      color: theme.white,
      fontSize: 20,
      marginBottom: 5,
    },
    mobilefooter: {
      display: 'none',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        mobilefooter: {
          display: 'flex',
          position: isWeb? 'sticky':'absolute',
          bottom: 0,
          // marginTop: 0
        },
      })
    )
  );
  return pstyles;
};

const MobileFooter = (props) => {
  const pstyles = getStyle(props);
  let history = useHistory();
  return (
    <View style={pstyles.mobilefooter}>
      <View style={pstyles.mbFooterWrap}>
        <View style={pstyles.innerWRap}>
          <TouchableOpacity onPress={()=>{history.push('/')}}>
            <View style={{ alignItems: 'center' }}>
              <MaterialCommunityIcons name="home" style={pstyles.ficon} />
            </View>
            <Text style={pstyles.fName}>Home</Text>
          </TouchableOpacity>
        </View>
        <View style={pstyles.innerWRap}>
          <TouchableOpacity onPress={()=>{history.push('/cakelisting')}}>
            <View style={{ alignItems: 'center' }}>
              <FontAwesome name="search" style={pstyles.ficon} />
            </View>
            <Text style={pstyles.fName}>Explore</Text>
          </TouchableOpacity>
        </View>
        <View style={pstyles.innerWRap}>
          <TouchableOpacity onPress={()=>{history.push('/expressdelivery')}}>
            <View style={{ alignItems: 'center' }}>
              <FontAwesome5 name="clock" style={pstyles.ficon} />
            </View>
            <Text style={pstyles.fName}>Need Today</Text>
          </TouchableOpacity>
          {/* <TouchableOpacity>
            <View style={{ flexDirection: 'row', paddingLeft: 2 }}>
              <View style={{ alignItems: 'center' }}>
                <FontAwesome5 name="shopping-bag" style={pstyles.ficon} />
              </View>
              <View style={{ paddingLeft: 5 }}>
                <CartCount
                  count={50}
                  countSize={theme.size10}
                  countColor={theme.white}
                />
              </View>
            </View>
            <Text style={pstyles.fName}>Cart</Text>
          </TouchableOpacity> */}
        </View>
        <View style={pstyles.innerWRap}>
          <TouchableOpacity onPress={()=>{history.push('/profile')}}>
            <View style={{ alignItems: 'center' }}>
              <MaterialCommunityIcons name="account" style={pstyles.ficon} />
            </View>
            <Text style={pstyles.fName}>account</Text>
          </TouchableOpacity>
        </View>
        <View style={pstyles.innerWRap}>
          {/* <TouchableOpacity onPress={()=>{history.push('/contactus')}}> */}
          <TouchableOpacity onPress={()=>{history.push('/contactus')}}>
            <View style={{ alignItems: 'center' }}>
              <AntDesign name="contacts" style={pstyles.ficon} />
            </View>
            <Text style={pstyles.fName}>Contact</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default MobileFooter;
