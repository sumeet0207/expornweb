import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Platform,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';
import OrderItemMolecule from '../molecule/OrderItemMolecule';
import { FontAwesome } from '@expo/vector-icons';
import { Feather } from '@expo/vector-icons';
import AddressComp from '../atom/AddressComp';

const { width, height } = Dimensions.get('window');

const isWeb = Platform.OS === 'web';

const OrderMolecule = (props) => {
  const {orderinfo} = props;
  const [toggleRadio, setToggleRadio] = useState(true);

  return (
    <View style={styles.reviewWrap}>
      <View>
        <OrderItemMolecule orderinfo={orderinfo}/>
      </View>
    </View>
  );
};

const base = {
  reviewWrap: {
    minHeight: 150,
    backgroundColor: 'white',
    borderColor: theme.lightgrey,
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    // marginHorizontal: 5,
    marginBottom: 10,
    // marginTop: 10,
    elevation: 5,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    borderRadius: 3,
  },
  mainbox: {
    paddingBottom: 0,
    marginRight: 4,
    marginLeft: 4,
    marginBottom: 15,
    borderRadius: 3,
    borderColor: '#ccc',
    borderWidth: 1,
    flexDirection: 'row',
  },
  radioWrap: {
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  radioIcon: {
    fontSize: 24,
    color: theme.green,
  },
  addressWrap: {
    paddingVertical: 15,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      reviewWrap: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        // borderWidth: 0,
        borderColor: '#ccc',
        // borderRadius: 0,
      },
      radioWrap: {
        paddingHorizontal: 8,
        paddingVertical: 9,
        paddingRight: 0,
      },
      radioIcon: {
        fontSize: 16,
        color: theme.green,
      },
      addressWrap: {
        paddingVertical: 7,
        marginBottom: 10,
      },
    })
  )
);

export default OrderMolecule;
