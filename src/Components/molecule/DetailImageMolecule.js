import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, Button, TextInput,TouchableOpacity } from 'react-native';
import { theme } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

import ProductImg from '../atom/ProductImg';

export default function DetailImageMolecule(props) {
  const {imgArray} = props;
  const [parentimg, setParentImg] = useState('');

  useEffect(() => {
    getImg();
  },[]);

  function getImg()
  {
    if(imgArray.length > 0)
    {
      setParentImg(imgArray[0])
    }
  }

  return (
    <View>
      <View style={styles.topContainer}>
        <ProductImg ImgHeight={400} mb_ImgHeight={400} imgsrc={parentimg}/>
      </View>
      <View  style={{ flexDirection: 'row', marginVertical: 10, justifyContent: 'center'}}>

      {imgArray.map(item => (
         <View style={styles.smallImg}>
          <TouchableOpacity onPress={()=>setParentImg(item)}>
              <ProductImg ImgHeight={70} mb_ImgHeight={70} imgsrc={item} onPress={()=>setParentImg(item)}/>
          </TouchableOpacity>
         </View>
      ))}
      </View>
    </View>
  );
}

const base = {
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },
  smallImg: {
    width: 70,
    '&:hover': {
      width: 30,
    },
    height: 70,
    marginHorizontal: 10,
  },
  topContainer:{
    marginTop:17,
  }
};

const styles = createStyles(base, minWidth(100, maxWidth(767, {
  topContainer:{
    marginTop:0,
  }
})));
