import React, {useState} from 'react';
import { StyleSheet, Text, View,Button,Modal,Platform } from 'react-native';
import WebModal from 'react-modal';
import { theme } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import DateTimePicker from '@react-native-community/datetimepicker';
import AccessoriesList from '../../Components/AccessoriesList';
import SlotItem from '../../Components/atom/SlotItem';
import RadioComp from '../../Components/atom/RadioComp';
import CloseModal from '../../Components/atom/CloseModal';


const customStyles = {
  content : {
    width: 'fit-content',
      height: 'fit-content',
      // margin: 'auto',
      transform: 'translate(-50%, -50%)',
      top:'50%',
      left:'50%',
      paddingTop: 35
  }
};


const isWeb = Platform.OS === 'web';

isWeb?WebModal.setAppElement('#root'):null;
export default function SlotMolecule() {

  const [modalIsOpen,setIsOpen] = useState(false);

  // const customStyles = {
  //   content : {
  //     width: 'fit-content',
  //       height: 'fit-content',
  //       // margin: 'auto',
  //       transform: 'translate(-50%, -50%)',
  //       top:'50%',
  //       left:'50%',
  //       paddingTop: 35
  //   }
  // };
 
  return (
    <View style={styles.container}>
        {isWeb?<WebModal isOpen={modalIsOpen} style={customStyles}>
          <CloseModal closeTop={-28} closeRight={-12} />
          <SlotItem />
          {/* <Text>Ruba</Text> */}

        </WebModal>:<Modal visible={modalIsOpen} animationType="slide" style={customStyles}>
          <View>
              <CloseModal />
              {/* <Text>Ruba</Text> */}
              <SlotItem />
              {/* <RadioComp /> */}
          </View>

        </Modal>}
         <View>
          <Button 
            onPress={()=>setIsOpen(true)} 
            title="Select Solt"
            color={theme.gery2} 
            style={styles.btnslot} 
          />
         {/* <Button  onPress={()=>setIsOpen(true)} title="Open" color="#841584"/> */}
         </View>
    </View>
    
 
  );
}

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         // justifyContent: 'center',
//         // alignItems: 'center',
//     },
//     btnslot:{
//       backgroundColor: theme.grey2,
//       textAlign:'left'
//     },
//     ".Modal":{
//       backgroundColor:'red'

//     }
// });

const base = {
  container: {
    flex: 1,
    
    // justifyContent: 'center',
    // alignItems: 'center',
},
webmodal :{
  width: 'fit-content',
  height: 'fit-content',
  transform: 'translate(-50%, -50%)',
  top:'50%',
  left:'50%',
  paddingTop: 35
},
btnslot:{
  backgroundColor: theme.grey2,
  textAlign:'left'
},
".Modal":{
  backgroundColor:'red'

},
content : {
  top                   : '50%',
  left                  : '50%',
  // right                 : '2%',
  // bottom                : '2%',
  // padding:'1%'
  position: 'absolute',
  padding: '1%',
  transform: 'translate(-50%, -50%)',
  width: '90%',
  // height: 150
}
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      content : {
        width: '90%'
      }
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      content : {
        width: '90%'
      }
    })
  )
);
