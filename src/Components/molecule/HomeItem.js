import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from '../atom/TitleText';

const { width, height } = Dimensions.get('window');

const HomeItem = (props) => {

  const { imgsrc } = props;
  const pstyles = getStyle(props);

  return (
    <View style={pstyles.prodWrap}>
      <View>
          <View style={pstyles.imgWrap}>
          <Image
            source={props.imgsrc}
            resizeMode="cover"
            style={{width:'100%', height:'100%'}}
          />
        </View>
        <View style={{ marginTop: 10 }}>
          <TitleText
            title={props.itemName}
            titleSize={16}
            txtColor={theme.black}
            titleAlign={'center'}
            mb_titleSize={13}          />
        </View>
      </View>
    </View>
  );
};

const getStyle = (props) => {
  const base = {
    prodWrap: {
      paddingBottom: 10,
      marginTop: 7,
      backgroundColor: theme.white,
      borderRadius: 2,
      borderWidth: 1,
      padding: 5,
      backgroundColor: theme.white,
      borderColor: theme.lightgrey,
      elevation: 8,
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.2,
      shadowRadius: 5,
    },
    imgWrap:{
      height:200
    }
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        prodWrap: {
          marginTop: 0,
          borderRadius: 2,
          borderWidth: 0.7,
          padding: 0,
          margin: 0,
        },
      })
    )
  );
  return pstyles;
};

export default HomeItem;
