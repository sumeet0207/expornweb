import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList
  
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import MoreItem from '../atom/MoreItem';



const MoreItemsMolecule = (props) => {
  const { heading, desc, name, rdate,accessItems } = props;
  const { isItemShow = false } = props;
  const { width, height } = Dimensions.get('window');

  return (
    <View>
      {isItemShow && (
        <View style={{ marginVertical: 7 }}>
          <Row>
            {accessItems.map((access) =>
              <Col xs={6} sm={6} md={3} lg={3} xl={3}>
                <MoreItem access={access} key={access.accessory_id}/>
              </Col>
            )}
          </Row>
        </View>
      )}
    </View>
  );
};

const base = {
  pimg: {
    width: 60,
  },
  cicon: {
    fontSize: 24,
  },
  borderBotm100blck: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
    marginTop: 5,
  },
  leftPdng: {
    paddingLeft: 20,
  },
  normalText: {
    fontSize: theme.size14,
    color: theme.Hash666,
    fontFamily: 'MontserratRegular',
  },
  ordrStatus: {
    fontSize: theme.size14,
    color: theme.darkbg,
    fontFamily: 'MontserratSemiBold',
  },
  pTop: {
    paddingTop: 10,
  },
  blueLink: {
    color: theme.blue,
    fontSize: theme.size14,
    fontFamily: 'MontserratRegular',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      pimg: {
        width: 70,
      },
      cicon: {
        fontSize: 15,
      },
      reviewWrap: {
        padding: 10,
      },
      leftPdng: {
        paddingLeft: 10,
      },
      pTop: {
        paddingTop: 2,
      },
      normalText: {
        fontSize: theme.size11,
      },
      ordrStatus: {
        fontSize: theme.size12,
      },
    })
  )
);

export default MoreItemsMolecule;
