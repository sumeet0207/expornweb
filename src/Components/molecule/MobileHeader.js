import React, { useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  Platform
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
// import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { ThemeColors } from 'react-navigation';
import { Link, useHistory } from '../../Route/routing';
import { useDispatch, useSelector } from 'react-redux';
import AlertTemplate from '../atom/AlertComponent';
import CartCount from '../atom/CartCount';

const isWeb = Platform.OS === 'web';
// const height = (Platform.OS === 'web') ? 20 : 0;
const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    mbHeaderWrap: {
      // marginBottom: 10,
      flexDirection: 'row',
      // justifyContent: 'space-between',
      // paddingVertical: 15,
      backgroundColor: theme.white,
      width: width,
      height: 60,
      paddingHorizontal: 10,
      alignItems: 'center',
      boxShadow: '0 0 4px 0 rgba(0,0,0,0.5)',
    },
    innerWRap: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    brandName: {
      color: theme.mbHFColor,
      fontSize: theme.size18,
      fontFamily: 'YellowMagician',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    brandNameText: {
      color: theme.mbHFColor,
      fontSize: theme.size10,
      fontFamily: 'YellowMagician',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    mobileheader: {
      display: 'none',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth( 
      100,
      maxWidth(991, {
        mobileheader: {
          display: 'flex',
          position: isWeb? 'sticky':'absolute',
          top: 0,
          marginTop: 0,
          zIndex:1,
          // borderBottomWidth: 1,
          // borderStyle: 'solid',
          // shadowColor: '#000',
          // shadowOpacity: 0.2,
          // elevation: 8,
        }
      })
    )
  );
  return pstyles;
};

const MobileHeader = (props) => {
  const pstyles = getStyle(props);
  const history = useHistory();
  const rediscartQuantity = useSelector(state => state.cartReducer.cartQuantity);
  const { isAlert, text, options, type } = useSelector(state => state.alertReducer);
  return (
    <View style={pstyles.mobileheader}>
      <View style={pstyles.mbHeaderWrap}>
        <View style={{ flexGrow: 1 }}>
          <View style={pstyles.innerWRap}>
            {/* <View style={{ marginRight: 10 }}>
              <FontAwesome name="angle-left" size={24} color={theme.darkbg} />
            </View> */}
            {/*<View>
            <View style={{ width: 50, height: 50 }}>
              <Image
                source={require('../../assets/img/logo.png')}
                resizeMode="contain"
                style={{ width: '100%', height: '100%' }}
              />
            </View>
            <Text style={pstyles.brandName}>Brand Name</Text>
          </View> */}

            <View style={{ width: 40, height: 40 }}>
              <Image
                source={require('../../assets/img/logoicon.png')}
                resizeMode="contain"
                style={{ width: '100%', height: '100%' }}
              />
            </View>
            <View style={{ alignItems: 'center', marginTop: 5 }}>
              <Text style={pstyles.brandName}>MIX DELIGHT</Text>
              <Text style={pstyles.brandNameText}>CAKES & MORE</Text>
            </View>
          </View>
        </View>
        <View>
           <TouchableOpacity onPress={()=>history.push('/cart')}>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ alignItems: 'center' }}>
                <Entypo
                  name="shopping-cart"
                  size={24}
                  color={theme.mbHFColor}
                  
                />
              </View>
              <View style={{ paddingLeft: 0 }}>
                <CartCount
                  count={rediscartQuantity}
                  mb_countSize ={theme.size11}
                  countFont={'MontserratSemiBold'}
                />
              </View>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      {isAlert === true ?
               <AlertTemplate text={text} options={options} type={type} props={props}/> : null}
    </View>
  );
};

export default MobileHeader;
