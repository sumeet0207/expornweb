import React from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Image, ImageBackground, Platform } from 'react-native';
import { Link, useLocation } from '../Route/routing';

import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import OrderMolecule from './molecule/OrderMolecule';
import TitleText from './atom/TitleText';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function Cupcake() {
  let location = useLocation();
  console.log(location.pathname);
  const image = { uri: "https://cdn.pixabay.com/photo/2017/08/06/04/16/cupcake-2588646_960_720.jpg" };
  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View style={{ padding: 20, flexGrow:1}}>
            {/* <View style={styles.imgWrap}>
              <Image
                source={require('../assets/img/cupcakeBg.jpg')}
                resizeMode="cover"
                style={{width:'100%', height:'100%'}}
              />
            </View> */}
             <View style={styles.imgWrap}>
              <ImageBackground source={require('../assets/img/cupcakeBg1.jpg')} style={{width: '100%', height: '100%'}}>
                <View style={styles.topBg}> 
              {/* <ImageBackground 
                 //source={require('../assets/img/cupcakeBg1.jpg')} 
                 source={image}
                 style={{width: '100%', height: '100%'}}>
                <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,alignItems: 'center'}}> */}
                  <View style={styles.txtBg}>
                    <Text style={styles.cupck_main}>You can't buy happiness, but you can buy cupcakes. Mix Delight have cupcakes also if you want some happiness contact on given number.</Text>
                  </View>
                </View>
                <View style={styles.secndBg}>
                  <View style={[styles.txtBg, {marginTop:10}]}>
                    <Text style={styles.offer_txt}>We will send you happiness at your doorstep</Text>
                    <Text style={styles.sendOrdr}>Cupcakes starting onwards 140Rs.</Text>
                  </View>
                  <View style={[styles.txtBg,styles.contactLeft]}>
                    <Text style={styles.contact_txt}>Contact : 9892092484</Text>
                  </View>
                </View>
                {/* <View style={styles.thirdBg}>
                  <View style={styles.txtBg}>
                    <Text style={styles.contact_txt}>Contact : 9892092484</Text>
                  </View>
                </View> */}
              </ImageBackground>
              </View>
              
          </View>
        </ScrollView>
      </View>
       <Footer />
       <MobileFooter />
       </View> 
  );
}

const base = {
  topBg:{
    position: 'absolute', top: 0, left: 0, right: 0, bottom: 0,alignItems: 'center'
  },
  secndBg:{
    position: 'absolute', top: 80, left: '50%', right: 0, bottom: 0,alignItems: 'center'
  },
  thirdBg:{
    position: 'absolute',  left: '50%', right: 0, bottom: 100,alignItems: 'center'
  },
  contactLeft :{
    marginLeft: 120,
  },
  txtBg:{
    backgroundColor:'#cccccc78',
    borderTopWidth:1,
    borderBottomWidth:1,
    borderColor:theme.grey,
    marginVertical:8,
    paddingHorizontal:20
  },
  cupck_main:{
    fontSize:theme.size16,
    color: theme.white,
    fontFamily: 'MontserratSemiBold',
    textAlign:'center',
  },
  contact_txt:{
    fontSize:theme.size20,
    color: theme.ogBlack,
    fontFamily: 'MontserratSemiBold',
    textAlign:'center',
  },
  offer_txt:{
    fontSize:theme.size16,
    color: theme.ogBlack,
    fontFamily: 'MontserratSemiBold',
    textAlign:'center',
  },
  sendOrdr:{
    fontSize:theme.size16,
    color: theme.white,
    fontFamily: 'MontserratSemiBold',
    textAlign:'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 200,
    flexGrow:1,
    backgroundColor: '#f9f9f9',
    zIndex: -15,
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
  termsTxt :{
    fontSize:theme.size16,
    color: theme.darkbg,
    fontFamily: 'MontserratRegular',
    textAlign:'left',
  },
  imgWrap:{
    height:500
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: { 
        paddingHorizontal: 0,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
      cupck_main:{
        fontSize:theme.size11,
      },
      contact_txt:{
        fontSize:theme.size11,
      },
      offer_txt:{
        fontSize:theme.size11,
      },
      txtBg:{
        marginVertical:5,
        paddingHorizontal:5
      },
      sendOrdr:{
        fontSize:theme.size11, 
      },
      secndBg:{
        position: 'absolute', top: 65, left: 0, right: 0, bottom: 0,alignItems: 'center'
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
      cupck_main:{
        fontSize:theme.size11,
      },
      contact_txt:{
        fontSize:theme.size11,
      },
      offer_txt:{
        fontSize:theme.size11,
      },
      sendOrdr:{
        fontSize:theme.size11, 
      },
      txtBg:{
        paddingHorizontal:10,
        paddingHorizontal:5
      },
      contactLeft :{
        marginLeft: 50,
      },
      secndBg:{
        position: 'absolute', top: 80, left: '40%', right: 0, bottom: 0,alignItems: 'center'
      },
    })
  ),
  minWidth(
    992,
    maxWidth(1199, {
      pageArea: {
        paddingHorizontal: 100,
      },
    })
  )
);
