import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform,
} from "react-native";
import { useForm, Controller } from 'react-hook-form';
import { Row, Col } from 'react-native-responsive-grid-system';
const isWeb = Platform.OS === 'web';
const { height, width } = Dimensions.get('window');
import { GetRequestFunction,PostRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import {getAsyncStorageFunction} from "./services/CookieModule";
import { Link,useParams,useHistory } from '../Route/routing';
import { useShow } from "./services/AlertHook";
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const theme = {
   smallFont:10,
   largeFont:12,
   checkboxHW:13,
   backColor:'white',
   primaryFontColor:"white",
   primaryGrey: '#A5A7AB',
   primaryBlack:"black",
   primaryRed:"#FF1E1E",
}

export default function ProfileEdit(){
        const { show, error, success } = useShow();
        let history = useHistory();
        const [datas,setDatas] = useState({});
        const { handleSubmit, control, errors,reset } = useForm({});

        useEffect(() => {
            getBasicInformation();
        },[]);

        useEffect(() => {
            reset(datas)
        },[datas]);


        async function getBasicInformation()
        {
          let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
          if(mixdelight_token)
          {
            let basicInfo = null;
            let header = { Token: mixdelight_token };
            let basicInfoUrl = getApiurl("loginBasicInfo");
            let getInfo = await GetRequestFunction(basicInfoUrl, header, {});
            if (getInfo.status)
            {
              basicInfo = getInfo.data[0];
              basicInfo['email'] = basicInfo['email_id'];
              basicInfo['mobile'] = basicInfo['mobile_no'];
              console.log(basicInfo);
              setDatas(basicInfo);
            }
          
          }else
          {
            history.push('/signup');
          }  
        }


        const onSubmit = async(data) => {
            let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
            if(mixdelight_token)
            {
                let header = { Token: mixdelight_token };
                let param = data;
                let updateUserProfileUrl = getApiurl("updateUserProfile");
                let profiledata = await PostRequestFunction(updateUserProfileUrl,header,param);

                if(profiledata.status)
                {  
                    success('Profile update successfully')
                    //history.goBack();
                    history.push('/profile')
                }else{
                    error(profiledata.message)
                    history.push('/profile')
                   
                }
                
            }else
            {
                history.push('/signup');
            }


        };

        return (
    <View style={{height:'100%'}}>
    <MobileHeader />
    <Header />
      <View style={styles.pageArea}>
        <ScrollView>
        <Row>
            <Col xs={12} sm={12} md={2} lg={3} xl={3}></Col>
            <Col xs={12} sm={12} md={8} lg={6} xl={6}>
            <View>
            <ScrollView style={styles.SafeAreaViewstyle}>
                <View style={styles.formView}>
                    <View>
                        <TouchableOpacity
                            style={styles.otpButtonContainer}
                            onPress={null}
                        >
                        <Text style={styles.HeadingTxt}>
                            Edit Your Basic Info
                        </Text>
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.formViewBox,styles.borderWidthStyle]}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                        <TextInput style={[styles.input,styles.borderWidthStyle,styles.disable]}
                    
                        onBlur={onBlur}
                        editable={false}
                        placeholder="Email"
                            onChangeText={(value) => onChange(value)} value={value}/>
                        )}
                        name="email"
                        rules={{ required: true }}
                        />
                        {errors.email && <Text style={styles.errorText}>This is required.</Text>}

                        <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                                placeholder="First name" 
                                onChangeText={(value) => onChange(value)} value={value}/>
                                )}
                            name="fname"
                            rules={{ required: true }}
                        /> 
                        {errors.fname && <Text style={styles.errorText}>This is required.</Text>}
                        <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                                placeholder="Last name" 
                                onChangeText={(value) => onChange(value)} value={value}/>
                                )}
                            name="lname"
                            rules={{ required: true }}
                        /> 
                        {errors.lname && <Text style={styles.errorText}>This is required.</Text>}

                       <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                            placeholder="Mobile Number"
                                onChangeText={(value) => onChange(value)} value={value}/>
                            )}
                            name="mobile"
                            rules={{ required: "This is required.", pattern:[{pattern:/^\d+$/,message:'Must be 10 digit long'}], 
                            validate: {
                                    // /
                                    length: (value) => value && value.toString().length === 10 || 'Must be 10 digit long.', 
                                    digits: (value) => value && /^[-+]?\d*$/.test(value) || 'Expected all integer.',
                                }
                            }  }
                        /> 
                        {errors.mobile && <Text style={styles.errorText}>{errors.mobile.message}</Text>}
                    </View>
                    <TouchableOpacity style={[styles.cabinetButton,styles.cabinetBack]}
                        onPress={handleSubmit(onSubmit)}>
                        <Text style={[styles.cabinetButtonText,styles.callnowtext]}>
                          Change Basic Information
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
            </View>
            </Col>
            <Col xs={12} sm={12} md={2} lg={3} xl={3}></Col>
        </Row>
        </ScrollView>
      </View>
      <Footer />
    <MobileFooter />
    </View>    
        );
   
}

const styles = StyleSheet.create({

    pageArea:{
        flexGrow:1,
      },

    viewWidth:{
        width:300
    },
    formView:{
      paddingLeft:15,
      paddingRight:15,
      marginBottom:45
    },
    borderWidthStyle:{
        borderWidth:1,
        borderStyle:'solid',
        borderColor:theme.primaryGrey
    },
    disable:
    {
        backgroundColor:'lightgray'
    },
    formViewBox:{
        padding:12,
        marginBottom:20,
    },
    SafeAreaViewstyle:{
        padding:10,
        marginTop: isWeb?20:60,
        backgroundColor : theme.backColor,
    },
    input: {
        borderRadius: 4,
        height:40,
        marginBottom:10,
        paddingLeft:10
      },
    pickerBox: {
        paddingTop:0,
        borderRadius: 4,
        height:50,
        marginBottom:10,
    },      
    otpButtonContainer: {
        backgroundColor: theme.primaryBlack,
        height:45,
        borderTopLeftRadius:6,
        borderTopRightRadius:6,
        alignItems:'center',
        justifyContent:'center',
        paddingBottom:isWeb?0:20
        

    },
    HeadingTxt:{
        marginLeft:15,
        top:12,
        fontSize:14,
        color:theme.backColor,
        fontWeight: "bold"
    },
    cabinetButton:{
        height:45,
        width:"100%",
        paddingVertical: 12,
        paddingLeft:15
     },
      cabinetButtonText: {
      fontSize: 15,
      fontWeight:"bold",
      justifyContent:"center",
      color:"white"
    },
    cabinetBack:{
        backgroundColor: theme.primaryRed,
      },
    errorText:{
        color:theme.primaryRed,
        marginBottom:5
    }  
  

});