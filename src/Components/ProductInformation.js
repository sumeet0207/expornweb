import React, { useState, useEffect } from "react";
import {
  ScrollView,
  Text,
  View,
  Button,
  TextInput,
  Dimensions,
  Platform,
  Modal,
  SafeAreaView,
  BackHandler
} from 'react-native';
import { Link,useParams,useHistory } from '../Route/routing';
import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import TitleText from '../Components/atom/TitleText';
import Price from '../Components/atom/Price';
import AmountDisc from '../Components/atom/AmountDisc';
import Rating from '../Components/atom/Rating';
import BtnWithIcon from '../Components/atom/BtnWithIcon';
import DetailImageMolecule from '../Components/molecule/DetailImageMolecule';
import SubmitButton from '../Components/atom/SubmitButton';
import RadioComp from '../Components/atom/RadioComp';
import DateButton from '../Components/atom/DateButton';
import ProductName from '../Components/atom/ProductName';
import SlotMolecule from '../Components/molecule/SlotMolecule';

import ReviewSection from '../Components/molecule/ReviewSection';
import { Input, Image } from 'react-native-elements';
import { GetRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import NewCustumCollapse from './atom/NewCustumCollapse';
import {valueByPercentage,valueByPrice,getPercentageChange} from './services/DiscountService';
import CalenderRN from '../Components/Uncommon/Calender';
import RNSelect from '../Components/Uncommon/Select';
import {CheckSlotForDelivery,whenYouOrdering} from './services/CheckSlotService';
import WebModal from 'react-modal';
import AccessoriesList from './AccessoriesList';
import { getAsyncStorageFunction } from "./services/CookieModule";
import { CheckValidPincode } from "./services/CommonModule";
import ProductImg from './atom/ProductImg';
import DateTimePicker from '@react-native-community/datetimepicker';
import SlotItem from './atom/SlotItem';
import { useShow } from "./services/AlertHook";
// import CloseModal from './atom/CloseModal';

import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import CloseModal from './atom/CloseModal';

const { width, height } = Dimensions.get('window');

const isWeb = Platform.OS === 'web';
isWeb?WebModal.setAppElement('#root'):null;

export default function ProductInformation() {
  const { showw, error, success } = useShow();
  const [pincodeval, getPincode] = useState('');
  const [pinError, setPincodeError] = useState('Currently we only deliver in Thane area better to check pincode');


  const [cakeinfo, setCakeInfo] = useState('');
  const [weightPrice, setWeightPrice] = useState([]);
  const [avgRating, setAvgRating] = useState(0);
  const [allreview, setAllReview] = useState([]);
  const [allaccessories, setAllAccessories] = useState([]);
  //price
  const [baseAmount, setBaseAmount] = useState(0);
  const [discount, setDiscount] = useState(0);
  const [distype, setDiscountType] = useState(1);
  const [finalAmount, setFinalAmount] = useState(0);
  //type & shape
  const [isHShape, setHShape] = useState(false);
  const [isEggless, setEggless] = useState(false);
  const [selectedWeight, setSelectedWeight] = useState('');
  const [shapeextra, setShapeExtra] = useState(0);
  const [eggExtra, setEggExtra] = useState(0);
  //date and slot
  const [deliverDate, setDeliverDate] = useState(new Date());
  const [minDeliverDate, setMinimumDeliverDate] = useState(new Date());
  const [deliverySlot, setDeliverySlot] = useState(0);
  const [deliverySlotLabel, setDeliverySlotLabel] = useState('');
  const [calenderModal, setCalenderModal] = useState(false);
  const [finalSlot, setSlot] = useState([]);
  const [modalIsOpen,setIsOpen] = useState(false);
  const [mszcake,setMszOnCake] = useState('');
  const [show, setShow] = useState(false);
  const [slotModal, openSlotModal] = useState(false);
  const [isActiveProduct, setIsActiveProduct] = useState(1);

  let { cakeurl } = useParams();
  let isInstant = 0;
  //let isActiveProduct = 0;
  let history = useHistory();
  

  const customStyles = {
    content : {
      top                   : '2%',
      left                  : '2%',
      right                 : '2%',
      bottom                : '2%',
      padding:'1%'
    }
  };

  useEffect(() => {
    if(cakeurl)
    {
      getAllInformation(cakeurl);
    }
    handleBackHnadler();
  },[]);

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      // if(calenderModal || modalIsOpen || slotModal || show)
      // {
      //   console.log('hshshshh')
      //   closeAllModal();
      // }else
      // {
      //   history.goBack();
      // }
      return true;

    };

    function closeAllModal()
    {
      setShow(false);
      openSlotModal(false);
      setCalenderModal(false);
      setIsOpen(false);
    }

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }

  async function getAllInformation(cakeid)
  {
    let param = { pid: cakeid };
    let getcakeinfoUrl = getApiurl("productDetails");
    let getCakeInfo = await GetRequestFunction(getcakeinfoUrl, {}, param);
    console.log(getCakeInfo)
    if(getCakeInfo.status)
    {
      let data = getCakeInfo.data;
      let product_detail = data.product_detail;
      let weight_price = data.weight_price;
      setCakeInfo(product_detail);
      isInstant = product_detail.is_instant_deliver;
      //isActiveProduct = product_detail.is_active;
      setIsActiveProduct(product_detail.is_active);
      //setIsInstant(product_detail.is_instant_deliver);
      //console.log(data)
      //let Load Price
      let amtshow = calculate(weight_price[0].base_amount,weight_price[0].discount_amount,weight_price[0].discount_type);
      setPriceSection(weight_price[0].base_amount,weight_price[0].discount_amount,weight_price[0].discount_type,amtshow);
      setWeightPrice(data.weight_price);
      setAvgRating(data.cake_rating.avgRating);
      setAllReview(data.cake_review);
      setEggExtra(product_detail.eggless_extra_base_price)
      setShapeExtra(product_detail.extra_shape_basic_amount);
      var date = new Date()
      var tomarrow =new Date(new Date(date).getTime() + 60 * 60 * 24 * 1000);
      let slotarry=[];
      console.log(isInstant)
      if(isInstant!==1)
      {
        slotarry = CheckSlotForDelivery(false,tomarrow);
        setDeliverDate(tomarrow)
        setMinimumDeliverDate(tomarrow)
      }else
      {
         let yorLuck = whenYouOrdering();
         if(yorLuck==3)
         {
            slotarry = CheckSlotForDelivery(false,tomarrow);
            setDeliverDate(tomarrow)
            setMinimumDeliverDate(tomarrow)
         }else
         {
           slotarry = CheckSlotForDelivery(true,date);
         }
      }
      setSlot(slotarry)
      //console.log(slotarry);
      let accss = await getAllAccessories();
      
    }
  }

  async function getAllAccessories()
  {
     let getallAccesUrl = getApiurl("getlistOfAccessories");
     let getAllAccessData = await GetRequestFunction(getallAccesUrl, {}, {});
    //allaccessories, 
     if(getAllAccessData.status)
     {
       setAllAccessories(getAllAccessData.data);
     }
  }

  function setPriceSection(baseamt,disc,disctype,finalamt)
  {
    setFinalAmount(finalamt);
    setDiscount(disc);
    setDiscountType(disctype);
    setBaseAmount(baseamt);
  }

  function priceFilter(weight=selectedWeight,shape=isHShape,type=isEggless)
  {
    let showamt=0;
    let base_amount=0;
    let discount_amount=0;
    let discount_type=0;

    let weightPriceFilter = weightPrice.filter(function (itm) {
      return itm.weight == weight
    });

    showamt = calculate(weightPriceFilter[0].base_amount,weightPriceFilter[0].discount_amount,
      weightPriceFilter[0].discount_type)

    base_amount = weightPriceFilter[0].base_amount;
    discount_amount = weightPriceFilter[0].discount_amount;
    discount_type = weightPriceFilter[0].discount_type;
    
    if(shape)
    {
      let newbaseamt = base_amount + shapeextra;
      let newshwamt = showamt + shapeextra;
      if(discount_type==2)
      {
        discount_amount = getPercentageChange(newbaseamt,newshwamt);
      }
      base_amount = newbaseamt;
      showamt = newshwamt;
    }

    if(type)
    {
      let newbaseamt = base_amount + eggExtra;
      let newshwamt = showamt + eggExtra;
      if(discount_type==2)
      {
        discount_amount = getPercentageChange(newbaseamt,newshwamt);
      }
      base_amount = newbaseamt;
      showamt = newshwamt;
    }
    setPriceSection(base_amount,discount_amount,discount_type,showamt);

  }

  function calculate(baseamt,disc,disctype)
  {
    let amountShow=baseamt;
    if(disctype && disctype!==0)
    {
       if(disctype==1)
       {
          amountShow = valueByPrice(baseamt,disc);
       }else if(disctype==2)
       {
          amountShow = valueByPercentage(baseamt,disc);
       }
    }
    return amountShow;
  }

  const onChange = (event, selectedDate) => {
   
    const currentDate = selectedDate;
    setShow(Platform.OS === 'ios');
    if(currentDate)
    { 
      let getSlot = CheckSlotForDelivery(isInstant,currentDate);
      setSlot(getSlot)
      setDeliverDate(currentDate)
      setDeliverySlot(0)
      setDeliverySlotLabel('');
    }
    
  
  };

  function getDeliverDate(selectedDate)
  {
    let getSlot = CheckSlotForDelivery(isInstant,selectedDate);
    setSlot(getSlot)
    setShow(true);
    setDeliverDate(selectedDate)
    setCalenderModal(false);
    setDeliverySlot(0)
    setDeliverySlotLabel('');
  }

  function onChangeSlot(slotSelect)
  {
    setDeliverySlot(slotSelect)
  }
  
  async function openAccessModal()
  {
    if(deliverySlot < 1)
    {
        error('Please select slot !')
        return false;
    }
    let token = await getAsyncStorageFunction('mixdelight_token');
    console.log(token)
    if(token)
    {
      setIsOpen(true);
    }else
    {
      history.push("/login");
    }
    
  }

  function validatePincode(pin) {
    const re = /^[0-9]+$/;
    if(pin.match(re))
    {
        return true;
    }else
    {
       return false;
    }
  }

  async function checkPincode()
  {
    console.log(pincodeval)
    if (pincodeval === '') {
      setPincodeError('Pincode is required');
      return false;
    }

    if (validatePincode(pincodeval)===false) {
      setPincodeError('Pincode must be an integer');
      return false;
    }

    if (pincodeval.length!==6) {
      setPincodeError('Pincode must be 6 character long');
      return false;
    }

    let checkResp = await CheckValidPincode(pincodeval)
    // let header={};
    // let pincodeValidationUrl = getApiurl("pincodeValidation");
    // let pinValidationRes = await GetRequestFunction(`${pincodeValidationUrl}/${pincodeval}`, header);
    if(checkResp)
    {
      setPincodeError('We deliver to your pincode..!!!');
    }else
    {
      setPincodeError('Sorry, we do not deliver there');
    }
  }

  function enterPincode(pinval)
  {
      
      if(pinval.length==0)
      {
        setPincodeError('Currently we only deliver in Thane area better to check pincode');
      }
      getPincode(pinval)
  }

// }

  return (
      <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView style={styles.container}>
          
        <Row style={{zIndex:-10}}>
          <Col xs={12} sm={12} md={6} lg={6} xl={6}>
            <View style={styles.leftWrap}>
              {cakeinfo.images?<DetailImageMolecule imgArray = {cakeinfo.images || []}/>:
               <ProductImg ImgHeight={400} mb_ImgHeight={400} imgsrc=''/>}
              <View style={{ paddingVertical: 0 }}>
                {/* <TitleText
                  title="NOTE: Design and icing of cake may vary from the image shown here since each chef has his/her own way of baking and designing a cake."
                  titleSize={theme.size12}
                  mb_titleSize={theme.size12}
                  titleAlign={'left'}
                  titleBotm={0}
                /> */}
                <View>
                  <Image
                    source={require('../assets/img/hygienic-desktop.png')}
                    resizeMode="contain"
                    style={styles.hygienImg}
                    // style={{
                    //   height: 150,
                    //   width: '60%',
                    //   marginTop: 20,
                    // }}
                  />
                </View>
              </View>
            </View>
          </Col>

          <Col xs={12} sm={12} md={6} lg={6} xl={6}>
            <View style={styles.rightWrap}>
              <View>
                <View
                  style={{
                    flexDirection: 'row',
                    paddingVertical: 10,
                  }}
                >
                  <View style={styles.availFlex}>
                    <ProductName
                      pname={cakeinfo.name?cakeinfo.name:''}
                      pnameSize={theme.size20}
                      mb_pnameSize={theme.size20}
                      pnameAlign={'left'}
                      pnameBotm={0}
                    />
                    <View
                      style={{
                        paddingTop: 5,
                        flexDirection: 'row',
                        alignItems: 'center',
                      }}
                    >
                      <Price price={finalAmount} titleAlign={'left'} />
                      <View style={{ paddingHorizontal: 10 }}>
                        {distype!==0?<AmountDisc
                          amount={baseAmount}
                          disc={discount}
                          distype={distype}
                          titleAlign={'flex-start'}
                        />:null}
                      </View>
                    </View>
                  </View>
                </View>
                {/* accordian start */}
                <View style={{marginBottom:20}}>
                  <NewCustumCollapse heading='Product Details' type={1} vendorname={cakeinfo.vendorname}
                  flavourname={cakeinfo.flavourname}
                  shape={cakeinfo.available_in_shape}
                  eggtype={cakeinfo.available_in_type}
                  />
                  {cakeinfo.product_details?<NewCustumCollapse type={2} heading='Product Description' detail={cakeinfo.product_details}/>:null}
                  {cakeinfo.care_instruction?<NewCustumCollapse type={3} heading='Care Instructions' detail={cakeinfo.care_instruction}/>:null} 
              </View>
              {/* accordian end */}
              </View>
    
              <TitleText
                title="Select Weight"
                titleSize={theme.size18}
                mb_titleSize={theme.size15}
                txtColor={theme.ogBlack}
                titleAlign={'left'}
              />

              <View style={{ flexDirection: 'row' }}>
              {weightPrice.length > 0?<RadioComp weightPrice={weightPrice} 
               priceFilterFunction={priceFilter}
               selectedWeight={selectedWeight}
               setSelectedWeight={setSelectedWeight}/>:null}
              </View>
              <View style={{ flexDirection: 'row', marginBottom: 20 }}>
                {cakeinfo.available_in_type===3?
                 <BtnWithIcon Mtext="Eggless" 
                  weight={selectedWeight} checktype={1} isHShape={isHShape}
                  Micon="checkbox-intermediate" priceFilter={priceFilter}
                  setIsActive={setEggless} isActive={isEggless}/>:null}
                {cakeinfo.available_in_shape===3?
                 <BtnWithIcon Mtext="Heart Shape" weight={selectedWeight}
                 priceFilter={priceFilter} checktype={2} isEggless={isEggless}
                 Micon="heart-box-outline" setIsActive={setHShape} isActive={isHShape}/>:null}
              </View>

              <Row>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                  <TextInput
                    style={{
                      borderColor: theme.grey2,
                      borderWidth: 1,
                      width: null,
                      borderRadius: 5,
                      padding: 5,
                      marginBottom: 20,
                      backgroundColor:theme.white
                    }}
                    multiline
                    numberOfLines={4}
                    editable
                    maxLength={10}
                    placeholder="Message on Cake upto 10 character long"
                    onChangeText={text => setMszOnCake(text)}
                  />
                </Col>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                  <Row>
                    <Col xs={12} sm={12} md={6} lg={12} xl={12}>
                      {isWeb?<CalenderRN modalIsOpen={calenderModal} 
                       minimumDate={minDeliverDate}
                       onChange={getDeliverDate} date={deliverDate}/>:null}
                      {show && (
                          <DateTimePicker
                            testID="dateTimePicker"
                            value={deliverDate}
                            mode={'date'}
                            display="default"
                            minimumDate={minDeliverDate}
                            onChange={onChange}
                          />
                        )}
                      <DateButton 
                       onClick={isWeb?()=>setCalenderModal(true):()=>setShow(true)} date={deliverDate}/>
                    </Col>
                    <Col xs={12} sm={12} md={6} lg={12} xl={12}>
                      <View style={{ marginTop: 6 }}>
                        <SubmitButton 
                         btnTxt={deliverySlot < 1 ?'Choose Slot': deliverySlotLabel}
                         onPress={()=>openSlotModal(true)}
                         Colorbg={theme.white}
                         sbtnbrdrColor={theme.grey2}
                         subBtncolor={theme.Hash666}
                         sbtntxtSiz={theme.size14}
                         mb_sbtntxtSiz={theme.size14}/>
                      </View>
                    </Col>
                  </Row>
                </Col>
              </Row>

              <View style={{ marginTop: 15 }}>
                <TitleText
                  title="Deliver To"
                  titleSize={theme.size18}
                  mb_titleSize={theme.size15}
                  txtColor={theme.ogBlack}
                  titleAlign={'left'}
                />
              </View>

              <TitleText
                title={pinError}
                txtColor={theme.green}
                titleSize={theme.size12}
                mb_titleSize={theme.size12}
                titleAlign={'left'}
              />

              <Row>
                <Col xs={12} sm={12} md={9} lg={9} xl={9}>
                  <TextInput
                    style={{
                      height: 40,
                      borderColor: '#ccc',
                      borderWidth: 1,
                      width: null,
                      borderRadius: 5,
                      padding: 5,
                    }}
                    placeholder="* Enter Pincode"
                    onChangeText={text => enterPincode(text)}
                  />
                </Col>
                <Col xs={12} sm={12} md={3} lg={3} xl={3}>
                  <View style={styles.checkpin}>
                    <SubmitButton btnTxt="check" Colorbg={theme.green} onPress={checkPincode}
                        Colorbg={theme.white}
                        sbtnbrdrColor={theme.green}
                        subBtncolor={theme.green}
                        sbtntxtSiz={theme.size14}
                      />
                  </View>
                </Col>
              </Row>

              <Row>
                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                  <View style={{ paddingVertical: 10 }}>
                      <SubmitButton 
                       btnTxt={isActiveProduct==1?'Add to cart':'Out of stock'}
                       Colorbg={isActiveProduct==1?null:theme.grey2}  
                       onPress={isActiveProduct==1?openAccessModal:null}/>
                    </View>
                </Col>
              </Row>
            </View>
          </Col>
        </Row>

        {/* start MOdal */}
        <View>
          {isWeb?<WebModal isOpen={modalIsOpen} style={customStyles}>
           
           <AccessoriesList allaccessories={allaccessories}
              deliverDate={deliverDate} Weight={selectedWeight}
              slot={1} mszcake={mszcake} isHShape={isHShape} 
              isEggless={isEggless} weightPrice={weightPrice}
              setIsOpen={setIsOpen} deliverySlot={deliverySlot}
            />
          </WebModal>:<Modal visible={modalIsOpen} animationType="slide">
           <View>
               
               <AccessoriesList allaccessories={allaccessories} slot={1}
                cakeid={cakeinfo.id} Weight={selectedWeight} mszcake={mszcake} 
                deliverDate={deliverDate} isHShape={isHShape} weightPrice={weightPrice}
                isEggless={isEggless} setIsOpen={setIsOpen} deliverySlot={deliverySlot}/>
           </View>
          </Modal>}

          {/* Slot modal */}
          {isWeb?<WebModal isOpen={slotModal} style={base}>
          <CloseModal closeTop={-14} closeRight={-12} onPress={()=>openSlotModal(false)}/>
          <SlotItem slotArray={finalSlot} setDeliverySlotLabel={setDeliverySlotLabel}
           setDeliverySlot={setDeliverySlot} deliverySlot={deliverySlot} 
           openSlotModal={openSlotModal}/>
        </WebModal>:<Modal visible={slotModal} animationType="slide" style={base}>
          <View>
              <CloseModal closeTop={20} closeRight={20} onPress={()=>openSlotModal(false)}/>
              <View style={{paddingVertical:50}}>
              <SlotItem slotArray={finalSlot} setDeliverySlotLabel={setDeliverySlotLabel}
              deliverySlot={deliverySlot} setDeliverySlot={setDeliverySlot} 
              openSlotModal={openSlotModal}/>  
              </View>
          </View>

        </Modal>}
        
        </View>
        {/* End MOdal */}
      
        </ScrollView>
      </View>
      <Footer />
        <MobileFooter />
      </View>  
  );
}


const base = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop:10,
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 50,
    flexGrow:1,
    zIndex:-15,
    // paddingBottom:isWeb? 20:60
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
  hygienImg: {
    // display: 'block',
    height: 150,
    width: '60%',
    marginTop: 20,
  },
  content : {
    width: 'fit-content',
    height: 'fit-content',
    // margin: 'auto',
    transform: 'translate(-50%, -50%)',
    top:'50%',
    left:'50%',
    paddingTop: 20
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 15,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
      checkpin: {
        marginTop: 10,
      },
      hygienImg: {
        display: 'none',
      },
      content : {
        width: '100%',
        height: 200,
      }
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
      checkpin: {
        marginTop: 10,
      },
      hygienImg: {
        display: 'none',
      },
    })
  )
);
