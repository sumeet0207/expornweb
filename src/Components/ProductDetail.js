import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Link } from '../Route/routing';

export default function ProductDetail() {
  return (
    <View style={styles.container}>
      <Text>Detail Page</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
