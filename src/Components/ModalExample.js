// import React, {useState} from 'react';
// import { StyleSheet, Text, View,Button,Modal,Platform } from 'react-native';
// import WebModal from 'react-modal';
// import DateTimePicker from '@react-native-community/datetimepicker';
// import AccessoriesList from './AccessoriesList';
// const isWeb = Platform.OS === 'web';

// isWeb?WebModal.setAppElement('#root'):null;
// export default function ModalExample() {
//   const [modalIsOpen,setIsOpen] = useState(false);
//   const customStyles = {
//     content : {
//       top                   : '2%',
//       left                  : '2%',
//       right                 : '2%',
//       bottom                : '2%',
//       padding:'1%'
//     }
//   };
 
//   return (
//     <View style={styles.container}>
//         {isWeb?<WebModal isOpen={modalIsOpen} style={customStyles}>
           
//           <AccessoriesList/>
//           {/* <View style={{position: 'absolute',bottom: 0,backgroundColor:'red', width: '100%', height: 50}}>
            
//           </View> */}
           

//         </WebModal>:<Modal visible={modalIsOpen} animationType="slide">
//           <View>
//               <AccessoriesList/>
//           </View>

//         </Modal>}
//          <View>
//          <Button  onPress={()=>setIsOpen(true)} title="Open" color="#841584"/>
//          </View>
//     </View>
    
 
//   );
// }

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     ".Modal":{
//       backgroundColor:'red'

//     }
// });

import React, {useState} from 'react';
import { StyleSheet, Text, View,Button,Modal,Platform } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
 
export default function ModalExample() {
  const [date, setDate] = useState(new Date()); 
  const [show, setShow] = useState(false);
 
  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };
 
  const showMode = (currentMode) => {
    setShow(true);
  };
 
  const showDatepicker = () => {
    showMode('date');
  };
  
  return (
    <View style={styles.container}> 
      <View >
        <Button onPress={showDatepicker} title="Show date picker!" />
      </View>
      <View>
      <Text>{date.toLocaleDateString()}</Text>
      </View>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={'date'}
          display="default"
          minimumDate={new Date()}
          onChange={onChange}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

// import React, {useState} from 'react';
// import { StyleSheet, Text, View,Button,Modal,Platform } from 'react-native';
// import Calendar from "react-calendar";
// import 'react-calendar/dist/Calendar.css';
// import WebModal from 'react-modal';
 
// export default function ModalExample() {
//   const [date, setDate] = useState(new Date()); 
//   const [modalIsOpen,setIsOpen] = useState(false);
  
//   const onChange = (selectedDate) => {
//        setDate(selectedDate);
//   };

//   return (
//     //https://youtu.be/T7lYAsC4iUA
//     <View style={styles.container}> 
//       <View >
//         <Button onPress={setIsOpen} title="Show date picker!" />
//       </View>
//       <View>
//       <Text>{date.toLocaleDateString()}</Text>
//       </View>
//       <WebModal  isOpen={modalIsOpen}>
//         <Calendar showWeekNumbers onChange={onChange} value={date} minDate={new Date()}/>
//       </WebModal>
       
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         justifyContent: 'center',
//         alignItems: 'center',
//     },
//     tile:{
//       backgroundColor:'red'
//     }
// });