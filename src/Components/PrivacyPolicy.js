import React from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Image, Platform } from 'react-native';
import { Link, useLocation } from '../Route/routing';

import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import OrderMolecule from './molecule/OrderMolecule';
import TitleText from './atom/TitleText';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function TermsOfUse() {
  let location = useLocation();
  console.log(location.pathname);
  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View style={{ padding: 20, flexGrow:1, backgroundColor:theme.white }}>
              <View style={{ marginVertical:10 }}>
                <TitleText
                  title="Privacy Policy"
                  txtColor={theme.darkbg}
                  titleAlign={'center'}
                  titleSize={theme.size20}
                  mb_titleSize={theme.size20}
                  titleFont={'MontserratSemiBold'}
                />
              </View>
              <View style={styles.termsTxt}>
                <ul>
                  <li style={styles.liBtm}>The types of information that Mix Delight may collect from you when you access or use its websites, applications and other online services, its practices for collecting, using, maintaining, protecting and disclosing that information.</li>
                  <li style={styles.liBtm}>This policy may change from time to time, your continued use of our Services after it makes any change is deemed to be acceptance of those changes, so please check the policy periodically for updates.</li>
                  <li style={styles.liBtm}>We may occasionally update this Policy and such changes will be posted on this page. If we make any significant changes to this Policy, we will endeavour to provide you with reasonable notice of such changes, such as via a prominent notice on the Platform or to your email address</li>
                  <li style={styles.liBtm}>By using the Platform and the Services, you agree and consent to the collection, transfer, use, storage, disclosure and sharing of your information as described and collected by us in accordance with this Policy.  If you do not agree with the Policy, please do not use or access the Platform.</li>
                  <li style={styles.liBtm}>Information we collect about you 
                    <ul>
                      <li style={styles.liBtm}>When you communicate with us (via email, phone, through the Platform or otherwise), we may maintain a record of your communication.</li>
                      <li style={styles.liBtm}>Location information: Depending on the Services that you use, and your app settings or device permissions, we may collect your real time information, or approximate location information as determined through data such as GPS.</li>
                      <li style={styles.liBtm}>If you are a customer or a delivery partner, we will, additionally, record your calls with us made from the device used to provide Services, related call details, SMS details location and address details.</li>
                    </ul>
                  </li>
                  <li style={styles.liBtm}>USES OF YOUR INFORMATION
                    <ul>
                      <li style={styles.liBtm}>To provide, personalise, maintain and improve our products and services, such as to enable deliveries and other services, enable features to personalise your Mix Delight account.</li>
                      <li style={styles.liBtm}>To generate and review reports and data about, and to conduct research on, our user base and Service usage patterns.</li>
                      <li style={styles.liBtm}>To measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you.</li>
                      <li style={styles.liBtm}>To provide you with information about services we consider similar to those that you are already using, or have enquired about, or may interest you. If you are a registered user, we will contact you by electronic means (email or SMS or telephone) with information about these services.</li>
                    </ul>
                  </li>
                </ul>
              </View>
          </View>
        </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  // container: {
  //   flex: 1,
  //   backgroundColor: '#fff',
  // },
  liBtm : {
    marginBottom: 15
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 200,
    flexGrow:1,
    backgroundColor: '#f9f9f9',
    zIndex: -15,
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
  termsTxt :{
    fontSize:theme.size16,
    color: theme.darkbg,
    fontFamily: 'MontserratRegular',
    textAlign:'left',
  },
  imgWrap:{
    height:200
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 0,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
    })
  ),
  minWidth(
    992,
    maxWidth(1199, {
      pageArea: {
        paddingHorizontal: 100,
      },
    })
  )
);
