import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, Platform } from 'react-native';
import { Link } from '../Route/routing';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Checkbox from './Uncommon/Checkbox';
import Radiobutton from './Uncommon/RadioButton';
import RNSelect from './Uncommon/Select';
import AccessoriesBox from './atom/AccessoriesBox';
import { increment, decrement, reset } from "../store/action/LoginFlagAction";
import { useSelector, useDispatch } from "react-redux";

const isWeb = Platform.OS === 'web';

export default function Test() {
  const dispatch = useDispatch();
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [toggleRadio, setToggleRadio] = useState('1');
  const [selectedValue, setSelectedValue] = useState('');

  const reduxGuestCart = useSelector(state => state.LoginOverlapReducer.counter)

  let radio_props = !isWeb
    ? [
        { value: '0.5', text: '0.5' },
        { value: '1', text: '1' },
      ]
    : [
        { value: '0.5', label: '0.5' },
        { value: '1', label: '1' },
      ];

  let RnSelectoptions = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
  ];

  function isCheck() {
    setToggleCheckBox(!toggleCheckBox);
  }

  function getRadioval(val) {
    console.log(val);
    setToggleRadio(val);
  }

  const handleChange = (selectedOption) => {
    console.log(selectedOption);
    setSelectedValue(selectedOption);
  };

  // return (<View style={styles.container}>

  //     <Checkbox toggleCheckBox={toggleCheckBox} setToggleCheckBox={isCheck}/>

  //     <Radiobutton types={radio_props} val={toggleRadio} onPress={getRadioval}/>

  //     <RNSelect selectedValue={selectedValue} setSelectedValue={handleChange} options={RnSelectoptions}/>
  //     <Text>This is the Home screen Radio : {toggleRadio}</Text>
  //     <Text>This is the Home screen Select :</Text>
  //     <Link to="/cakeinfo/1"><Text>Cake info</Text></Link>
  //     <Link to="/profile"><Text>Profile</Text></Link>
  //     <Link to="/cakelisting"><Text>Cake list</Text></Link>
  //     <Link to="/grid"><Text>Grid</Text></Link>
  //     <Link to="/modal"><Text>Modal</Text></Link>
  //     <Link to="/signup"><Text>Signup</Text></Link>
  //     <Link to="/login"><Text>Login</Text></Link>
  //     <Link to="/accordainexp"><Text>Accordian</Text></Link>
  //     <Link to="/add/address"><Text>Add Address</Text></Link>

  function isCheck() {
    setToggleCheckBox(!toggleCheckBox);
  }

  function getRadioval(val) {
    console.log(val);
    setToggleRadio(val);
  }

  function doIncrement()
  {
    dispatch(increment())
  }

  function doDecrement()
  {
    dispatch(decrement())
  }

  return (
    <View style={styles.container}>
      <Button
        onPress={doIncrement}
        title="doIncrement"
        color="#841584"
      />

      <Button
        onPress={doDecrement}
        title="doDecrement"
        color="#841584"
      />
      <Checkbox toggleCheckBox={toggleCheckBox} setToggleCheckBox={isCheck} />

      <Radiobutton
        types={radio_props}
        val={toggleRadio}
        onPress={getRadioval}
      />
      <Text>{reduxGuestCart}</Text> 

      <Text style={{ fontFamily: 'Dancing' }}>
        This is the Home screen {toggleRadio}
      </Text>
      <Link to="/cakeinfo/1">
        <Text>Cake info</Text> 
      </Link>
      <Link to="/profile">
        <Text>Profile</Text>
      </Link>
      <Link to="/cakelisting">
        <Text>Cake list</Text>
      </Link>
      <Link to="/grid">
        <Text>Grid</Text>
      </Link>
      <Link to="/modal">
        <Text>Modal</Text>
      </Link>
      <Link to="/signup">
        <Text>Signup</Text>
      </Link>
      <Link to="/login">
        <Text>Login</Text>
      </Link>
      <Link to="/accordainexp">
        <Text>Accordian</Text>
      </Link>
      <Link to="/orders">
        <Text>Order Page</Text>
      </Link>
    </View>
  );
}

const base = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(400, {
      container: {
        backgroundColor: 'blue',
      },
    })
  )
);
