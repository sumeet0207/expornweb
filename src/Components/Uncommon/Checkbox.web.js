import React, {useState} from 'react';
import CheckBox from 'rc-checkbox';
import { View } from 'react-native';

export default function Checkbox(props)
{
    return(
        <View>
            <CheckBox
                checked={props.toggleCheckBox}
                onChange={(newValue) =>props.setToggleCheckBox(newValue)}
             />
        </View>


    )
}