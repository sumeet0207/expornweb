import Calendar from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import WebModal from 'react-modal';
import React, {useState} from 'react';
import {View} from 'react-native';
import CloseModal from '../../Components/atom/CloseModal';

export default function CalenderRN(props) {
    //http://reactcommunity.org/react-modal/
    const customStyles = {
        overlay:{

        },
        content : {
            width: 'fit-content',
            height: 'fit-content',
            // margin: 'auto',
            transform: 'translate(-50%, -50%)',
            top:'50%',
            left:'50%',
            paddingTop: 35
        //    left:'35%',
        //    height : '300px',
        //    width :'355px'
        },
        modalwrap : {
            padding:10
        }
      };

    const {modalIsOpen,onChange,date,minimumDate}=props;
    return (
        <View style={{alignItems:'center'}}>
            
            <WebModal isOpen={modalIsOpen} style={customStyles}>
            <CloseModal closeTop={-28} closeRight={-12} />
                <Calendar 
                showWeekNumbers 
                onChange={onChange} 
                value={date} 
                minDate={minimumDate}/> 
            </WebModal>
        </View>
    )
}