import { View, Text, Platform } from 'react-native';
import React, { useState } from 'react';
import RadioButtonGroup from 'react-radio-button-group';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';

const isWeb = Platform.OS === 'web';

export default function Radiobutton(props) {
  return (
    <View>
      {/* <RadioButtonGroup
        style={{ color: 'red' }}
        labelClassName={{ color: 'red' }}
        options={props.types}
        name="weight"
        value={props.val}
        onChange={(newValue) => props.onPress(newValue)}
        fireOnMount={false}
        row={true}
        buttonColor={'red'}
      /> */}
      <RadioForm
        radio_props={props.types}
        initial={0}
        // formHorizontal={true}
        labelHorizontal={true}
        buttonOuterSize= {isWeb?25:10}
        buttonSize={15}
        selectedButtonColor={'#f77800'}
        buttonColor={'#f77800'}
        animation={true}
        onPress={(newValue) => props.onPress(newValue)}
      />
    </View>
  );
}
