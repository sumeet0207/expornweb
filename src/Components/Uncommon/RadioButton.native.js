import { View, StyleSheet } from 'react-native';
import React, { useState } from 'react';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

export default function Radiobutton(props) {
  return (
    <View>
      <RadioForm
        radio_props={props.types}
        initial={0}
        formHorizontal={true}
        labelHorizontal={true}
        buttonOuterSize={23}
        buttonSize={12}
        selectedButtonColor={'#f77800'}
        buttonColor={'#f77800'}
        animation={true}
        onPress={(newValue) => props.onPress(newValue)}
      />
    </View>
  );
}

// const base = {
//   radiostyle: {
//       paddingHorizontal :5,
//   },
// };

// const styles = createStyles(
//   base,
//   minWidth(
//     100,
//     maxWidth(400, {

//       },
//     })
//   )
// );
