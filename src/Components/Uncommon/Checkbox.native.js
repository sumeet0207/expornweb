import CheckBox from '@react-native-community/checkbox';
import { View } from 'react-native';
import React, {useState} from 'react';

export default function Checkbox(props)
{
    return(
        <View>
            <CheckBox
                disabled={false}
                value={props.toggleCheckBox}
                onValueChange={(newValue) =>props.setToggleCheckBox(newValue)}
             />
        </View>


    )
}