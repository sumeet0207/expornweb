import { View} from 'react-native';
import React from 'react';
import {Picker} from '@react-native-community/picker';


export default function RNSelect(props){
    return(
        <View style={{borderWidth:1,borderColor:'#ff73e1',height: 50, width: 200,borderRadius:10}}>
            <Picker selectedValue={props.selectedValue}
                onValueChange={(itemValue) =>props.setSelectedValue(itemValue)}>
                {props.options.map((item, key)=>(
                    <Picker.Item label={item.label} value={item.value} key={key}/>)
                )}
            </Picker>
        </View>
    )
}
