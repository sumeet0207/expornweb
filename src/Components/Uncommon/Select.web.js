import React, { Component } from 'react'
import Select from 'react-select';

//custum style
//https://react-select.com/components
//https://react-select.com/styles
const styleee = {
  control: (css) => ({
    ...css,
    borderWidth:1,
    borderColor:'#ff73e1',
    width: 200,
    position:'relative'
  }),
  menu: ({ width, ...css }) => ({
    ...css,
    width: 200,
    color:'black',
    borderRadius: "1px solid black",
    backgrounColor:'red',
    borderBottom: '1px dotted pink',
    
  }),
  option: (css) => ({ ...css, paddingRight: 36 + 8 })
};

export default function RNSelect(props){
  const {options,selectedValue,setSelectedValue} = props;
  return(
    
      <Select options={options}
        placeholder="Select slot"
        styles={styleee}
        menuColor='red'
        value={selectedValue}
        onChange={(selectedOption )=>setSelectedValue(selectedOption)}/>
    )
}
