import React from 'react';
import {View,StyleSheet, Platform, SafeAreaView,ScrollView, Dimensions} from 'react-native';
import Nothing from './atom/Nothing';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const isWeb = Platform.OS === 'web';

const { width, height } = Dimensions.get('window');

export default function NotFound(props) {
  // let notlogin =1;
  // let emptycart=2;
  // let notfound=3;
  const {passurl='/',heading,btnName='CONTINUE SHOPPING',imgsrc} = props;
    return(
      <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View >
            <Nothing 
              imgsrc={imgsrc} 
              heading={heading}
              passurl={props.passurl}
              btnName={btnName}
              />
          </View>
          </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
       )
 }

const styles = StyleSheet.create({
  pageArea : {
    flexGrow: 1,
    marginVertical: isWeb? 0:60,
  }
});