import React, { useState, useEffect} from "react";
import { StyleSheet, Text, View, ScrollView, Dimensions,SafeAreaView,BackHandler, Platform,Linking } from 'react-native';
import { Link, useLocation,useHistory } from '../Route/routing';
import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import CheckoutAddress from '../Components/molecule/CheckoutAddress';
import CheckoutDetail from '../Components/molecule/CheckoutDetail';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { GetRequestFunction,PostRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import {getAsyncStorageFunction} from "./services/CookieModule";
import { useShow } from "../Components/services/AlertHook";
import { useDispatch, useSelector } from 'react-redux';
import { setCartCount } from "../store/action/cartAction";
import * as WebBrowser from 'expo-web-browser';

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function Checkout() {
  const { show, error, success } = useShow();
  const [canOrder, setCanOrder] = useState(false);
  const [shipAddress, getShippingAddress] = useState(0);
  const [gateway, getGetway] = useState(1);
  var abc= '';

  let location = useLocation();
  //console.log(location.pathname);
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    handleBackHnadler();
  },[]);

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }

  async function CashOnDelivery()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    if(mixdelight_token)
    {
      if(shipAddress < 1)
      {
        error('Please choose address with valid pincode');
        //console.log("choose address")
        return false;
      }

      let header = { Token: mixdelight_token };
      let param = {shipping_id : shipAddress, gateway: 2}
      let orderAddUrl = getApiurl("orderAdd");
      let placeResp = await PostRequestFunction(orderAddUrl,header, param);  
      if(placeResp.status)
      {
        success('Order Place !');
        dispatch(setCartCount(0));
        history.push('/orders')
      }else
      {
        error(placeResp.message);
        history.push('/cart');
      }

    }else
    {
      history.push('/');
    }
  }

  function callduddwe()
  {
    alert('ksksk')
  }

  async function razorPayPayment()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    if(mixdelight_token)
    {
      if(shipAddress < 1)
      {
        error('Please choose address with valid pincode');
        //console.log("choose address")
        return false;
      }

      let header = { Token: mixdelight_token };
      let param = {shipping_id : shipAddress, gateway: 1}

      

      let orderAddUrl = getApiurl("orderAdd");
      
      let placeResp = await PostRequestFunction(orderAddUrl,header, param);
      if(placeResp.status)
      {

        let getamt =  await getAmountCart();
        if(!getamt.cakeAmount)
        {
          return false;
        }
        let data = placeResp.data[0];
        let passt = 'pa';
        if(isWeb)
        {
          passt = 'bw'
        }
        let result = await WebBrowser.openAuthSessionAsync(`https://payment.mixdelight.com/c?order_id=${data.order_id}&token=${mixdelight_token}&razor_order_id=${data.rzr_order_id}&amount=${data.amount}&cakeamount=${getamt.cakeAmount}&acessamount=${getamt.accessoryAmount}&t=${passt}`)
        //let result = await WebBrowser.openAuthSessionAsync(`http://localhost:3000/c?order_id=${data.order_id}&token=${mixdelight_token}&razor_order_id=${data.rzr_order_id}&amount=${data.amount}&cakeamount=${getamt.cakeAmount}&acessamount=${getamt.accessoryAmount}&t=${passt}`)
        if(true)
        {
          // check status
            let getorderInfoUrl = getApiurl("getOrderInfo");
            let res = await GetRequestFunction(getorderInfoUrl, header, { order_id : data.order_id});
            if(res.status)
            {
              let data = res.data[0];
              if(data.delivery_status==1 && data.order_status==1 && data.payment_status==15)
              {
                success('Order Place !');
                dispatch(setCartCount(0));
                history.push('/orders')
              }else
              {
                //error('order failed');
              }
          }

        }else{
          
          // let getorderInfoUrl = getApiurl("checkWithRazor");
          // let res = await GetRequestFunction(getorderInfoUrl, header, { order_id : data.order_id});
          // console.log(res)
          // if(res.status)
          // {
          //   success('Order Place !');
          //   dispatch(setCartCount(0));
          //   history.push('/orders')

          // }else
          // {
          //       error('order failed');
          // }

        }
        
        

      }else
      {
        error('Order failed');
      }

    }

  }

  async function stripePayment(stripeToken)
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");

    if(mixdelight_token)
    {
      if(shipAddress < 1)
      {
        error('Please choose address with valid pincode');
        //console.log("choose address")
        return false;
      }

      let header = { Token: mixdelight_token };
      let param = {shipping_id : shipAddress, gateway: 3, stripeToken:stripeToken}
      let orderAddUrl = getApiurl("orderAdd");
      let placeResp = await PostRequestFunction(orderAddUrl,header, param);
      if(placeResp.status)
      {
        
            success('Order Place !');
            dispatch(setCartCount(0));
            history.push('/orders');
      }else
      {
        error('Order failed');
      }

    }

  }

  async function getAmountCart()
  {
      let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
      if(mixdelight_token)
      {
        let header = { Token: mixdelight_token };
        let cartAmountUrl = getApiurl("getCartAmount");
        let resp = await GetRequestFunction(cartAmountUrl,header, {});
        if(resp.status)
        {
            let data = resp.data[0];
            return data;
         }
      }else
      {
            return [];
      }
  }
  
  async function PlaceOrder()
  {
      if(gateway==1)
      {
        razorPayPayment();
       
        
      }else
      {
        CashOnDelivery();
      }
      
  }

  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
        <View>
          <View style={styles.msgBg}>
            <Text style={styles.msg}>You can cancel order within 1 hrs once placed.</Text>
          </View>
          <Row>
            <Col xs={12} sm={12} md={6} lg={8} xl={8}>
              <CheckoutAddress getShippingAddress={getShippingAddress}/>
            </Col>
            <Col xs={12} sm={12} md={6} lg={4} xl={4}>
              <View>
                      <CheckoutDetail placeOrder={PlaceOrder} 
                        getGetway={getGetway} gateway={gateway} hidecash={true}/>
                  </View>
            </Col>
          </Row>
        </View>
        </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  msgBg:{
    backgroundColor:'#ffffa9c2',
    borderTopWidth:1,
    borderBottomWidth:1,
    borderColor:'#ffffa9c2',
    marginTop: 30,
    marginBottom: 10,
    paddingHorizontal:20
  },
  msg:{
    fontSize:theme.size16,
    color: theme.orange,
    fontFamily: 'MontserratSemiBold',
    textAlign:'center',
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 50,
    flexGrow:1,
    zIndex: -15,
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 15,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
      msg:{
        fontSize:theme.size12,
      },
      msgBg:{
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal:5
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
      msg:{
        fontSize:theme.size12,
      },
      msgBg:{
        marginTop: 20,
        marginBottom: 10,
        paddingHorizontal:5
      },
    })
  )
);
