import React from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Button, Image, SafeAreaView, Platform } from 'react-native';
import { Link, useLocation } from '../Route/routing';

import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import HomeBanner from './atom/HomeBanner';
import TitleText from './atom/TitleText';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

import ServiceComp from './atom/ServiceComp';
import HomeItem from './molecule/HomeItem';
import { useDispatch, useSelector } from 'react-redux';
import AlertTemplate from './atom/AlertComponent';


const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function Home(props) {
  let location = useLocation();
  const { isAlert, text, options, type } = useSelector(state => state.alertReducer);
  console.log(location.pathname); 
  return (
    <View style={{height:'100%'}}>
    <MobileHeader />
    <Header />
     <ScrollView>
          <View style={styles.outerPageArea}>
            <HomeBanner />
            <View style={styles.pageArea}>
              <View>
                {/* About Us Start */}
                <View style={{marginVertical:30}}>
                  <Row>
                      <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                      <TitleText
                        title="Celebrate with us"
                        titleSize={25}
                        txtColor={theme.darkbg}
                        titleAlign={'left'}
                        titleFont={'MontserratSemiBold'}
                        mb_titleSize={theme.size18}
                      />
                        <TitleText
                        title="Cake is an art, love, happiness and also an emotion. A celebration without cake is just a meeting. We bake to delight your taste and    to cherish your every moment with happiness and to create delightful memories.  You will fall in love with our cakes. We bake love spread happiness over it and serve at your doorstep to twist your mood. Mix delight presents cakes & more at your doorstep with a good quality, fantastic taste and quality service. Because we care for your smile. The best cake shop under one roof. sooo hurry up to add sweetness in your life and to celebrate moment with us. we are happy to serve you."
                        titleSize={theme.size14}
                        txtColor={theme.darkbg}
                        titleAlign={'left'}
                        mb_titleSize={theme.size13}
                      />
                      </Col>
                      <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                        <View style={{height:200}}>
                          <Image
                            source={require('../assets/img/bannertwo.jpg')}
                            resizeMode="contain"
                            style={{width:'100%', height:'100%'}}
                          />
                        </View>
                      </Col>
                  </Row>
                </View>
                {/* About Us End */}

                {/* Our Gallery Start */}
                <View style={styles.ourProductWrap}>
                  <View>
                    <TitleText
                      title="Our Products"
                      titleSize={25}
                      txtColor={theme.darkbg}
                      titleAlign={'center'}
                      titleFont={'MontserratSemiBold'}
                      mb_titleSize={theme.size18}
                    />
                  </View> 
                  <Row>
                      <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                      <Link to="/cakelisting" style={{ textDecoration: 'none' }}>
                        <HomeItem itemName={'All Occasions Cake'} imgsrc={require('../assets/img/spectacular.jpg')} />
                      </Link>  
                      </Col>
                      <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                        <Link to="/expressdelivery" style={{ textDecoration: 'none' }}>
                        <HomeItem itemName={'Quick Deliver'} imgsrc={require('../assets/img/weddingCake.jpg')} />
                        </Link>  
                      </Col>
                      {/* <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                        <HomeItem itemName={'Anniversary Cake'} imgsrc={require('../assets/img/anniversary.jpg')} />
                      </Col>
                      <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                        <HomeItem itemName={'Customzie'} imgsrc={require('../assets/img/customized.jpg')} />
                      </Col> */}
                      <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                      <Link to="/cupcake" style={{ textDecoration: 'none' }}>
                        <HomeItem itemName={'Cupcakes'} imgsrc={require('../assets/img/cupcakes.jpg')} />
                        </Link>
                      </Col>
                      <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                        <HomeItem itemName={'Decore Itmes'} imgsrc={require('../assets/img/decore.jpg')} />
                      </Col>
                      {/* <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                        <HomeItem itemName={'Chocholates'} imgsrc={require('../assets/img/chocolate.png')} />
                      </Col> */}
                      {/* <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                        <HomeItem itemName={'Flowers'} imgsrc={require('../assets/img/flowers.jpg')} />
                      </Col> */}
                    </Row>
                </View>
                {/* Our Gallery Start */}

                {/* Service Start */}
                <View>
                    <TitleText
                      title="Our Services"
                      titleSize={25}
                      txtColor={theme.darkbg}
                      titleAlign={'center'}
                      titleFont={'MontserratSemiBold'}
                    />
                  </View> 
                  <Row>
                    <Col xs={12} sm={12} md={4} lg={4} xl={4}>
                      <ServiceComp icon={'cake'} serviceName={'Cake Baking'} serviceDesc={'we bake fresh homemade cakes with good quality and good taste. We bake delight full cake for you and your family. We bake especially for you'} />
                    </Col>
                    <Col xs={12} sm={12} md={4} lg={4} xl={4}>
                      <ServiceComp icon={'truck-delivery'} serviceName={'Delivery To Any Point'} serviceDesc={'we are here to delivered the fresh homemade cake till your doorstep. Mix Delight  present you some good quality and hygiene cakes at your doorstep'} />
                    </Col>
                    <Col xs={12} sm={12} md={4} lg={4} xl={4}>
                      <ServiceComp icon={'gift'} serviceName={'Individual Approach'} serviceDesc={'All our vendors & customers receive an individual approach, which is achieved by trust. we ensure that no one will be sad with our service'} />
                    </Col>
                  </Row>
              </View>
              {/* Service End */}
            </View>
          </View>
          </ScrollView>
          <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  contentGrow:{
    height:height
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  outerPageArea:{
    marginVertical: isWeb? 0:60,
  },
  pageArea: {
    paddingHorizontal: 200,
    //backgroundColor: '#f9f9f9',
    backgroundColor: 'white',
    flexGrow:1,
    zIndex:-15,
  },
  checkpin: {
    marginTop: 0,
  },
  ourProductWrap :{
    marginTop:50,
    marginBottom:50
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 15,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
      ourProductWrap :{
        marginTop:10,
        marginBottom:50
      },
      contentGrow:{
        height:height-145
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
      contentGrow:{
        height:height-145
      },
    })
  ),
  minWidth(
    992,
    maxWidth(1199, {
      pageArea: {
        paddingHorizontal: 100,
      },
    })
  )
);
