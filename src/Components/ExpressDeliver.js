import React, { useState, useEffect } from "react";
import { ScrollView,View,FlatList,Dimensions,SafeAreaView, Platform} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import ProductCardMolecule from './molecule/ProductCardMolecule';
// import { isMobile } from 'react-device-detect';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { getApiurl } from "../api/ApiKeys";
import { PostRequestFunction,GetRequestFunction } from "../api/ApiHelper";

const { width, height } = Dimensions.get('window');
const isWeb = Platform.OS === 'web';

export default function ExpressDeliver() {
  const [cakes, setCakes] = useState([]);
  const width = Dimensions.get('window').width;
  useEffect(() => {
    getCakes();
  },[]);

  async function getCakes(param={})
  {
    // let param = { category: paramericvalue, page_limit: limit, page: page };
    let getcakeUrl = getApiurl("getInstantDeliverProduct");
    let getProductList = await GetRequestFunction(getcakeUrl, {}, param);
    if (getProductList.status)
    {
      console.log(getProductList.data)
      let cakelist = getProductList.data;
      setCakes(cakelist);
    } 
  }
 
  return (
      <SafeAreaView style={{flex:1}}>
        <MobileHeader />
        <Header />
        <SafeAreaView style={styles.outerContainer}>
          <FlatList
            data={cakes}
            scrollEnabled={true}
            initialNumToRender={10}
            numColumns={width>768?4:2}
            keyExtractor={item => item.id}
            renderItem={({ item, index, separators }) => (
                <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                  <View style={styles.container}>
                    <ProductCardMolecule cakedata={item}/>
                  </View>
                </Col>
             )}
         /> 
        </SafeAreaView>
          <Footer />
        <MobileFooter />
      </SafeAreaView>
  );
}

const base = {
  outerContainer: {
    flex: 1,
    paddingHorizontal: 80, 
    paddingVertical: 20,
    marginVertical: isWeb? 0:60,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(1024, {
      outerContainer: {
        paddingHorizontal: 0,
        paddingVertical: 0,
      },
    })
  )
);
