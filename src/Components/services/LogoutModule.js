import React from "react";
import { getAsyncStorageFunction,flushAsyncStorageFunction } from "./CookieModule";
import { getApiurl } from "../../api/ApiKeys";
import { GetRequestFunction } from "../../api/ApiHelper";

export async function LogoutFunction(history=null) {


    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    let logoutUrl = getApiurl('logout');
    let header = { Token: mixdelight_token }
    let logoutResp = GetRequestFunction(logoutUrl, header, {})
    flushAsyncStorageFunction('mixdelight_token');
    flushAsyncStorageFunction('persist:mixDelightrRoot');
    //localStorage.clear();
    if(history)
    {
        history.push('/');
    }
    
}

export async function clearLogout(target = '') {
   
    flushAsyncStorageFunction('mixdelight_token');
}


