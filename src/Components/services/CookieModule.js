import AsyncStorage from '@callstack/async-storage';

//set and get email_id and password
export function getAsyncStorageFunction(key) {
  let getItem = AsyncStorage.getItem(key);
  return getItem;
}

export async function setAsyncStorageFunction(key, data) {
  let setItem = await AsyncStorage.setItem(key, data);
}

export function flushAsyncStorageFunction(key) {
  AsyncStorage.removeItem(key);
}
