
import { useDispatch, useSelector } from "react-redux";
import { addAlert} from "../../store/action/alertAction";

const useShow = () => {
 
    const dispatch = useDispatch();
    const { isAlert } = useSelector(state => state.alertReducer)

    const show = async (text = "", opt = {}) => {

        let defaultStyle = {
            "zIndex": "9999",
            "position": "absolute"
        }

        let showType = 'TOP_RIGHT'

        if (!opt?.showType)
            opt.showType = showType



        if (opt?.style)
            opt.style = { ...defaultStyle, ...opt.style }
        else
            opt['style'] = { ...defaultStyle }




        let obj = {
            text,
            options: opt,
            isAlert: true,
            type: 'info'
        }

        dispatch(addAlert(obj))

        time()
    };

    const error = (text = "", opt = {}) => {

        let defaultStyle = {
            "zIndex": "10",
            "position": "absolute"
        }


        let showType = 'TOP_CENTER'

        if (!opt?.showType)
            opt.showType = showType

        if (opt?.style)
            opt.style = { ...defaultStyle, ...opt.style }
        else
            opt['style'] = { ...defaultStyle }

        let obj = {
            text,
            options: opt,
            isAlert: true,
            type: 'error'
        }
        dispatch(addAlert(obj))

        time()
    };

    const success = (text = "", opt = {}) => {


        let defaultStyle = {
            // "zIndex": "9999",
            // "margin": 'auto 0 auto auto',
            // "position": 'absolute',
            // "width":'100%'
            "zIndex": "9999",
            "position": "absolute"
        }


        let showType = 'TOP_RIGHT'

        if (!opt?.showType)
            opt.showType = showType


        if (opt?.style)
            opt.style = { ...defaultStyle, ...opt.style }
        else
            opt['style'] = { ...defaultStyle }

        let obj = {
            text,
            options: opt,
            isAlert: true,
            type: 'success'
        }

        dispatch(addAlert(obj))

        time()
    };

    const time = () => {

        setTimeout(() => {

            dispatch(addAlert({}))

        }, 2000);

    }

    const close = () => {
        dispatch(addAlert({}))

    }


    return { show, error, success, close };
};

export { useShow };
