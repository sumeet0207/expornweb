import { GetRequestFunction, DeleteRequestFunction } from '../../api/ApiHelper';
import { getApiurl } from '../../api/ApiKeys';

export async function CheckValidPincode(pincodeval)
{
    let header={};
    let pincodeValidationUrl = getApiurl("pincodeValidation");
    let pinValidationRes = await GetRequestFunction(`${pincodeValidationUrl}/${pincodeval}`, header);
    if(pinValidationRes.status)
    {
        return true;
    }else
    {
        return false;
    }
}


export function getNameObject(fullname) {
  let fname = "";
  let lname = "";
  if (fullname) {
    let nameArry = fullname.split(" ");
    let nameArryLength = nameArry.length;
    if (nameArryLength === 1) {
      fname = nameArry[0];
    }
    if (nameArryLength > 1) {
      fname = nameArry[0];
      lname = nameArry[nameArryLength - 1];
    }
  }
  return { firstname: fname, lastname: lname };
}

export async function getBasicInformation(token) {
  let basicInfo = null;
  let header = { Token: token };
  let basicInfoUrl = getApiurl("loginBasicInfo");
  let getInfo = await GetRequestFunction(basicInfoUrl, header, {});
  if (getInfo.status) {
    basicInfo = getInfo.data[0];
  }
  return basicInfo;
}

// export function getIdFromString(passstr) {
//   let returnid = "";
//   if (passstr) {
//     let eleArray = passstr.split("-");
//     returnid = eleArray.pop();
//   }
//   return returnid;
// }

// export function getPageUrlFromString(passstr) {
//   let prod_pageurl = "";
//   if (passstr) {
//     let eleArray = passstr.split("-");
//     eleArray.splice(-1, 1);
//     prod_pageurl = eleArray.join("-");
//   }
//   return prod_pageurl;
// }

export function Setimage(e) {
  e.target.src = noimage;
}

// export async function getShippingList() {
//   let peepul_token = getCookieFunction(config.TOKENKEY);
//   let header = { Token: peepul_token }

//   let url = getApiurl("getShipping");
//   let resp = await GetRequestFunction(url, header, {})

//   return resp;

// }

// export async function deleteShipping(body) {
//   let peepul_token = getCookieFunction(config.TOKENKEY);
//   let header = { Token: peepul_token }

//   let url = getApiurl("deleteShipping");
//   let resp = await GetRequestFunction(url, header, body)

//   return resp;

// }

// export function checkSpaceStartEnd(val)
//   {
//       let flag = false;
//       let start = val.startsWith(" ");
//       let end = val.endsWith(" ");
//       if(start || end)
//       {
//           flag = true;
//       }    

//       return flag;
//   }

  // export function hasWhiteSpace(s) 
  // {
  //   let flag = true;
  //   if(checkSpaceStartEnd(s) || s.indexOf(' ') >= 0)
  //   {
  //     flag =false;
  //   }
  //   return flag
    
  // }

  // export function getInclusiveTaxAmount(basePrice,taxPer)
  // {
  //   // console.log(basePrice,taxPer)
  //   let resultAmt=0;
  //   if(taxPer)
  //   {
  //     let taxamt = basePrice * taxPer/100;
  //     //console.log(taxamt)
  //     resultAmt = basePrice + taxamt;
  //   }
  //   console.log(resultAmt)
  //   return resultAmt;
  // }
