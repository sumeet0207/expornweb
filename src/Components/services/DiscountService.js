
 export function valueByPercentage(baseamt,disAmount) {
    let perValue = parseFloat(baseamt) * (parseFloat(disAmount) / 100);
    let discountvalue = parseFloat(baseamt) - parseFloat(perValue);
    return discountvalue;
  }

export function valueByPrice(baseamt,disAmount) {
    let discountvalue = parseFloat(baseamt) - parseFloat(disAmount);
    return discountvalue;
  }

export function getPercentageChange(oldNumber, newNumber){
    var decreaseValue = oldNumber - newNumber;
    return (decreaseValue / oldNumber) * 100;
}