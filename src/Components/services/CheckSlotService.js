
import moment from 'moment';
const slotarr = [
    { value: '1', label: '8 PM - 10 PM'},
    { value: '2', label: '10 PM - 12 AM'},
    { value: '3', label: '1 PM - 4 PM' },
    { value: '4', label: '5 PM - 8 PM' },
    { value: '5', label: '9 PM - 12 AM' }

]
export function CheckSlotForDelivery(istoday,deliverDate)
{
    var date = new Date()
    let newformat1 = moment(date).format('L');
    let newformat2 = moment(deliverDate).format('L');
    var a = moment(newformat2);
    var b = moment(newformat1);
    let noOfdays = a.diff(b, 'days')

    let yourLuck = whenYouOrdering();
    let filterSlot=[];

    if(noOfdays > 1)
    {
        filterSlot.push(slotarr[0],slotarr[1],slotarr[2],slotarr[3],slotarr[4]);
        return filterSlot;
    }

    if(istoday)
    {
        if(yourLuck==1)
        {
            filterSlot.push(slotarr[0],slotarr[1],slotarr[4])
        }else if(yourLuck==2)
        {
            filterSlot.push(slotarr[0],slotarr[1])
        }
        
    }else
    {
        if(yourLuck==1)
        {
            filterSlot.push(slotarr[2],slotarr[3],slotarr[4])
        }else if(yourLuck==2)
        {
            filterSlot.push(slotarr[3],slotarr[4])
        }else{
            filterSlot.push(slotarr[0],slotarr[1])
        }
    }

   
    console.log(filterSlot)

    return filterSlot;
   
}

export function whenYouOrdering()
{
    //let currentTime = moment('1:00 pm', "HH:mm a");
    let currentTime= moment();
    let yourluck=0;
    let morn12 = moment('12:00 pm', 'HH:mm a');
    let even5 = moment('5:00 pm', 'HH:mm a');
    let checkBefore = currentTime.isBefore(morn12);
    let isInBetween = currentTime.isBetween(morn12 , even5);
    let checkexact12 = currentTime.isSame(morn12);
    let checkexact5 = currentTime.isSame(morn12);
   
    if(checkBefore)
    {
        yourluck=1;
    }else if(checkexact12)
    {
        yourluck=1;
    }else if(isInBetween)
    {
        yourluck=2;
    }else if(checkexact5)
    {
        yourluck=2;
    }else{
        yourluck=3;
    }   

    return yourluck;

}



