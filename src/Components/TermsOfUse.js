import React from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Image, Platform } from 'react-native';
import { Link, useLocation } from '../Route/routing';

import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import OrderMolecule from './molecule/OrderMolecule';
import TitleText from './atom/TitleText';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function TermsOfUse() {
  let location = useLocation();
  console.log(location.pathname);
  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View style={{ padding: 20, flexGrow:1, backgroundColor:theme.white }}>
              <View style={{ marginVertical:10 }}>
                <TitleText
                  title="Terms and Conditions"
                  txtColor={theme.darkbg}
                  titleAlign={'center'}
                  titleSize={theme.size20}
                  mb_titleSize={theme.size20}
                  titleFont={'MontserratSemiBold'}
                />
              </View>
              <View style={styles.termsTxt}> 
                <ul>
                  <li style={styles.liBtm}>If you see content that does not align with these guidelines or our Terms of Service, please let us know. We will consider all reports. </li>
                  <li style={styles.liBtm}>Write your review based on facts and your own experiences (e.g. not a friend's or any hearsay experience or based on media reports). Please don't exaggerate or falsify your experience.</li>
                  <li style={styles.liBtm}>Your profile is your identity on Mix Delight, so keep it real. </li>
                  <li style={styles.liBtm}>We are a website and mobile application with no claims to be a court of law or a ministry of health. If you feel a vendor does not measure up to health codes, we encourage you to contact the appropriate authorities directly. We are not the appropriate platform for reporting illegal activities, health code violations, or anything under investigation by local governing bodies or law-enforcement personnel.</li>
                  <li style={styles.liBtm}>These Terms of Use are subject to modifications at any time. We reserve the right to modify or change these Terms of Use and other Mix Delight policies at any time by posting changes on the Platform, and you shall be liable to update yourself of such changes, if any, by accessing the changes on the Platform.</li>
                  <li style={styles.liBtm}>By accepting these Terms of Use, you also accept and agree to be bound by the other terms and conditions and Mix Delight policies including but not limited to Cancellation & Refund Policy, Privacy Policy as may be posted on the Platform from time to time.</li>
                  <li style={styles.liBtm}>All the Products listed on the Platform will be sold at specific prices unless otherwise specified. These prices are subject to change with/without notification to you. </li>
                  <li style={styles.liBtm}>Certain products like cake, pastry, chocolate, sweets, and breads need to be consumed within 1-2 days of delivery or in accordance with the instructions or specifications of the Manufacturers and producers, made available on the package.</li>
                  <li style={styles.liBtm}>Mix Delight may contact via telephone, SMS or other electronic messaging or by email with information about the Service to be offered to you or any feedback there on. </li>
                  <li style={styles.liBtm}>We will create your Mix Delight Account for your use of the Platform services based upon the personal information you provide to us You shall only have one Mix Delight Account and not permitted to create multiple accounts. If found, you having multiple accounts, with us reserves the right to suspend such multiple account without being liable for any compensation.</li>
                  <li style={styles.liBtm}>In the event of an item on your Order being unavailable, we will contact you on the phone number provided to us at the time of placing the Order and inform you of such unavailability. In such an event you will be entitled to cancel the Order and shall be entitled to a refund in accordance with our refund policy.</li>
                  <li style={styles.liBtm}>The product images displayed on platform are only for reference purpose. While every reasonable effort is made to maintain accuracy of information on the platform, actual product packaging, product size, weight, price and such other product details may contain more and/or different information than what is shown on the platform. It is recommended not to solely rely on the information presented on the platform.</li>
                  <h3 style={styles.refunalign}>Refund and Returns</h3>
                  <h4 style={styles.liBtm}>Mix Delight will credit a refund to your account within 7-8 business days. You will get a refund if –</h4>
                  <li style={styles.liBtm}>Damaged/tampered packaging at the time of delivery.</li>
                  <li style={styles.liBtm}>After placing the order, you have 1 hour for order cancelation and you will be eligible for refund.</li>
                  <li style={styles.liBtm}>Order can cancelled due to unavailability of items in your order.</li>

                </ul>
              </View> 
              {/* <View style={styles.termsTxt}>2.  Write your review based on facts and your own experiences (e.g. not a friend's or any hearsay experience or based on media reports). Please don't exaggerate or falsify your experience.</View>
              <View style={styles.termsTxt}>3. Your profile is your identity on Mix Delight, so keep it real. </View>
              <View style={styles.termsTxt}>4. We are a website and mobile application with no claims to be a court of law or a ministry of health. If you feel a vendor does not measure up to health codes, we encourage you to contact the appropriate authorities directly. We are not the appropriate platform for reporting illegal activities, health code violations, or anything under investigation by local governing bodies or law-enforcement personel.</View> */}
              
          </View>
        </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  // container: {
  //   flex: 1,
  //   backgroundColor: '#fff',
  // },
  refunalign : {
    textAlign: 'center',
  },

  liBtm : {
    marginBottom: 15
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 200,
    flexGrow:1,
    backgroundColor: '#f9f9f9',
    zIndex: -15,
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
  termsTxt :{
    fontSize:theme.size16,
    color: theme.darkbg,
    fontFamily: 'MontserratRegular',
    textAlign:'left',
  },
  imgWrap:{
    height:200
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 0,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
    })
  ),
  minWidth(
    992,
    maxWidth(1199, {
      pageArea: {
        paddingHorizontal: 100,
      },
    })
  )
);
