
export const mix_delight_const = {
    slot :{ 
        1: '8 PM - 10 PM', 
        2: '10 PM - 12 AM',
        3: '1 PM - 4 PM',
        4: '5 PM - 8 PM',
        5: '9 PM - 12 AM',
    
    },
    imgpath: '',   //productimage,accessorybox
    noImgpath: 'https://www.staticwhich.co.uk/static/images/products/no-image/no-image-available.png'

};

