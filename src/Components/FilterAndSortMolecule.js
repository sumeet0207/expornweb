import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, Dimensions, Platform, ScrollView } from 'react-native';
import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import TitleAndCount from './atom/TitleAndCount';
import { getApiurl } from "../api/ApiKeys";
import { PostRequestFunction,GetRequestFunction } from "../api/ApiHelper";
import { useDispatch, useSelector } from 'react-redux';
import { setToFlavourFilter } from "../store/action/flavourFilterAction";
import { setToShapeFilter } from "../store/action/shapeFilterAction";
import { setToWeightFilter } from "../store/action/weightAction";
import { setToVendorFilter } from "../store/action/vendorFilterAction";
import { setToTypeFilter } from "../store/action/cakeTypeAction";
import CloseApplyButton from './atom/CloseApplyButton';


const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
    const base = {
      mainDiv:{
        // flex:1, 
        // flexDirection:'row',
        // height: 100
      },
      LeftPart: {
        // width: '100%',
        // flex: '0.3',
        height: height-100
      },
      RightPart:{
        // flex: '0.7',
        // maxHeight: height,
        // overflow:'scroll',
        // overflowY: 'scroll',
        // height: height- 200
        height: height-100
      },
      filterTitleWrap:{
        //backgroundColor: theme.lightergrey,
        borderColor: '#ccc',
        borderLeftWidth: 0,
        borderRightWidth: 0, 
      },
      filter:{
        paddingVertical: 15,
        paddingHorizontal: 15,
        fontSize: theme.size16,
        color: theme.darkbg,
        fontFamily: 'MontserratSemiBold',
        borderBottomWidth: 0.5,
        borderColor: '#ccc',
      }
    };
  
    const pstyles = createStyles(
      base,
      minWidth(
        100,
        maxWidth(991, {
        })
      )
    );
    return pstyles;
  };

const FilterAndSortMolecule = (props) => {
    const dispatch = useDispatch();

    const pstyles = getStyle(props);
    const [filtername, setFilterName] = useState('Flavour');

    const flavourRedux = useSelector(state => state.flavourReducer.flavoured)
    const shapeRedux = useSelector(state => state.shapeReducer.shaped)
    const vendorRedux = useSelector(state => state.vendorReducer.vendors)
    const weightRedux = useSelector(state => state.weightReducer.weights)
    const typesRedux = useSelector(state => state.cakeTypeReducer.types)
    
    const [flavourEle, setFlvaourEle] = useState(flavourRedux);
    const [shapeEle, setShapeEle] = useState(shapeRedux);
    const [vendorEle, setVendorEle] = useState(vendorRedux);
    const [weightEle, setWeightEle] = useState(weightRedux);
    const [typesEle, setTypeEle] = useState(typesRedux);

    function ChangeFilter(passdata)
    {
      setFilterName(passdata);
    }

    function passValueInArray(checkid)
    {
       
       if(filtername=='Flavour')
       {
          handleFlavourToggle(checkid);
       }else if(filtername=='Shape')
       {
          handleShapeToggle(checkid);
       }else if(filtername=='Vendor')
       {
          handleVendorToggle(checkid);
       }else if(filtername=='Weight')
       {
          handleWeightToggle(checkid);
       }else if(filtername=='Type')
       {
          handleTypesToggle(checkid);
       }
       
    }

    function handleFlavourToggle(chkid)
    {
       var tempflavour = [];
       if(flavourEle.includes(chkid))
       {
          const filteredItems = flavourEle.filter(item => item != chkid)
          setFlvaourEle(filteredItems);
          //dispatch(setToFlavourFilter(tempflavour));
       }else
       {
         flavourEle.push(chkid);
         setFlvaourEle(flavourEle);
         //dispatch(setToFlavourFilter(flavourEle));
       }
       //console.log(flavourEle)
    }

    function handleShapeToggle(chkid)
    {
      var tempShape = [];
      if(shapeEle.includes(chkid))
      {
         const filteredItems = shapeEle.filter(item => item != chkid)
         //tempShape = _.remove(shapeEle, function(n) { return n == chkid});
         setShapeEle(filteredItems)
         //dispatch(setToShapeFilter(tempShape));
      }else
      {
        shapeEle.push(chkid);
        setShapeEle(shapeEle)
        //dispatch(setToShapeFilter(shapeEle));
      }
    }

    function handleVendorToggle(chkid)
    {
      var tempVendor = [];
      if(vendorEle.includes(chkid))
      {
         const filteredItems = vendorEle.filter(item => item != chkid)
         //tempVendor = _.remove(vendorEle, function(n) { return n == chkid});
         setVendorEle(filteredItems);
         //dispatch(setToVendorFilter(tempVendor));
      }else
      {
        vendorEle.push(chkid);
        setVendorEle(vendorEle);
        //dispatch(setToVendorFilter(vendorEle));
      }
      console.log(vendorEle)
    }

    function handleWeightToggle(chkid)
    {
      var tempWeight = [];
      if(weightEle.includes(chkid))
      {
         const filteredItems = weightEle.filter(item => item != chkid)
         //tempWeight = _.remove(weightEle, function(n) { return n == chkid});
         //dispatch(setToWeightFilter(weightEle));
         setWeightEle(filteredItems);
      }else
      {
        weightEle.push(chkid);
        setWeightEle(weightEle);
        //dispatch(setToWeightFilter(weightEle));
      }
     
    }

    function handleTypesToggle(chkid)
    {
      
      var tempTypes = [];
      if(typesEle.includes(chkid))
      {
         const filteredItems = typesEle.filter(item => item != chkid)
         setTypeEle(filteredItems);
      }else
      {
        typesEle.push(chkid);
        setTypeEle(typesEle);
      }
     
    }

    async function applyFinaloFilter()
    {
      
       dispatch(setToFlavourFilter(flavourEle));
       dispatch(setToShapeFilter(shapeEle));
       dispatch(setToTypeFilter(typesEle));
       dispatch(setToVendorFilter(vendorEle));
       dispatch(setToWeightFilter(weightEle));
       props.onPress();//to close
       props.getfilterdata(flavourEle,typesEle,shapeEle,vendorEle,weightEle);
    }

    function clearFilter()
    {
      setFlvaourEle([])
      setShapeEle([])
      setVendorEle([])
      setWeightEle([])
      setTypeEle([]);
  
      dispatch(setToFlavourFilter([]));
      dispatch(setToShapeFilter([]));
      dispatch(setToTypeFilter([]));
      dispatch(setToVendorFilter([]));
      dispatch(setToWeightFilter([]));
   
    }
      
    return (
        <View>
            <View>
                <View >
                    <View style={[pstyles.filter,{flexDirection:'row', justifyContent: 'space-between'}]}>
                      <Text>FILTERS</Text>
                      <TouchableOpacity onPress={clearFilter}>
                        <Text style={{color:theme.mbHFColor}}>Clear All</Text> 
                      </TouchableOpacity>
                    </View>
                </View>
                <Row>
                  <Col xs={3} sm={3} md={3} lg={3} xl={3}>
                     {/* Categore Start */}
                     <ScrollView style={pstyles.LeftPart}>
                        <View style={pstyles.filterTitleWrap}>
                            <TitleAndCount title={'Flavour'} 
                              count={props.flavours?props.flavours.length:0} 
                               handleClick={ChangeFilter} filtername={filtername}/> 
                            <TitleAndCount title={'Shape'} 
                            count={props.shape?props.shape.length:0} 
                             handleClick={ChangeFilter} filtername={filtername}/> 
                             <TitleAndCount title={'Type'} 
                              count={props.types?props.types.length:0} 
                             handleClick={ChangeFilter} filtername={filtername}/> 
                            <TitleAndCount title={'Vendor'} 
                             count={props.vendors?props.vendors.length:0} 
                              handleClick={ChangeFilter} filtername={filtername}/> 
                            <TitleAndCount title={'Weight'} 
                             count={props.weights?props.weights.length:0} 
                              handleClick={ChangeFilter} filtername={filtername}/> 
                            
                        </View>
                    </ScrollView>
                    {/* Categore End */}

                   
                  </Col>
                  <Col xs={9} sm={9} md={9} lg={9} xl={9}>
                     {/* Varient Start */}
                     <ScrollView style={pstyles.RightPart}>
                        <View>
                        {filtername=='Flavour' && props.flavours.map((item, key) => (
                          <TitleAndCount title={item.name} 
                           handleClick={passValueInArray}
                           passid={item.id} isCheckVisible checkarray={flavourEle}
                           isLeft={false}/> 
                        ))}

                        {filtername=='Shape' && props.shape.map((item, key) => (
                          <TitleAndCount title={item.name} passid={item.id} 
                           handleClick={passValueInArray}
                           isCheckVisible isLeft={false} checkarray={shapeEle}/> 
                        ))}

                        {filtername=='Type' && props.types.map((item, key) => (
                          <TitleAndCount title={item.name} passid={item.id} 
                           handleClick={passValueInArray}
                           isCheckVisible isLeft={false} checkarray={typesEle}/> 
                        ))}

                        {filtername=='Vendor' && props.vendors.map((item, key) => (
                          <TitleAndCount title={`${item.fname} ${item.lname}`} 
                          handleClick={passValueInArray}
                            passid={item.id} isCheckVisible isLeft={false} checkarray={vendorEle}/> 
                        ))}

                        {filtername=='Weight' && props.weights.map((item, key) => (
                          <TitleAndCount title={`${item.weight} Kg`}
                            handleClick={passValueInArray}
                            passid={item.weight} isCheckVisible isLeft={false} checkarray={weightEle}/> 
                        ))}
                            
                        </View>
                    </ScrollView>
                    {/* Varient End */}
                  </Col>
                  
                </Row>
                <CloseApplyButton onPress={props.onPress} applyfilter={applyFinaloFilter}/>
            </View>
        </View>
    )
}

export default FilterAndSortMolecule
