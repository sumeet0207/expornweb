import React, { useState, useEffect } from "react";
import { StyleSheet, TextInput,Text,ImageBackground,View, 
Image, TouchableOpacity, ScrollView, SafeAreaView , Platform} from "react-native";
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link,useHistory } from '../Route/routing';
import { useForm, Controller } from 'react-hook-form';
import { getApiurl } from "../api/ApiKeys";
import { PostRequestFunction,GetRequestFunction } from "../api/ApiHelper";
import { setAsyncStorageFunction,getAsyncStorageFunction,
    flushAsyncStorageFunction } from "./services/CookieModule";
import { useDispatch, useSelector } from 'react-redux';
import { setProfileName } from "../store/action/profileAction";
import { setCartCount } from "../store/action/cartAction";
import { useShow } from "./services/AlertHook";
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';
import { getNameObject, getBasicInformation } from "../Components/services/CommonModule";
import * as GoogleSignIn from 'expo-google-sign-in';




const isWeb = Platform.OS === 'web';
//import AsyncStorage from '@callstack/async-storage';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { theme } from "../Theme/theme";


export default function Login() {
        let history = useHistory();
        const { show, error, success } = useShow();
        const [googleLogin, setGoogleLogin] = useState(false);
        const dispatch = useDispatch();
        const { isAlert, text, options, type } = useSelector(state => state.alertReducer);
        const { register, setValue, handleSubmit, control, errors } = useForm({
            defaultValues: {
                email:"",
                password:"",
            }
        });

        useEffect(() => {
             initSocialLogin();
             if(!isWeb)
             {
                initAsync();
             }
             
        }, []);

        const initSocialLogin = async () => {
            try {
                await Facebook.initializeAsync('134170801550907');
            } catch (e) {
                console.log(e);
            }
        };

        const initAsync = async () => {
            await GoogleSignIn.initAsync({
            });
           
        };

        const signInAsync = async () => {
            try {
            await GoogleSignIn.askForPlayServicesAsync();
            const { type, user } = await GoogleSignIn.signInAsync();
            if (type === 'success') {
                _syncUserWithStateAsync();
            }
            } catch ({ message }) {
            alert('login: Error:' + message);
            }
        };

        const _syncUserWithStateAsync = async () => {
            const user = await GoogleSignIn.signInSilentlyAsync();
            setGoogleLogin(true)
            //alert(user.email)
            handleGoogleLogin(user);
            
        };

        async function handleFacebookLogin(user)
        {
            if(user.email)
            {
                 let nameobj = getNameObject(user.name);
                 let param = {
                    email_id: user.email,
                    fb_id: user.id,
                    fname: nameobj.firstname,
                    lname: nameobj.lastname,
                    };
                let fbLoginUrl = getApiurl("facebookLogin");
                let loginResp = await PostRequestFunction(fbLoginUrl, {}, param);
                console.log(loginResp)
                 if(loginResp.status)
                 {
                    let token = loginResp.data[0].login_token;
                    let info = await getBasicInformation(token);
                    let set = await setAsyncStorageFunction('mixdelight_token', token);
                    success('Login Successfully');
                    //history.goBack();
                    history.push('/profile');

                 }else
                 {
                    error(loginResp.message);
                    console.log(loginResp.message)

                 }

            }

        }

         async function handleGoogleLogin(profileObj) {
    
                if (profileObj.displayName)
                {
                    let email = profileObj.email;
                    let name = profileObj.displayName;
                    let nameobj = getNameObject(name);
                    let param = {
                        email_id: email,
                        fname: nameobj.firstname,
                        lname: nameobj.lastname,
                    };

                    let googleLoginUrl = getApiurl("googleLogin");
                    let loginResp = await PostRequestFunction(googleLoginUrl, {}, param);
                    console.log(loginResp)
                    if(loginResp.status)
                    {
                        let token = loginResp.data[0].login_token;
                        let info = await getBasicInformation(token);
                        let set = await setAsyncStorageFunction('mixdelight_token', token);
                        success('Login Successfully');
                        history.push('/profile');
                        
                    }else
                    {
                        error("Something went wrong");
                    }

                }
            }

        async function getBasicInformation(token) {
            let basicInfo = null;
            let header = { Token: token };
            let basicInfoUrl = getApiurl("loginBasicInfo");
            let getInfo = await GetRequestFunction(basicInfoUrl, header, {});
            if (getInfo.status) {
              basicInfo = getInfo.data[0];
              dispatch(setProfileName({ fname: basicInfo.fname, lname: basicInfo.lname }));
              dispatch(setCartCount(basicInfo.cart_item_count));

            }else
            {
                error(getInfo.message);
            }
            return basicInfo;
          }
        

        const onSubmit = async (data) => {
                let param = { email: data.email, password: data.password };
                
                let loginurl = getApiurl("loginWithEmail");
                let loginResp = await PostRequestFunction(loginurl, {}, param);
              
                if(loginResp.status)
                {
                    let token = loginResp.data[0].login_token;
                    let info = await getBasicInformation(token);
                    let set = await setAsyncStorageFunction('mixdelight_token', token);
                    success('Login Successfully');
                    //history.goBack();
                    history.push('/profile');

                }else
                {
                    error(loginResp.message);
                    console.log(loginResp.message)
                }
           };

           async function signInWithGoogleAsync() {
                try {
                const result = await Google.logInAsync({
                    androidClientId: '185933748013-secc5tnb0i0p2a0k4tkvlga0gpm853go.apps.googleusercontent.com',
                    webClientId:'185933748013-ivf8bbiqo6cru53ceb4iuq1d8m0vc383.apps.googleusercontent.com',
                    scopes: ['profile', 'email'],
                });

                if (result.type === 'success') {
                    let user = result.user;
                    console.log(user)
                    handleGoogleLogin(user);
                } else {
                    error(result.message);
                    
                }
                } catch (e) {
                    alert(e)
                    
                }
            }
           

        async function fblogIn() {
            try {
                const {
                    type,
                    token
                } = await Facebook.logInWithReadPermissionsAsync('134170801550907', {
                    permissions: ['public_profile'],
                });
                if (type === 'success') {
                    // Get the user's name using Facebook's Graph API
                    fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,name,email,picture.height(500)`)
                    .then(response => response.json())
                    .then(data => {
                        handleFacebookLogin(data)
                        
                    })
                    .catch(e => console.log(e))
                } else {
                    // type === 'cancel'
                }
                } catch ({ message }) {
                alert(`Facebook Login Error: ${message}`);
                }
        }

        const onPressg = () => {
            if (googleLogin) {
                signOutAsync();
            } else {
                signInAsync();
            }
        };

        const signOutAsync = async () => {
            await GoogleSignIn.signOutAsync();
                setGoogleLogin(false)
        };

        return (
            <View style={{height:'100%'}}>
            <MobileHeader />
            <Header />
            <View style={styles.pageArea}>
            <ScrollView style={styles.container}>
                <View>
                <Row>
                <Col xs={12} sm={12} md={2} lg={4} xl={4}></Col>
                <Col xs={12} sm={12} md={8} lg={4} xl={4} style={{marginHorizontal:10}}>
                <View style={styles.loginDiv}>
                    <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Image style={styles.image} 
                        source={{ uri: 'https://images.vexels.com/media/users/3/147063/isolated/preview/43ddb672bf0c4afd27685e053d14277f-grey-birthday-cake-logo-by-vexels.png' }} />
                    </View>
                    <View style={{ marginTop: 10, alignItems: "center", justifyContent: "center" }}>
                        <Text style={[styles.text, {fontSize: 22, fontWeight: "500" }]}>Sign In</Text>
                    </View>

                    {!isWeb?<View style={{ marginTop: 30, flexDirection: "row", justifyContent: "center" }}>
                        <TouchableOpacity onPress={fblogIn}>
                            <View style={styles.socialButton}>
                                <Image source={{ uri: 'https://pngimage.net/wp-content/uploads/2018/06/%E0%B9%84%E0%B8%AD%E0%B8%84%E0%B8%AD%E0%B8%99facebook-png-2.png' }} style={styles.socialLogo} />
                                <Text style={styles.text}>Facebook</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.socialButton} onPress={onPressg}>
                            <Image source={{ uri: 'https://image.flaticon.com/icons/png/512/270/270014.png'}} style={styles.socialLogo} />
                            <Text style={styles.text}>Google</Text>
                        </TouchableOpacity>
                    </View>:null}

                    <Text style={[styles.text, { color: "#ABB4BD", fontSize: 15, textAlign: "center", marginVertical: 20 }]}>{isWeb?'':'OR'}</Text>

                    <View style={{paddingHorizontal:20}}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                        <TextInput style={[styles.input,{marginTop: 10,marginBottom: 8}]}
                        onBlur={onBlur}
                        placeholder="Email"
                            onChangeText={(value) => onChange(value)} value={value}/>
                        )}
                        name="email"
                        defaultValue=""
                        rules={{ required: "This is required.",
                            validate: {
                                pattern: (value) => value && /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) || 'The Email must be a valid email address',
                            }
                        }
                      }
                    />
                    {errors.email && <Text style={styles.errorText}>{errors.email.message}</Text>}

                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                        <TextInput style={[styles.input,{marginTop: 10,marginBottom: 8}]}
                        secureTextEntry={true} 
                        onBlur={onBlur}
                        placeholder="Password"
                            onChangeText={(value) => onChange(value)} value={value}/>
                        )}
                        name="password"
                        defaultValue=""
                        rules={{ required: "This is required.", 
                            validate: {
                                minlength: (value) => value && (value.length >= 8 && value.length <= 15)  || 'The password length must be between 8 & 15 characters',
                            }
                        }
                      }
                    />
                    {errors.password && <Text style={styles.errorText}>{errors.password.message}</Text>}
                    {/* <Text style={[styles.text, styles.link, { textAlign: "right" }]}>Forgot Password?</Text> */}

                    <TouchableOpacity style={styles.submitContainer} onPress={handleSubmit(onSubmit)}>
                        <Text
                            style={[
                                styles.text,
                                {
                                    color: "#FFF",
                                    fontWeight: "600",
                                    fontSize: 16
                                }
                            ]}
                        >
                            Login
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>history.push('/signup')}>
                    <Text
                        style={[
                            styles.text,
                            {
                                fontSize: 14,
                                color: "#ABB4BD",
                                textAlign: "center",
                                marginTop: 24
                            }
                        ]}
                    >
                        Don't have an account? <Text style={[styles.text, styles.link]}>
                          Register Now
                          </Text>
                    </Text>
                    </TouchableOpacity>
                    </View>
                    {/* <TouchableOpacity style={styles.submitContainer} onPress={onSet}>
                        <Text
                            style={[
                                styles.text,
                                {
                                    color: "#FFF",
                                    fontWeight: "600",
                                    fontSize: 16
                                }
                            ]}
                        >
                            set
                        </Text>
                    </TouchableOpacity> */}
                    {/* <TouchableOpacity style={styles.submitContainer} onPress={onget}>
                        <Text
                            style={[
                                styles.text,
                                {
                                    color: "#FFF",
                                    fontWeight: "600",
                                    fontSize: 16
                                }
                            ]}
                        >
                            get
                        </Text>
                    </TouchableOpacity> */}
                    {/* <TouchableOpacity style={styles.submitContainer} onPress={clear}>
                        <Text
                            style={[
                                styles.text,
                                {
                                    color: "#FFF",
                                    fontWeight: "600",
                                    fontSize: 16
                                }
                            ]}
                        >
                            clear
                        </Text>
                    </TouchableOpacity> */}
                </View>
                </Col>
                <Col xs={12} sm={12} md={2} lg={4} xl={4}></Col>
                </Row>
                </View>
                
                </ScrollView>
            </View>
            <Footer />
            <MobileFooter />
            </View> 
        );
    
}

const styles = StyleSheet.create({

    pageArea:{
        flexGrow:1,
        zIndex: -10,
        // marginBottom: 20,
        // marginHorizontal:20,
        // borderWidth:1,
        borderColor:theme.Hash666,
        marginVertical: isWeb? 0:60,
    },

    boxshdw:{
      backgroundColor: theme.white,
      elevation: 5,
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      borderRadius:3,

    },
    loginDiv : {
      backgroundColor: theme.white,
      elevation: 8,
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      borderRadius:3,
      marginTop: 30,
      paddingVertical: 20
    },
    container: {
        flex: 1,
        // backgroundColor: "#fff",
        // paddingHorizontal: 25,
        paddingHorizontal: 5,
        marginHorizontal:10,
        marginBottom:20,
        paddingVertical:1
    },
    image: {
      height: 85,
      width:100
    },
    input: {
      borderWidth:0.5,
      paddingVertical: 10,
      paddingHorizontal: 10,
      color: "#1D2029",
      fontSize: 14,
      borderRadius:5,
      //fontFamily: "Avenir Next",
      borderColor:'#FF1654'
    },
    text: {
        //fontFamily: "Avenir Next",
        color: "#1D2029"
    },
    socialButton: {
        flexDirection: "row",
        marginHorizontal: 12,
        paddingVertical: 12,
        paddingHorizontal: 30,
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: "rgba(171, 180, 189, 0.65)",
        borderRadius: 4,
        backgroundColor: "#fff",
        shadowColor: "rgba(171, 180, 189, 0.35)",
        shadowOffset: { width: 0, height: 10 },
        shadowOpacity: 1,
        shadowRadius: 20,
        elevation: 5
    },
    socialLogo: {
        width: 16,
        height: 16,
        marginRight: 8
    },
    link: {
        color: "#FF1654",
        fontSize: 14,
        fontWeight: "500"
    },
    submitContainer: {
        backgroundColor: "#FF1654",
        fontSize: 16,
        borderRadius: 4,
        paddingVertical: 12,
        marginTop: 32,
        alignItems: "center",
        justifyContent: "center",
        color: "#FFF",
        shadowColor: "rgba(255, 22, 84, 0.24)",
        shadowOffset: { width: 0, height: 9 },
        shadowOpacity: 1,
        shadowRadius: 20,
        elevation: 5
    },
    errorText:{
        color:"#FF1E1E",
        marginBottom:5
    } 
});