import React, { useState, useEffect } from "react";
import { ScrollView,View,FlatList,Dimensions,SafeAreaView,BackHandler,Platform, Text} from 'react-native';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import ProductCardMolecule from './molecule/ProductCardMolecule';
// import { isMobile } from 'react-device-detect';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { getApiurl } from "../api/ApiKeys";
import { Link, useHistory } from '../Route/routing';
import { PostRequestFunction,GetRequestFunction } from "../api/ApiHelper";
import FilterSortButton from "./atom/FilterSortButton";
import { useDispatch, useSelector } from 'react-redux';
import DeskFilterSortButton from "./atom/DeskFilterSortButton";


const isWeb = Platform.OS === 'web';

export default function CakeListing() {
  let history = useHistory();
  const [cakes, setCakes] = useState([]);
  const width = Dimensions.get('window').width;

  const flavourRedux = useSelector(state => state.flavourReducer.flavoured)
  const shapeRedux = useSelector(state => state.shapeReducer.shaped)
  const vendorRedux = useSelector(state => state.vendorReducer.vendors)
  const weightRedux = useSelector(state => state.weightReducer.weights)
  const typeRedux = useSelector(state => state.cakeTypeReducer.types)
  const reduxSort = useSelector(state => state.sortReducer.sortOpt)

  useEffect(() => {
    getCakes();
    handleBackHnadler();
   
   
  },[]);

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }

  async function getCakes(param={})
  {
    // let param = { category: paramericvalue, page_limit: limit, page: page };
    let getcakeUrl = getApiurl("getallproduct");
    let getProductList = await GetRequestFunction(getcakeUrl, {}, param);
    if (getProductList.status)
    {
      let cakelist = getProductList.data;
      setCakes(cakelist);
    } 
  }

  async function getfilterdata(flavourEle,typesEle,shapeEle,vendorEle,weightEle)
  {
      let param = { 
        flavours: flavourEle, 
        shapes: shapeEle, 
        vendors: vendorEle,
        weights: weightEle,
        types:typesEle,
        sortOpt : reduxSort
       };
      let justassign = await getCakes(param);


    //  let getcakeUrl = getApiurl("getallproduct");
    //  let getProductList = await GetRequestFunction(getcakeUrl, {}, param);
    //  console.log(getProductList)
  }

  async function getSortingData(sortopt)
  {
    let param = { 
      flavours: flavourRedux, 
      shapes: shapeRedux, 
      vendors: vendorRedux,
      weights: weightRedux,
      types: typeRedux,
      sortOpt : sortopt
     };

     let justassign = await getCakes(param);
  }

  
 
  return (
    
    <SafeAreaView style={{flex:1}}>
        <MobileHeader />
        <Header />
        <SafeAreaView style={styles.outerContainer}>
          <DeskFilterSortButton getfilterdata={getfilterdata} getSortingData={getSortingData}/>
          <FlatList
              data={cakes}
              scrollEnabled={true}
              initialNumToRender={10}
              numColumns={width>768?4:2}
              keyExtractor={item => item.id}
              renderItem={({ item, index, separators }) => (
                  <Col xs={6} sm={6} md={4} lg={3} xl={3}>
                    <View style={styles.container}>
                      <ProductCardMolecule cakedata={item}/>
                    </View>
                    {/* <View>
                      <FilterSortButton />
                    </View> */}
                  </Col>
              )}
          /> 
          </SafeAreaView>
          <Footer />
        {/* <MobileFooter /> */}
       <View style={styles.stickyContainer}> 
          {width < 768?<FilterSortButton getfilterdata={getfilterdata} getSortingData={getSortingData}/>:null}
        </View>
      </SafeAreaView>
  );
}

const base = {
  outerContainer: {
    flex: 1,
    paddingHorizontal: 80,
    paddingVertical: 20,
    marginVertical: isWeb? 0:0,
    // paddingHorizontal: 0,
    // paddingVertical: 0,
  },
  stickyContainer:{
    display: 'flex',
    position: isWeb? 'sticky':'relative',
    bottom: 0,
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(1024, {
      outerContainer: {
        paddingHorizontal: 0,
        paddingTop: isWeb? 10:60,
        paddingBottom: isWeb? 10:0,
      },
    })
  )
);
