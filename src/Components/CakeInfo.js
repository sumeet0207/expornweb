import React from 'react';
import {View, Text, StyleSheet, Button, Platform} from 'react-native';
import { Link,useParams,useLocation } from '../Route/routing';

const isWeb = Platform.OS === 'web';

function CakeInfo({ match }) {
    let { cakeurl } = useParams();
    let location = useLocation();
    console.log(location.pathname);
    return(<View>
            <Text>Parameter : {cakeurl}</Text>
            <Link to="/cart"><Text>Add to cart</Text></Link>
          </View>)
 }

export default CakeInfo
