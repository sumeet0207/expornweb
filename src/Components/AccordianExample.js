import React, { Component } from 'react';
import { SafeAreaView, LayoutAnimation,UIManager, View, StyleSheet,Platform,ScrollView } from 'react-native';
import Accordion_Panel from './Uncommon/Accordian';

export default class AccordianExample extends Component {

    constructor() {
      super();
  
      if (Platform.OS === 'android') {
  
        UIManager.setLayoutAnimationEnabledExperimental(true)
  
      }
  
      const array = [
  
        { expanded: false, title: "Panel 1", body: "Hello Guys this is the Animated Accordion Panel." },
        { expanded: false, title: "Panel 2", body: "Hello Guys this is the Animated Accordion Panel." },
  
      ];
  
      this.state = { AccordionData: [...array] }
    }
  
    update_Layout = (index) => {
  
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  
      const array = this.state.AccordionData.map((item) => {
  
        const newItem = Object.assign({}, item);
        if(newItem.expanded)
        {
            newItem.expanded = false;
        }
        newItem.expanded = false;
        
  
        return newItem;
      });
  
      array[index].expanded = true;
  
      this.setState(() => {
        return {
          AccordionData: array
        }
      });
    }
  
    render() {
      return (
        <View style={styles.MainContainer}>
  
          <ScrollView contentContainerStyle={{ paddingHorizontal: 10, paddingVertical: 5 }}>
            {
              this.state.AccordionData.map((item, key) =>
                (
                  <Accordion_Panel key={key} onClickFunction={this.update_Layout.bind(this, key)} item={item} />
                ))
            }
          </ScrollView>
  
        </View>
      );
    }
  }
  
  const styles = StyleSheet.create({
  
    MainContainer: {
      flex: 1,
      justifyContent: 'center',
    },
  
    Panel_text: {
      fontSize: 18,
      color: '#000',
      padding: 10
    },
  
    Panel_Button_Text: {
      textAlign: 'center',
      color: '#fff',
      fontSize: 21
    },
  
    Panel_Holder: {
      borderWidth: 1,
      borderColor: '#FF6F00',
      marginVertical: 5
    },
  
    Btn: {
      padding: 10,
      backgroundColor: '#FF6F00'
    }
  
  });
