import React from 'react';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import { Link } from '../Route/routing';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

export default function PlaceOrder() {
  
  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View style={styles.container}>
            <Text>This is Place order Screen</Text>
            <Link to="/cakeinfo/1"><Text>Cake info</Text></Link>
            <Link to="/add/address"><Text>Add Address</Text></Link>
            <Link to="/orders"><Text>Orders</Text></Link>
          </View>
      </ScrollView>
    </View>
    <Footer />
    <MobileFooter />
    </View> 
  );
}

const styles = StyleSheet.create({

  pageArea:{
    flexGrow:1
  },

  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});