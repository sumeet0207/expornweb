import { StyleSheet, Text, FlatList,View, ScrollView, Dimensions, SafeAreaView,BackHandler, Platform } from 'react-native';
import { useLocation,Link, useHistory } from '../Route/routing';
import React, { useState, useEffect } from "react";
import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

import ProductImg from '../Components/atom/ProductImg';
import TitleText from '../Components/atom/TitleText';
import CartCount from '../Components/atom/CartCount';
import Price from '../Components/atom/Price';
import CartItemMolecule from '../Components/molecule/CartItemMolecule';
import CartDetail from '../Components/molecule/CartDetail';
import { GetRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import { getAsyncStorageFunction } from "./services/CookieModule";

import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { setCartCount } from "../store/action/cartAction";
import { useDispatch, useSelector } from 'react-redux';
import NotFound from '../Components/NotFound';
import { useShow } from "./services/AlertHook";

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function Cart() {
  // let location = useLocation();
  // console.log(location.pathname);
  let history = useHistory();
  const { show, error, success } = useShow();
  const dispatch = useDispatch();
  const [isLogin, setIsLogin] = useState(false);
  const [cakes, setCartCake] = useState([]);
  const [accessories, setAccessories] = useState([]);
  const [cartCount, setlocalCartCount] = useState(0);
  const [orderAmount, setorderAmount] = useState(0);
  
  useEffect(() => {
    getCartDetails();
    handleBackHnadler();
   
  },[]);

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }


  async function getCartDetails()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    //console.log(mixdelight_token)
      if(mixdelight_token)
      {
        setIsLogin(true);
        let header = { Token: mixdelight_token };
        let cartUrl = getApiurl("getCartDetails");
        let cartList = await GetRequestFunction(cartUrl,header, {});
        console.log(cartList)
        if(cartList.status)
        {
            setlocalCartCount(cartList.count);
            dispatch(setCartCount(cartList.count));

            if(cartList.count > 0)
            {
              let data = cartList.data;
              setorderAmount(data.calculated_amount);
              setCartCake(data.orderProduct);
            }
        }else
        {
          error(cartList.message)
          console.log("no data")
        }
      }else
      {
        console.log("Must login")
      }
  }

  if (isLogin && cartCount < 1) {
    return(
        <View style={{height:'100%'}}>
          <NotFound heading={'Your cart is empty !'}
            imgsrc={'https://www.onlinetestsindia.com/assets/images/empty_cart.jpg'}
            passurl={'/'}/>
        </View>

    )
  }

  if (isLogin==false) {
    return(
        <View style={{height:'100%'}}>
          <NotFound heading={'Please Login !'}
            imgsrc={'https://www.onlinetestsindia.com/assets/images/empty_cart.jpg'}
            btnName={"LOGIN"}
            passurl={'/login'}/>
        </View>

    )
  }

  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
        <View style={styles.cartHeading}>
          <TitleText
            title="My Cart"
            titleSize={theme.size22}
            mb_titleSize={theme.size18}
            titleAlign={'left'}
            txtColor={theme.ogBlack}
          />
          <View style={{ paddingHorizontal: 5 }}>
            <CartCount count={cartCount} countSize={theme.size18} />
          </View>
        </View>

        {/* <View style={{ paddingVertical: 10 }}>
          <View style={styles.borderBotm100}></View>
        </View> */}

        <View>
          <Row>
          <Col xs={12} sm={12} md={6} lg={8} xl={8}>
            <FlatList
                data={cakes}
                scrollEnabled={true}
                initialNumToRender={4}
                numColumns={1}
                keyExtractor={item => item.id}
                renderItem={({ item, index, separators }) => (
                  <CartItemMolecule product={item} 
                   setCartCake={setCartCake} 
                   setlocalCartCount={setlocalCartCount}
                   setorderAmount={setorderAmount}/>
                )}
            /> 
            </Col>
            {/* <Col xs={12} sm={12} md={6} lg={8} xl={8}>
              <CartItemMolecule />
              <CartItemMolecule />
            </Col> */}
            <Col xs={12} sm={12} md={6} lg={4} xl={4}>
              <View>
                <CartDetail cartAmount={orderAmount}/>
              </View>
            </Col>
          </Row>
        </View>
        
        </ScrollView>
      </View>
       <Footer />
       <MobileFooter />
       </View> 
      
  );
}

const base = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 50,
    flexGrow:1,
    zIndex: -15,
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
  cartHeading:{
    flexDirection: 'row',
    marginVertical:15,
    alignItems:'center',
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 15,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
      cartHeading:{
        marginVertical:10,
      }
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
      cartHeading:{
        marginVertical:10,
      }
    })
  )
);
