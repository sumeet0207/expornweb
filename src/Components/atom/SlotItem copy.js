import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, Platform } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Radiobutton from '../Uncommon/RadioButton';
import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from './TitleText';


const isWeb = Platform.OS === 'web';

export default function SlotItem() {
  const [slotRadio, setslotRadio] = useState('1'); 

  let slot_radio_props = !isWeb
    ? [
        { value: '08:00 - 12:00', label: '08:00 - 12:00 hrs' },
        { value: '12:00 - 15:00', label: '12:00 - 15:00 hrs' },
        { value: '15:00 - 18:00', label: '15:00 - 18:00 hrs' },
        { value: '18:00 - 21:00', label: '18:00 - 21:00 hrs' },
      ]
    : [
        { value: '08:00 - 12:00', label: '08:00 - 12:00 hrs' },
        { value: '12:00 - 15:00', label: '12:00 - 15:00 hrs' },
        { value: '15:00 - 18:00', label: '15:00 - 18:00 hrs' },
        { value: '18:00 - 21:00', label: '18:00 - 21:00 hrs' },
      ];

  function getSlotRadioval(val) {
    console.log(val);
    setslotRadio(val);
  }

  return (
    <View>
      <TitleText
        title={'Select Time Slot'}
        titleSize={theme.size18}
        mb_titleSize={theme.size14}
        titleFont={'MontserratSemiBold'}
      />
      {/* <hr /> */}
      <View style={styles.container}>
        <Radiobutton
          types={slot_radio_props}
          val={slotRadio}
          onPress={getSlotRadioval}
          row={true}
          className={styles.test}
        />
      </View>
    </View>
  );
}

const base = {
  container: {
    // flex: 1,
    flexDirection: 'row',
    display: 'flex',
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
  test:{
    paddingVerticle: 20,
    paddingHorizontal :20
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(400, {
      container: {
        // backgroundColor: 'blue',
      },
    })
  )
);
