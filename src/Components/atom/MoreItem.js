import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Row, Col } from 'react-native-responsive-grid-system';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import ProductImg from '../atom/ProductImg';
import ProductName from '../atom/ProductName';
import Price from '../atom/Price';

const { width, height } = Dimensions.get('window');

const MoreItem = (props) => {
  const { heading, desc, name, rdate,access } = props;

  let totalPrice = parseInt(access.quantity) * parseInt(access.price);
  //console.log(totalPrice)
  return (
    <View>
      <View style={{ flexDirection: 'row', marginBottom: 5 }}>
        <View style={styles.pimg}>
          <ProductImg ImgHeight={60} mb_ImgHeight={45} 
            imgsrc={access.image || ''}
           />
        </View>
        <View style={styles.leftPdng}>
          <ProductName
            pname={access.name || ''}
            pnameSize={theme.size12}
            mb_pnameSize={theme.size11}
            pnameBotm={1}
            pnameAlign={'left'}
            pnameFont={'MontserratRegular'}
          />
          <View>
            <Text style={styles.normalText}>{`Qty: ${access.quantity || 0}`}</Text>
          </View>
          <View>
            <Price
              price={totalPrice}
              titleAlign={'left'}
              titleSize={theme.size12}
              mb_titleSize={theme.size11}
              titleFont={'MontserratRegular'}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const base = {
  pimg: {
    width: 60,
  },
  leftPdng: {
    paddingLeft: 8,
  },
  normalText: {
    fontSize: theme.size12,
    color: theme.Hash666,
    fontFamily: 'MontserratRegular',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      pimg: {
        width: 45,
      },
      normalText: {
        fontSize: theme.size11,
        marginTop: -3,
      },
    })
  )
);

export default MoreItem;
