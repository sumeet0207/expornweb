import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

import { theme } from '../../Theme/theme';

const WeightBtn = (props) => {
  const styles = getStyle(props);

  const [isActive, setIsActive] = useState(true);

  return (
    <View>
      <TouchableWithoutFeedback
        onPress={() => {
          setIsActive(!isActive);
        }}
      >
        {isActive ? (
          <View style={styles.container}>
            <View style={[styles.btnOuter, styles.btnActive]}>
              <Text style={[styles.btnText, styles.btnActiveText]}>
                {props.wt} {props.unit}
              </Text>
            </View>
          </View>
        ) : (
          <View style={styles.container}>
            <View style={[styles.btnOuter]}>
              <Text style={[styles.btnText]}>
                {props.wt} {props.unit}
              </Text>
            </View>
          </View>
        )}
      </TouchableWithoutFeedback>
    </View>
  );
};

const getStyle = (props) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
      paddingTop: 7,
      paddingBottom: 15,
    },
    btnOuter: {
      elevation: 8,
      alignSelf: 'center',
      justifyContent: 'center',
      backgroundColor: theme.white,
      borderColor: theme.lightGreen,
      borderWidth: 1,
      width: props.btnWidth || 78,
      height: props.btnHeight || 30,
      borderRadius: 50,
      marginRight: 8,
    },
    btnText: {
      fontSize: props.btnFsize || theme.size14,
      fontFamily: props.titleFont || 'MontserratBold',
      alignSelf: 'center',
      justifyContent: 'center',
      color: theme.lightGreen,
      textAlignVertical: 'center',
      marginBottom:0
    },
    btnActive: {
      backgroundColor: theme.lightGreen,
    },
    btnActiveText: {
      color: theme.white,
    },
  });
  return styles;
};

export default WeightBtn;
