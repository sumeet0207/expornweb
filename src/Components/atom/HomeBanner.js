import React from 'react';
import { View, StyleSheet, Image, Dimensions, Text, ImageBackground, Button, Alert ,TouchableOpacity  } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme } from '../../Theme/theme';
import RedirectBtn from '../atom/RedirectBtn';
import {useHistory } from '../../Route/routing';
import { useShow } from "../services/AlertHook";
// import RazorpayCheckout from 'react-native-razorpay-expo';
// import StripeCheckout from 'expo-stripe-checkout';
import * as WebBrowser from 'expo-web-browser';


const { width, height } = Dimensions.get('window');
var paymentwindow = ''

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    imgWrap: {
      height: props.ImgHeight || 400,
    },
    imgstyle: {
      height: '100%',
      width: '100%',
      // opacity:0.5 
    },
    bannerText:{
      textTransform:'uppercase',
      color: theme.white,
      //fontSize: 50,
      fontSize: 25,
      fontFamily: 'MontserratSemiBold',
      paddingLeft: 10,
      // fontWeight: 600,
    },
    hombtn:{
      color: theme.darkbg,
      // BackgroundColor: theme.grey,
      paddingHorizontal: 50
    }
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        imgWrap: {
          height: props.mb_ImgHeight || 200,
        },
        bannerText:{
          fontSize: 20,
          paddingLeft:27
        }
      })
    )
  );
  return pstyles;
};

const HomeBanner = (props) => {
  const { imgsrc } = props;
  const pstyles = getStyle(props);
  let history = useHistory();
  const { show, error, success } = useShow();

  function openAlert(){
   
     history.push('/cakelisting')
  
  }


  function abcd()
  {
    paymentwindow = window.open('https://www.w3schools.com?s=100&p[title]=MD Share&p[summary]=Facebook share popup&p[url]=javascript:&p[images][0]="http://goo.gl/dS52U"', 'sharer', 'toolbar=0,status=0,width=548,height=325');
    console.log(window.location.href)
    //paymentwindow.close();
  }

  function closing()
  {
    WebBrowser.maybeCompleteAuthSession()
    //paymentwindow.close();
  }


  return (
    <View>
      <View style={pstyles.imgWrap}>
      <ImageBackground source={require('../../assets/img/bannerthree.jpg')} style={{width: '100%', height: '100%'}}>
        <View style={{position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center'}}>
        <Text style={pstyles.bannerText}>Lets celebrate every moment with our home made cake</Text>
        <RedirectBtn rbtnTxt="Shop Now" onPress={openAlert}/>
        </View>
      </ImageBackground> 
      
      
      

      
      </View>
    </View>
  );
};

export default HomeBanner;
