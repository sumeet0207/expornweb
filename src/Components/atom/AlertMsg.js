import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

const isWeb = Platform.OS === 'web';

export default function AlertMsg(props) {
  // const { Mtext, Micon } = props;
  const { Mtext, Micon,alertmsg } = props;

  return (
    <View>
      <View>
            <View style={styles.errorItem}>
              <MaterialCommunityIcons
                name="alert-circle-outline"
                style={[styles.errorICon]}
                size={24}
              />
              <Text style={styles.errorText}>{alertmsg}</Text>
              {/* <Text style={styles.errorText}>Email address registered with another method</Text> */}
              <TouchableOpacity>
                <MaterialCommunityIcons name="close" style={[styles.erClosICon]}
                size={24} />
              </TouchableOpacity>
            </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  errorItem: {
    elevation: 8,
    backgroundColor: theme.error,
    borderColor: theme.white,
    borderWidth: 0.5,
    width: 330,
    marginHorizontal: isWeb? 0:25,
    minHeight: 45,
    padding: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    flex:1,
    justifyContent: isWeb? 'center':'space-around',
    alignItems: 'center',
  },
  errorICon: {
    color: theme.white,
    marginRight: isWeb? 10:0,
    paddingHorizontal: isWeb? 0:12,
  },
  erClosICon:{
    color: theme.white,
    paddingRight: isWeb? 0:10,
  },
  errorText: {
    fontSize: theme.size14,
    fontFamily: 'MontserratRegular',
    alignSelf: 'center',
    justifyContent: 'center',
    color: theme.lightGreen,
    textAlignVertical: 'center',
    color: theme.white,
    paddingHorizontal: isWeb? 0:30,
    flexGrow: isWeb? 1:0
  },
});
