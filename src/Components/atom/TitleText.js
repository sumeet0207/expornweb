import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme } from '../../Theme/theme';

// const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const base = {
    titleClass: {
      fontSize: props.titleSize || theme.size16,
      marginBottom: props.titleBotm || 6,
      color: props.txtColor || theme.darkbg,
      fontFamily: props.titleFont || 'MontserratRegular',
      textAlign: props.titleAlign || 'center',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        titleClass: {
          fontSize: props.mb_titleSize || theme.size16,
        },
      })
    )
  );
  return pstyles;
};

const TitleText = (props) => {
  const pstyles = getStyle(props);

  return (
    <View>
      <Text style={pstyles.titleClass}>{props.title}</Text>
    </View>
  );
};

export default TitleText;
