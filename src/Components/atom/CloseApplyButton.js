import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, Modal, Platform } from 'react-native';
import { theme } from '../../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const isWeb = Platform.OS === 'web';


const CloseApplyButton = (props) => {

    return (
        <View>
            <View style={{flexDirection:'row', flex:1}}>
                <TouchableOpacity 
                style={[styles.btnArea,styles.sortStyle]} 
                onPress={props.onPress} onPress={props.onPress}
                >
                    <View>
                        <Text style={{color: theme.darkbg, fontSize: theme.size14,fontFamily: 'MontserratRegular'}}>CLOSE</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.btnArea, styles.filterStyle]} onPress={props.applyfilter}>
                    <View>
                        <Text style={{color: theme.mbHFColor, fontSize: theme.size14,fontFamily: 'MontserratRegular'}}>APPLY</Text>
                    </View>
                </TouchableOpacity >
            </View>

        </View>
    )
}

const base = {

    btnArea : {
        flex:0.5,
        backgroundColor:theme.white,
        height: 50,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center'
    },
    sortStyle:{
        borderWidth:0.4,
        // borderRightWidth:0,
        // borderLeftWidth:0,
        borderColor:'#ccc',
        // borderTopWidth: 0.4,
    },
    filterStyle:{
        borderWidth: 0.4,
        borderLeftWidth:0.1,
        borderColor:'#ccc',
    },
  };
  
  const styles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(1024, {

        

      })
    )
  );
  

export default CloseApplyButton
