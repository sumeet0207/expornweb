import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { Link } from '../../Route/routing';

import { theme, setFontStyle } from '../../Theme/theme';
import TitleText from './TitleText';

const { width, height } = Dimensions.get('window');

const SlotItem = (props) => {
  const { slotArray,setDeliverySlot,setDeliverySlotLabel,openSlotModal,deliverySlot } = props;
  //console.log(slotArray);

  function getslot(key,label)
  {
    //console.log(key,label)
    setDeliverySlot(key);
    setDeliverySlotLabel(label);
    openSlotModal(false)
  }
  
  return (
    <View>
      <View style={styles.headingText}>
        <TitleText
          title={'Select Time Slot'}
          titleSize={theme.size18}
          mb_titleSize={theme.size16}
          titleFont={'MontserratSemiBold'}
        />
      </View>

      <View style={styles.slotWrap}>
        {slotArray.map(slt => (
          <TouchableOpacity key={slt.value} 
             
             onPress={()=>getslot(slt.value,slt.label)}>
            <View style={styles.timediv}>
              <View style={[styles.timeText]} >
                <Text style={{'color': slt.value==deliverySlot?'#ff5200de':'black'}}>
                  {slt.label}
                </Text>
                </View>
            </View>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

const base = {
  timediv: {
    padding: 10,
    height:50,
    width:250,
    marginTop: 7,
    backgroundColor: theme.white,
    borderRadius: 2,
    borderWidth: 1,
    backgroundColor: theme.white,
    borderColor: theme.Hash666,
    borderWidth:1,
    margin:'auto',
    alignItems:'center',
    // paddingTop:5
  },
  timeText:{
    textAlign: 'center',
    fontSize: theme.size14,
    color: theme.ogBlack,
    fontFamily: 'MontserratRegular',
    marginTop:4
  },

  slotWrap: {
    // flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      headingText : {
        marginTop:8
      }
    })
  )
);

export default SlotItem;
