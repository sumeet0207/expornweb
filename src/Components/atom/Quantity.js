import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

import { AntDesign } from '@expo/vector-icons';
import { theme } from '../../Theme/theme';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    aicon: {
      fontSize: props.aicon || theme.size20,
      color: theme.Hash666
    },
    amoutxt: {
      fontSize: props.qtySize || theme.size17,
      paddingHorizontal: 10,
      paddingVertical: 0,
      marginVertical: 0,
      color: theme.black,
      backgroundColor: theme.white,
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        aicon: {
          fontSize: props.mb_aicon || theme.size22,
        },
        amoutxt: {
          fontSize: props.qtySize || theme.size14,
        },
      })
    )
  );
  return pstyles;
};

const Quantity = (props) => {
  const { imgsrc } = props;
  const pstyles = getStyle(props);
  const {inCreaseCount,quatCount,decreaseCount,iscart=false}=props;
  const [accessCount, setAccessoriesCount] = useState(quatCount);

  function incrementAccessCount()
  {
    if(!iscart)
    {
      inCreaseCount()
    }
    
    props.updateAccessoriesQuantity(accessCount + 1);
    setAccessoriesCount(accessCount + 1)
    
  }

  function decrementAccessCount()
  {
    let checkwith = !iscart?0:1;
    if(accessCount > checkwith)
    {
      if(!iscart)
      {
        decreaseCount();
      }
      props.updateAccessoriesQuantity(accessCount - 1);
      setAccessoriesCount(accessCount - 1)
    }
    
  }



  return (
    <View>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={pstyles.margRight6} onPress={decrementAccessCount}>
          <AntDesign name="minussquareo" style={pstyles.aicon} color="black"/>
        </TouchableOpacity>
        <TouchableOpacity style={pstyles.margRight6}>
            <Text style={pstyles.amoutxt}>{quatCount}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={pstyles.margRight6} onPress={incrementAccessCount}>
          <AntDesign name="plussquareo" style={pstyles.aicon} color="black" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Quantity;
