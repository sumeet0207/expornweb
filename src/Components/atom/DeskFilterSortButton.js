import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, Modal, Platform, Dimensions } from 'react-native';
import { theme } from '../../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import CloseModal from '../atom/CloseModal';
// import FilterAndSortMolecule from '../molecule/FilterAndSortMolecule';
import WebModal from 'react-modal';
import FilterAndSortMolecule from '../FilterAndSortMolecule';
import CloseApplyButton from '../atom/CloseApplyButton';
import SortingMolecule from '../molecule/SortingMolecule';
import { getApiurl } from "../../api/ApiKeys";
import { PostRequestFunction,GetRequestFunction } from "../../api/ApiHelper";

const isWeb = Platform.OS === 'web';

// isWeb?WebModal.setAppElement('#root'):null;
const { width, height } = Dimensions.get('window');


const customStyles = {
    content : {
        padding: 0,
        borderWidth:0,
        width:'50%',
        margin: 'auto',
        overflow: 'inherit',
        boxShadow: 'rgb(0 0 0 / 50%) 0px 0px 4px 0px',
    }
}

function DeskFilterSortButton(props) {

    const [filterModal, openFilterModal] = useState(false);
    const [sortModal, openSortModal] = useState(false);
    const [flavours, setFlavour] = useState([]);
    const [shape, setShapes] = useState([]);
    const [weights, setWeight] = useState([]);
    const [vendors, setVendor] = useState([]);
    const [types, setTypes] = useState([]);
    const [whichfilter, setWhichFilter] = useState('');

    useEffect(() => {
        getAllFilterData();
       
      },[]);
    

    async function getAllFilterData()
    {
        let getfilterUrl = getApiurl("getFilterData");
        let getFilterBaseList = await GetRequestFunction(getfilterUrl, {}, {});
        console.log(getFilterBaseList)
        if(getFilterBaseList.status)
        {
          let data = getFilterBaseList.data[0];
          console.log(data)
          setFlavour(data.flavours);
          setShapes(data.shape);
          setVendor(data.vendors);
          setWeight(data.weights);
          setTypes(data.type);
        }

    }

    function closeSort()
    {
        openSortModal(false);
    }


    return (
        <View>
            
            {!filterModal?
            <View style={styles.wrapper}>
                <View>
                    <TouchableOpacity style={styles.btnStyle} onPress={() => openSortModal(true)}>
                        <Text style={styles.btnTxtStyle}>Sort</Text>
                    </TouchableOpacity>
                </View>
                <View>
                    <TouchableOpacity style={[styles.btnStyle,{marginLeft: 10}]} onPress={() => openFilterModal(true)}>
                        <Text style={styles.btnTxtStyle}>Filter</Text>
                    </TouchableOpacity>
                </View>
            </View>:null}
            {/* </View> */}

            {/* Sort Modal Start */}
            {isWeb?<WebModal isOpen={sortModal} style={customStyles}>                   
                <View>
                    {/* <CloseModal closeTop={14} closeRight={12} onPress={closeSort}/> */}
                    <SortingMolecule closemodal={closeSort} getSortingData={props.getSortingData} />
                    <View style={{position:'absolute',right:0}}>
                    <CloseModal closeTop={14} closeRight={12} onPress={closeSort}/>
                    </View>
                    
                </View>
            </WebModal>:null}
            {/* Sort Modal End */}

            {/* Filter MOdal Start */}
            {isWeb?<WebModal isOpen={filterModal} style={base}>  
                    <FilterAndSortMolecule 
                     flavours={flavours} shape={shape} 
                     vendors={vendors} weights={weights}
                     types={types}
                     onPress={() => openFilterModal(false)}
                     getfilterdata={props.getfilterdata}
                    />
            </WebModal>:null}
            {/* Filter MOdal End */}


        </View>
    );
}

const base = {
    content : {
        padding: 0,
        width:'50%',
        margin: 'auto',
        top:0,
        overflow: 'inherit',
      },

    wrapper:{
        paddingHorizontal:15,
        paddingVertical:10,
        display: 'inline-flex',
        flexDirection: 'row',
        justifyContent: 'flex-end'

    },
    btnTxtStyle:{
        fontSize:16,
        color:'#000',
        textAlign:'center',
        fontFamily: 'MontserratSemiBold',
    },
    btnStyle:{
        border: '2px solid #ea123a',
        paddingHorizontal: 17,
        paddingVertical: 2,
        borderRadius: 20,
        width:80
    }

  };
  
  const styles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(1024, {
        main :{
            position:'sticky',
            bottom: 0
        },
        wrapper:{
            display: 'none'
        }
        

      })
    )
  );
  

export default DeskFilterSortButton
