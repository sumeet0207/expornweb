import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

import { theme } from '../../Theme/theme';

const SignUpButton = (props) => {
  const styles = getStyle(props);
  //console.log(props.paddprop)

  const [isActive, setIsActive] = useState(true);

  return (
    <View>
      <TouchableOpacity onPress={props.onClick}>
        <View style={styles.container}>
          <View style={styles.btnOuter}>
            <Text style={styles.btnText}>{props.navText}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const getStyle = (props) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'row',
      paddingTop: 15,
      paddingBottom: 15,
    },
    btnOuter: {
      elevation: 8,
      alignSelf: 'center',
      justifyContent: 'center',
      backgroundColor: props.upBtnBg || theme.HFColorText,
      borderColor: props.upBtnBrd || theme.HFColorText,
      borderWidth: 2,
      width: props.btnWidth || 78,
      height: props.btnHeight || 30,
      borderRadius: 50,
      marginRight: 8,
      
    },
    btnText: {
      fontSize: props.btnFsize || theme.size14,
      fontFamily: props.titleFont || 'MontserratBold',
      alignSelf: 'center',
      justifyContent: 'center',
      color: props.upBtnColr || theme.white,
      textAlignVertical: 'center',
      textTransform: 'uppercase',
    },
  });
  return styles;
};

export default SignUpButton;
