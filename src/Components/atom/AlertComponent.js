import React, { Fragment } from "react";
import { View, Platform } from "react-native";
import ErrorAlert from './AlertMsg';
import SuccessAlert from './SuccessAlert';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const isWeb = Platform.OS === 'web';


// const AlertTemplate = ({ text, options, type,props}) => {
const AlertTemplate = (props) => {
    
const {text,options,type}=props;
const pstyles = getStyle(props);
let { margin ,...rest }=options?.style;

//let classNam=options?.showType
//let classNam=options.showType
let abc = pstyles.TOP_RIGHT;
let rested = props.rest

return(
    // <View style={rested}>
    <View style={pstyles.outerAlertWrap}>
        <View style={pstyles.TOP_RIGHT}>
            {type === "success" && <SuccessAlert alertmsg={text}  style={options?.pstyles} />}
            {type === "error" && <ErrorAlert alertmsg={text}  style={options?.pstyles} />}
        </View>
        
    </View>
)

}

const getStyle = (props) => {
    const base = {
        TOP_RIGHT:{
            position:  isWeb? 'fixed':'absolute',
            alignItems: 'center',
            justifyContent: 'center',
            right: 20,
            top: 90,
        },

        outerAlertWrap:{
            flex:1
        }
         
    };
  
    const pstyles = createStyles(
      base,
      minWidth(
        100,
        maxWidth(991, {
            TOP_RIGHT:{
              left: 0,
              right: 0,
              top:55,
            },
        
        })
      )
    );
    return pstyles;
  };

export default AlertTemplate;