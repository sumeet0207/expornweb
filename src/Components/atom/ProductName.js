import React from 'react';
import { View, StyleSheet, Text, Dimensions } from 'react-native';
import { theme } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    prodName: {
      fontSize: props.pnameSize || theme.size16,
      marginBottom: props.pnameBotm || 6,
      color: props.txtColor || theme.darkbg,
      fontFamily: props.pnameFont || 'MontserratSemiBold',
      textAlign: props.pnameAlign || 'center',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        prodName: {
          fontSize: props.mb_pnameSize || theme.size15,
        },
      })
    )
  );
  return pstyles;
};

const ProductName = (props) => {
  // const { pname } = props;
  const pstyles = getStyle(props);

  return (
    <View>
      <Text style={pstyles.prodName}>{props.pname}</Text>
    </View>
  );
};

export default ProductName;
