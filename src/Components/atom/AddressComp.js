import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import { useHistory } from '../../Route/routing';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const theme = {
  smallFont: 12,
  middleFont: 12,
  largeFont: 15,
  backColor: 'white',
  primaryGreen: '#0F8F00',
  primaryRed: '#FF1E1E',
};

const AddressComp = (props) => {
  const pstyles = getStyle(props);
  const history = useHistory();
  const {title,address,number,pincode,city,id,deleteShipping}=props;
  let newAddress = `${address} ${city}`
  return (
    <View>
      <View style={pstyles.mainbox}>
        <Text style={pstyles.custName}>{title}</Text>
        <Text style={pstyles.address}>{newAddress}</Text>
        <Text style={pstyles.address}>{pincode}</Text>
        <Text style={pstyles.address}>Contact : {number}</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity
          style={[pstyles.cabinetButton]}
          onPress={() => history.push(`/edit/address/${id}`)}
        >
          <Text style={[pstyles.cabinetButtonText]}>Edit</Text>
        </TouchableOpacity>
        {props.isHide?null:<TouchableOpacity
          style={[pstyles.cabinetButton, pstyles.marLeft]}
          onPress={deleteShipping}
        >
          <Text style={[pstyles.cabinetButtonText]}>Delete</Text>
        </TouchableOpacity>}
      </View>
    </View>
  );
};

const getStyle = (props) => {
  const base = {
    mainbox: {
      flex: 1,
      paddingHorizontal: 0,
      paddingBottom: 10,
      borderRadius: 20,
    },
    marLeft: {
      marginLeft: 15,
    },
    redeemedinfoChild: {
      paddingHorizontal: 15,
      paddingBottom: 15,
      paddingTop: 10,
      borderColor: '#eee',
      borderStyle: 'solid',
    },

    redeemedinfo: {
      paddingBottom: 0,
      // marginTop: 15,
      marginRight: 4,
      marginLeft: 4,
      marginBottom: 15,
      // backgroundColor: 'white',
      // elevation: 8,
      // shadowOffset: { width: 1, height: 1 },
      // shadowOpacity: 0.5,
      // shadowRadius: 5,
      borderRadius: 3,
      borderColor: '#ccc',
      borderWidth: 1,
    },
    custName: {
      // fontSize: theme.largeFont,
      // fontWeight: '600',
      //alignSelf:"left",
      marginBottom: 5,
      fontFamily: 'MontserratSemiBold',
    },
    address: {
      fontSize: theme.middleFont,
      fontWeight: '400',
      //alignSelf:"left",
      marginBottom: 5,
      // marginTop: 5,
      fontFamily: 'MontserratRegular',
      flexWrap: 'wrap',
      display: 'flex',
    },
    subtitle: {
      fontSize: theme.smallFont,
      fontWeight: '400',
      alignSelf: 'center',
      justifyContent: 'center',
      fontFamily: 'MontserratRegular',
    },
    cabinetButton: {
      width: 80,
      height: 40,
      borderRadius: 5,
      paddingVertical: 15,
      justifyContent: 'center',
      alignItems: 'center',
      borderWidth: 1,
      borderColor: theme.primaryRed,
    },
    cabinetButtonText: {
      fontSize: 15,
      fontWeight: 'bold',
      justifyContent: 'center',
      color: theme.primaryRed,
      fontFamily: 'MontserratRegular',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        cabinetButton: {
          width: 60,
          height: 30,
        },
        cabinetButtonText: {
          fontSize: 12,
        },
        mainbox: {
          paddingHorizontal: 8,
        }
      })
    )
  );
  return pstyles;
};

export default AddressComp;
