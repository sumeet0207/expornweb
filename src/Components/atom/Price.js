import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { theme } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

// const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    priceClass: {
      fontSize: props.titleSize || theme.size20, //18
      // marginBottom: 6,
      color: props.txtColor || theme.darkbg,
      fontFamily: props.titleFont || 'MontserratRegular',
      textAlign: props.titleAlign || 'center',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        priceClass: {
          fontSize: props.mb_titleSize || theme.size16,
          marginBottom: 0,
        },
      })
    )
  );
  return pstyles;
};

const Price = (props) => {
  // const { title } = props;
  const pstyles = getStyle(props);

  return (
    <View>
      <Text style={pstyles.priceClass}>₹{props.price}</Text>
    </View>
  );
};

export default Price;
