import React from 'react'
import { View, StyleSheet, 
Text, Image, Dimensions,TouchableOpacity  } from 'react-native'
import { useHistory,Link } from "../../Route/routing";
const theme = {
   smallFont:12,
   largeFont:15,
   primaryRed:"#FF1E1E",
}

const ProfileBox = (props) => {
    const history = useHistory();
    return (
        <View>  
          <View style={styles.redeemedinfo}>
              <View style={styles.redeemedinfoChild}>
                  <View style={styles.mainbox} >
                   <TouchableOpacity  onPress={()=>history.push(props.redirectTo)}>
                      <Image source={{uri: props.image}} style={styles.imgstyle}/>
                    </TouchableOpacity > 
                    <TouchableOpacity  onPress={()=>history.push(props.redirectTo)}>
                      <Text style={styles.brandName}>{props.title}</Text>
                    </TouchableOpacity >
                    <Text style={styles.subtitle}>{props.subtitle}</Text>
                    
                  </View>
              </View>
          </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
  
    mainbox:{
        flex:1, 
        justifyContent:'space-around',
        padding:8,
        borderRadius:20
    },
   redeemedinfoChild:{
        padding:15,
        paddingBottom:10,
        borderColor:'#eee',
        borderBottomWidth: 1,
        borderStyle:'solid',
   },
  
   redeemedinfo:{
     paddingBottom:0,
     marginTop:15,
     marginRight:4,
     marginLeft:4,
     marginBottom:0,
     backgroundColor:"white",
     elevation: 8,
     shadowOffset: { width: 1, height: 1 },
     shadowOpacity: 0.5,
     shadowRadius: 5,
     borderRadius:3
    },
    imgstyle:{
        height: 120, 
        width: 120, 
        alignSelf:"center",
        marginBottom:15
    },
    brandName:{
        fontSize: theme.largeFont, 
        fontWeight: '600',
        alignSelf:"center",
        marginBottom:5
    },
    subtitle:{
        fontSize: theme.smallFont, 
        fontWeight: '400',
        alignSelf:"center",
        justifyContent:"center"
    }

})

export default ProfileBox