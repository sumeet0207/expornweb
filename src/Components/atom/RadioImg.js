import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
import { Feather } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const styles = StyleSheet.create({
    starWrap: {
      // height: props.starHeight || 30,
      // position: 'absolute',
      // left: 8,
      // top: 12,
    },
    starstyle: {
      height: '100%',
      width: '100%',
    },
  });
  return styles;
};

const RadioImg = (props) => {
  const [isFill, setIsFill] = useState(true);

  const styles = getStyle(props);

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          setIsFill(!isFill);
        }}
      >
        {isFill ? (
          <View style={styles.starWrap}>
            <Feather name="circle" size={15} color="black" />
          </View>
        ) : (
          <View style={styles.starWrap}>
            <FontAwesome name="dot-circle-o" size={15} color="black" />
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default RadioImg;
