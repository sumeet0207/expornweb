import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Button,
} from 'react-native';
import { theme, setFontStyle } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const base = {
    cabinetButton: {
      elevation: 8,
      height: props.sbtnHeight || 40,
      width: props.sbtnWidth || '100%',
      alignSelf: 'center',
      justifyContent: 'center',
      borderTopLeftRadius: props.topleftradius || 5,
      borderTopRightRadius: props.toprightradius || 5,
      borderBottomLeftRadius: props.botmleftradius || 5,
      borderBottomRightRadius: props.botmrightradius || 5,
      borderColor: props.sbtnbrdrColor || theme.white,
      borderWidth:1,
    },
    cabinetButtonText: {
      fontSize: props.sbtntxtSiz || theme.size16,
      fontFamily: props.titleFont || 'MontserratSemiBold',
      alignSelf: 'center',
      justifyContent: 'center',
      color: props.subBtncolor || theme.white,
      textAlignVertical: 'center',
      textTransform: props.fontTrans || 'uppercase',
      paddingHorizontal: 10,
    },
    cabinetBack: {
      backgroundColor: props.Colorbg || theme.orange,
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        cabinetButtonText: {
          fontSize: props.mb_sbtntxtSiz || theme.size16,
        },
        cabinetButton: {
          height: props.mb_sbtnHeight || 40,
          width: props.mb_sbtnWidth || '100%',
        }
      })
    )
  );
  return pstyles;
};

const SubmitButton = (props) => {
  const { btnTxt } = props;
  const pstyles = getStyle(props);


  return (
    <View>
      <TouchableOpacity
        style={[pstyles.cabinetButton, pstyles.cabinetBack]}
        onPress={props.onPress}
      >
        <Text style={[pstyles.cabinetButtonText, pstyles.callnowtext]}>
          {props.btnTxt}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default SubmitButton;
