import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  TextInput,
  Dimensions,
} from 'react-native';
import { theme } from '../../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import TitleText from '../atom/TitleText';
import SubmitButton from '../atom/SubmitButton';
import GiveRating from '../atom/GiveRating';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { Input, Image } from 'react-native-elements';

export default function ServiceComp(props) {
  return (
    <View>
          <View style={{ alignItems: 'center', paddingVertical: 10 }}>
            <View style={{alignItems:"center"}}>
              <View
                style={styles.circle}
              >
                <MaterialCommunityIcons name={props.icon} style={styles.icon} />
              </View>
              <View style={{ paddingVertical: 10 }}>
                <TitleText
                  title={props.serviceName}
                  titleSize={theme.size20}
                  txtColor={theme.orange}
                  titleAlign={'center'}
                  titleFont={'MontserratSemiBold'}
                  mb_titleSize={theme.size14}
                />
                <TitleText
                  title={props.serviceDesc}
                  titleSize={theme.size14}
                  txtColor={theme.darkgrey}
                  titleAlign={'center'}
                  mb_titleSize={theme.size12}
                />
              </View>
            </View>
          </View>
    </View>
  );
}

const base = {
  icon :{
    fontSize: 36,
    color: theme.orange,
  },
  circle :{
    padding: 10,
    borderWidth: 5,
    borderColor: theme.HFColorText,
    // backgroundColor: ('linear-gradient(-187.71deg, #ff005c 62%, #f77800 98%)'),
    width: 120,
    height: 120,
    borderRadius: 120 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  }
  
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(991, {
      circle :{
        width: 80,
        height: 80,
        borderRadius: 80 / 2,
      },
      icon :{
        fontSize: 20
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
    })
  )
);
