import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default function BtnWithIcon(props) {
  const {weight,isHShape,isEggless,checktype} = props;

  function changeShapeType()
  {
    props.setIsActive(!props.isActive)

    if(checktype===1)
    {
      props.priceFilter(weight,isHShape,!props.isActive);
    }else if(checktype===2){
      props.priceFilter(weight,!props.isActive,isEggless);
    }
    
  }

  return (
    <View>
      <View>
        <TouchableOpacity
          onPress={changeShapeType}
        >
          {props.isActive ? (
            <View style={[styles.menuItem, styles.menuItemActive]}>
            <MaterialCommunityIcons
              name={props.Micon}
              style={[styles.vctrICon, styles.vctrIConActive]}
              size={24}
            />
            <Text style={[styles.userText, styles.userTextActive]}>
              {props.Mtext}
            </Text>
          </View>
          ) : (
          <View style={styles.menuItem}>
          <MaterialCommunityIcons
            name={props.Micon}
            style={[styles.vctrICon]}
            size={24}
          />
          <Text style={styles.userText}>{props.Mtext} </Text>
          </View>)}
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  menuItem: {
    elevation: 8,
    backgroundColor: theme.white,
    borderColor: theme.mbHFColor,
    borderWidth: 1,
    width: 150,
    height: 40,
    padding: 10,
    borderRadius: 5,
    marginRight: 8,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15
  },
  vctrICon: {
    color: theme.green,
    marginRight: 10,
  },
  userText: {
    fontSize: theme.size14,
    fontFamily: 'MontserratSemiBold',
    alignSelf: 'center',
    justifyContent: 'center',
    color: theme.mbHFColor,
    textAlignVertical: 'center',
    color: theme.Hash666,
    // marginBottom: 4,
  },
  menuItemActive: {
    backgroundColor: theme.green,
  },
  vctrIConActive: {
    color: theme.white,
  },
  userTextActive: {
    color: theme.white,
  },
});
