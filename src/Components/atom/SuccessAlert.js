import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';
import { Entypo } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

const isWeb = Platform.OS === 'web';

export default function SuccessAlert(props) {
  const { Mtext, Micon,alertmsg } = props;
  

  return (
    <View>
      <View>
            <View style={styles.succItem}>
              <Ionicons name="md-checkmark-circle-outline" style={[styles.SuccICon]}
                size={24} />
              <Text style={styles.succText}>{alertmsg}</Text>
              <TouchableOpacity>
                <MaterialCommunityIcons name="close" style={[styles.closeICon]}
                size={24}/>
              </TouchableOpacity>
            </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  succItem: {
    elevation: 8,
    backgroundColor: theme.success,
    borderColor: theme.white,
    borderWidth: 0.5,
    width: 330,
    marginHorizontal: isWeb? 0:25,
    minHeight: 45,
    padding: 10,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    flex:1,
    justifyContent: isWeb? 'center':'space-around',
    alignItems: 'center',
  },
  SuccICon: {
    color: theme.white,
    marginRight: isWeb? 10:0,
    paddingHorizontal: isWeb? 0:12,
  },
  closeICon:{
    color: theme.white,
    paddingRight: isWeb? 0:10,
  },
  succText: {
    fontSize: theme.size14,
    fontFamily: 'MontserratRegular',
    alignSelf: 'center',
    justifyContent: 'center',
    color: theme.lightGreen,
    textAlignVertical: 'center',
    color: theme.white,
    paddingHorizontal: isWeb? 0:30,
    flexGrow: isWeb? 1:0
  },
});
