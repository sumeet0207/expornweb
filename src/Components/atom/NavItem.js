import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme } from '../../Theme/theme';

// const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const base = {
    navitem: {
      fontSize: props.titleSize || theme.size16,
      marginBottom: props.titleBotm || 6,
      color: props.navtxtColor || '#f77800',
      fontFamily: props.titleFont || 'MontserratSemiBold',
      textAlign: props.titleAlign || 'center',
      // textTransform: 'uppercase',
      // paddingHorizontal: 20,
      paddingRight: 50,
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        navitem: {
          fontSize: props.mb_titleSize || theme.size16,
        },
      })
    )
  );
  return pstyles;
};

const NavItem = (props) => {
  const pstyles = getStyle(props);

  return (
    <View>
      <TouchableOpacity onPress={props.onPress}>
        <Text style={pstyles.navitem}>{props.navtitle}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default NavItem;
