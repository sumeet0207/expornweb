import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import { useHistory } from '../../Route/routing';
import Checkbox from '../Uncommon/Checkbox';
import Quantity from '../atom/Quantity';
import {mix_delight_const} from '../constant';

const theme = {
  smallFont: 12,
  middleFont: 12,
  largeFont: 14,
  backColor: 'white',
  primaryGreen: '#0F8F00',
  primaryRed: '#FF1E1E',
};

const AccessoriesBox = (props) => {
  const history = useHistory();
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [quatCount, setQuanQuantity] = useState(0);
  const {checkid,accessBoxdata,removeFromAccessArray,
    pushAccess,selectAcc,setSelectedAcc} = props;
  let accessid =accessBoxdata.id;
  let acceessObj={
    accessory_id: accessid,
    accessory_name: accessBoxdata.name,
    quantity: quatCount
  }

  let finalSrc = mix_delight_const.noImgpath;
  finalSrc = accessBoxdata.image?`${mix_delight_const.imgpath}${accessBoxdata.image}`:finalSrc;

  function updateQuantity(acceid,quat)
  {
    for (let  i = 0; i < selectAcc.length; i++) {
      if (selectAcc[i].accessory_id === acceid) {
        selectAcc[i].quantity = quat;
        break;
      }
    }
    setSelectedAcc(selectAcc);
    // pushAccess(acceessObj);
    
  }

  function isCheck(e)
  {
    
    if(!toggleCheckBox==false)
    {
     
      setQuanQuantity(0)
      removeFromAccessArray(accessid)
      updateQuantity(accessid,0)
    }else
    {
      setQuanQuantity(quatCount + 1)
      pushAccess({
        accessory_id: accessid,
        accessory_name: accessBoxdata.name,
        quantity: 1
      })
      //pushAccess(acceessObj)
    }
    setToggleCheckBox(!toggleCheckBox)
  }

  function increaseCount()
  {
    console.log(quatCount)
    if(quatCount + 1 ==1)
    {
      console.log('jsjsj')
      setToggleCheckBox(true)
      pushAccess(acceessObj)
    }
    updateQuantity(accessid,quatCount + 1)
    setQuanQuantity(quatCount + 1)
  }

  function decreaseCount()
  {
    console.log(quatCount - 1)
    if((quatCount - 1) == 0)
    {
      setToggleCheckBox(false)
      removeFromAccessArray(accessid)
    }

    if(quatCount > 0)
    {
      setQuanQuantity(quatCount - 1)
    }
    updateQuantity(accessid,quatCount - 1)
  }


  
  let accSrc =
    'https://3.bp.blogspot.com/-CaXlbtXvaAo/VBBoghuKubI/AAAAAAAAAh4/9lWNaLpgzTc/s1600/candle.JPG';
  return (
    <View>
      <View style={styles.redeemedinfo}>
        <View style={styles.redeemedinfoChild}>
          <Image source={{ uri: finalSrc }} style={styles.imgstyle} />
          <View style={{ alignItems: 'center' }}>
            <Text style={styles.custName}>{accessBoxdata.name?accessBoxdata.name:''}</Text>
            <Text style={[styles.custName, styles.marTop, styles.fontcolor]}>
              ₹ {accessBoxdata.price?accessBoxdata.price:''}
            </Text>
          </View>

          <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center' }}>
            <View style={{ flex: 0.35 }}>
              <View style={styles.checkview}>
                <Checkbox val={checkid} toggleCheckBox={toggleCheckBox}
                 setToggleCheckBox={isCheck}
                  />
              </View>
            </View>
            <View style={{ flex: 0.65 }}>
              <Quantity decreaseCount={decreaseCount} 
              updateAccessoriesQuantity={setQuanQuantity}
               inCreaseCount={increaseCount} quatCount={quatCount}/>
            </View>
            {/* <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={styles.margRight6}>
                <AntDesign name="minussquareo" size={18} color="black" />
              </TouchableOpacity>
              <TouchableOpacity style={styles.margRight6}>
                <Text style={styles.amoutxt}>2</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.margRight6}>
                <AntDesign name="plussquareo" size={18} color="black" />
              </TouchableOpacity>
            </View> */}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  fontcolor: {
    color: 'red',
  },
  amoutxt: {
    fontSize: theme.largeFont,
  },
  checkview: {
    alignSelf: 'flex-start',
    // marginRight: 70,
    // marginLeft: 20,
  },
  margRight6: {
    marginRight: 6,
  },
  redeemedinfoChild: {
    padding: 15,
    paddingBottom: 10,
    borderColor: '#eee',
    borderStyle: 'solid',
    alignItems: 'center',
  },
  imgstyle: {
    height: 120,
    width: 120,
    marginBottom: 15,
  },

  redeemedinfo: {
    paddingBottom: 0,
    marginTop: 15,
    marginRight: 4,
    marginLeft: 4,
    marginBottom: 0,
    backgroundColor: 'white',
    elevation: 8,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    borderRadius: 3,
  },
  custName: {
    fontSize: theme.largeFont,
    fontWeight: '600',
    marginBottom: 5,
  },
  subtitle: {
    fontSize: theme.smallFont,
    fontWeight: '400',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  marTop: {
    marginTop: 6,
    marginBottom: 8,
  },
});

export default AccessoriesBox;
