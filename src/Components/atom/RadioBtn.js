import * as React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import RadioButton from './RadioButton';

const options = [
  {
    key: 'pay',
    text: 'Most High Pay',
  },
  {
    key: 'performance',
    text: 'Most Perfomance',
  },
  {
    key: 'aToZ',
    text: 'A - Z',
  },
  {
    key: 'zToA',
    text: 'Z - A',
  },
];

export default class RadioBtn extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <RadioButton options={options} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
