import React, { useState } from 'react';
import { View, Text, StyleSheet, Button, Platform } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Checkbox from '../Uncommon/Checkbox';

const isWeb = Platform.OS === 'web';

export default function CheckboxComp(props) {
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  function isCheck() {
    setToggleCheckBox(!toggleCheckBox);
  }

  return (
    <View style={{ flexDirection: 'row' }}>
      <View style={styles.container}>
        <Checkbox toggleCheckBox={toggleCheckBox} setToggleCheckBox={isCheck} />
        <Text>{props.checkText}</Text>
      </View>
    </View>
  );
}

const base = {
  container: {
    flexDirection: 'row',
    // backgroundColor: 'red',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(400, {
      container: {
        // backgroundColor: 'blue',
      },
    })
  )
);
