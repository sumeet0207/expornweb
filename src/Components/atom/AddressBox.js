import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Dimensions,
} from 'react-native';
const { width, height } = Dimensions.get('window');
import { useHistory } from '../../Route/routing';
import AddressComp from '../atom/AddressComp';
import { getApiurl } from "../../api/ApiKeys";
import { GetRequestFunction } from "../../api/ApiHelper";
import { getAsyncStorageFunction } from "../services/CookieModule";
import { useShow } from "../services/AlertHook";


const theme = {
  smallFont: 12,
  middleFont: 12,
  largeFont: 15,
  backColor: 'white',
  primaryGreen: '#0F8F00',
  primaryRed: '#FF1E1E',
};

const AddressBox = (props) => {
  const history = useHistory();
  const {title,address,contact,pincode,city,id}=props;
  const { show, error, success } = useShow();

  async function deleteShipping()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    if(mixdelight_token)
    {
        let header = { Token: mixdelight_token };
        let param = {address_id : id}
        let deleteAddressUrl = getApiurl("deleteShipping");
        let addressList = await GetRequestFunction(deleteAddressUrl,header, param);
        console.log(addressList)
        if(addressList.status)
        {
           let data = addressList.data;
           props.setAllAddress(data)
           success('Delete address successfully')
        }else
        {
           error(addressList.message)
        }
    }
  }

  return (
    <View>
      <View style={styles.redeemedinfo}>
        <View style={styles.redeemedinfoChild}>
          <View style={styles.mainbox}>
            <AddressComp
              id={id}
              title={title}
              address={address}
              city={city}
              pincode={pincode}
              number={contact}
              deleteShipping={deleteShipping}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainbox: {
    flex: 1,
    // paddingHorizontal: 10,
    paddingBottom: 10,
    borderRadius: 20,
  },
  marLeft: {
    marginLeft: 15,
  },
  redeemedinfoChild: {
    paddingHorizontal: 15,
    paddingBottom: 15,
    paddingTop: 15,
    borderColor: '#eee',
    borderStyle: 'solid',
  },

  redeemedinfo: {
    paddingBottom: 0,
    // marginTop: 15,
    marginRight: 4,
    marginLeft: 4,
    marginBottom: 15,
    // backgroundColor: 'white',
    // elevation: 8,
    // shadowOffset: { width: 1, height: 1 },
    // shadowOpacity: 0.5,
    // shadowRadius: 5,
    borderRadius: 3,
    borderColor: '#ccc',
    borderWidth: 1,
  },
  custName: {
    // fontSize: theme.largeFont,
    // fontWeight: '600',
    //alignSelf:"left",
    marginBottom: 5,
    fontFamily: 'MontserratSemiBold',
  },
  address: {
    fontSize: theme.middleFont,
    fontWeight: '400',
    //alignSelf:"left",
    marginBottom: 5,
    // marginTop: 5,
    fontFamily: 'MontserratRegular',
  },
  subtitle: {
    fontSize: theme.smallFont,
    fontWeight: '400',
    alignSelf: 'center',
    justifyContent: 'center',
    fontFamily: 'MontserratRegular',
  },
  cabinetButton: {
    width: 80,
    height: 40,
    borderRadius: 10,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: theme.primaryRed,
  },
  cabinetButtonText: {
    fontSize: 15,
    fontWeight: 'bold',
    justifyContent: 'center',
    color: theme.primaryRed,
    fontFamily: 'MontserratRegular',
  },
});

export default AddressBox;
