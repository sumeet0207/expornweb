import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme } from '../../Theme/theme';

// const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const base = {
    countClass: {
      fontSize: props.countSize || theme.size16,
      marginBottom: props.countBotm || 6,
      color: props.countColor || theme.darkbg,
      fontFamily: props.countFont || 'MontserratSemiBold',
      textAlign: props.countAlign || 'center',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        countClass: {
          fontSize: props.mb_countSize || theme.size16,
        },
      })
    )
  );
  return pstyles;
};

const CartCount = (props) => { 
  // const { count } = props;
  // const styles = getStyle(props);
  const pstyles = getStyle(props);

  return (
    <View>
      <Text style={pstyles.countClass}>({props.count})</Text>
    </View>
  );
};

export default CartCount;
