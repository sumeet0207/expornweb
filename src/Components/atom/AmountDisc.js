import React from 'react';
import { View, StyleSheet, Text, Dimensions } from 'react-native';
import { theme } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    redclass: {
      fontSize: props.amountSize || theme.size15,
      color: theme.darkgrey,
      alignSelf: 'center',
      alignItems: 'center',
      textDecorationLine: 'line-through',
      fontFamily: props.pnameFont || 'MontserratRegular',
    },
    greyclass: {
      fontSize: props.discSize || theme.size15,
      marginLeft: 10.667,
      color: theme.green,
      alignSelf: 'center',
      alignItems: 'center',
      fontFamily: props.pnameFont || 'MontserratRegular',
    },
    rGwrapper: {
      flexDirection: 'row',
      alignSelf: props.titleAlign || 'center',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        redclass: {
          fontSize: props.mb_amountSize || theme.size15,
        },
        greyclass: {
          fontSize: props.mb_discSize || theme.size15,
        },
      })
    )
  );
  return pstyles;
};

const AmountDisc = (props) => {
  const {} = props;
  const pstyles = getStyle(props);
  //'₹':'%'
  return (
    <View style={pstyles.rGwrapper}>
      <Text style={pstyles.redclass}>₹{props.amount}</Text>
       <Text style={pstyles.greyclass}>
         {props.distype==1?'₹':''}{props.disc}{props.distype==2?'%':''} OFF</Text>
    </View>
  );
};

export default AmountDisc;
