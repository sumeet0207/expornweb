import React from 'react';
import { View, StyleSheet, Image, Dimensions, TouchableOpacity } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    imgWrap: {
      height: props.ImgHeight || 20, 
      right: props.closeRight || 5,
      top: props.closeTop || 5,
      width: 20,
      position: 'absolute',
      cursor : 'pointer',
      zIndex:10,
    },
    imgstyle: {
      height: '100%',
      width: '100%',
      zIndex:1
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        imgWrap: {
          height: props.mb_ImgHeight || 20,
        },
      })
    )
  );
  return pstyles;
};

const CloseModal = (props) => {
  const { imgsrc,onPress } = props;
  const pstyles = getStyle(props);
  // let cakesrc =
  //   'https://images.immediate.co.uk/production/volatile/sites/2/2019/02/Banana-cake-e977d1b.jpg?quality=45&resize=620,413';
  return (
    <View>
      <TouchableOpacity style={pstyles.imgWrap} onPress={onPress}>
        
          <Image
            // source={{ uri: cakesrc }}
            source={require('../../assets/img/cross-mark.png')}
            resizeMode="contain"
            style={pstyles.imgstyle}
          />
        
      </TouchableOpacity>
    </View>
    
  );
};

export default CloseModal;
