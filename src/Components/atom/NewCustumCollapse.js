import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, LayoutAnimation, Platform, UIManager, TouchableOpacity } from 'react-native';
import { theme } from "../../Theme/theme";
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';


export default function NewCustumCollapse(props){
  const [expanded, setExpanded] = useState(false);
  const [shape, setShape] = useState('');
  const [eggtype, setEggType] = useState('');

  useEffect(() => {
    if(props.type==1)
    {
        checkShape();
        checkEggType();
    }
  },[]);

  function checkShape()
  {
    if(props.shape===1)
    {
        setShape('Round')
    }else if(props.shape===2)
    {
        setShape('Heart')
    }else
    {
        setShape('Round & Heart')
    }
  }

  function checkEggType()
  {
    if(props.eggtype===1)
    {
        setEggType('Egg')
    }else if(props.eggtype===2)
    {
        setEggType('Eggless')
    }else
    {
        setEggType('Egg & Eggless')
    }
  }
  
  if (Platform.OS === 'android') {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }

   function changeLayout(){
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setExpanded(!expanded)
    // this.setState({ expanded: !this.state.expanded });
  }

  
    return (
      <View style={styles.container}>
        <View style={styles.btnTextHolder}>
          <TouchableOpacity activeOpacity={0.8} onPress={changeLayout} style={styles.Btn}>
            <Text style={styles.btnText}>{props.heading}</Text>
          </TouchableOpacity>
          <View style={{ height: expanded ? null : 0, overflow: 'hidden' }}>
            {props.type!==1?
                <View>
                <Text style={styles.text}>{props.detail}</Text>
                </View>:
                <View>
                <Text style={styles.text}>{props.vendorname?`Vendor Name : ${props.vendorname}`:''}</Text>
                <Text style={styles.text}>{props.flavourname?`Flavour : ${props.flavourname}`:''}</Text>
                <Text style={styles.text}>Shape : {shape}</Text>
                <Text style={[styles.text,styles.marbtm]}>Type : {eggtype}</Text>
                </View>}
           </View>
        </View>
    </View>
    );

}

const base = {
// const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginBottom:10,
    paddingHorizontal: 0,
    justifyContent: 'center',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0
  },
  marbtm:{
      marginBottom:10
  },
  text: {
    color: theme.black,
    fontFamily: 'MontserratRegular',
    fontSize: 14,
    paddingHorizontal:10,
    paddingVertical:3,
  },
  text1: {
    color: theme.ogBlack,
    fontFamily: 'MontserratRegular',
    fontSize: 14,
    paddingHorizontal:10,
    paddingVertical:3,
    paddingBottom: 0,
  },

  btnText: {
    color: theme.ogBlack,
    fontFamily: 'MontserratRegular',
    fontSize: 18
  },

  btnTextHolder: {
    borderBottomWidth: 1,
    borderColor: theme.Hash666,
  },

  Btn: {
    paddingHorizontal: 0,
    paddingVertical:5
    // backgroundColor: 'white'
  }
};


const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(1024, {
      btnText: {
        fontSize: 15
      },
      text: {
        fontSize: 13,
        paddingHorizontal:0,
        paddingVertical:0.5,
      },
    })
  )
);