import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';
import { theme, setFontStyle } from '../../Theme/theme';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const styles = StyleSheet.create({
    stripeWrap: {
      backgroundColor: theme.white,
      borderWidth:1,
      borderColor: theme.grey2,
      flexDirection: 'row',
      alignItems: 'center',
      height: 40,
      borderRadius: 5,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    icon: {
      color: theme.Hash666,
      paddingLeft: 10,
      paddingRight: 10,
    },
    dlvtyTxt: {
      fontFamily: 'MontserratSemiBold',
      fontSize: theme.size14,
      color: theme.Hash666,
      alignSelf: 'center',
      paddingHorizontal: props.padding || 0,
    },
  });
  return styles;
};

const DateButton = (props) => {
  const styles = getStyle(props);
  var options = { year: 'numeric', month: 'long', day: 'numeric' };
  const { cart, padding,onClick,date } = props;

  
  return (
    <View>
      <TouchableOpacity onPress={onClick}>
        <View style={styles.stripeWrap}>
          <View style={{ flexDirection: 'row' }}>
            <MaterialCommunityIcons
              name="calendar-month"
              size={24}
              style={styles.icon}
            />
            <Text style={styles.dlvtyTxt}>{date.toLocaleDateString("en-US", options)}</Text>
          </View>
          <View>
            <FontAwesome name="angle-right" size={24} style={styles.icon} />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default DateButton;
