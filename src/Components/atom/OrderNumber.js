import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { theme } from '../../Theme/theme';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';

// const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    numclass: {
      fontSize: props.ordernoSize || theme.size15,
      color: props.txtColor || theme.darkbg,
      fontFamily: props.ordernoFont || 'MontserratSemiBold',
      textAlign: props.ordernoAlign || 'left',
    },
    ordr: {
      color: theme.darkbg,
      fontFamily: 'MontserratRegular',
      fontSize: theme.size15,
      paddingRight: 5,
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        numclass: {
          fontSize: props.mb_ordernoSize || theme.size13,
          marginBottom: 0,
        },
        ordr: {
          fontSize: theme.size13,
        },
      })
    )
  );
  return pstyles;
};

const OrderNumber = (props) => {
  // const { orderno } = props;
  const pstyles = getStyle(props);

  return (
    <View
      style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center' }}
    >
      <Text style={pstyles.ordr}>Ordre NO :</Text>
      <Text style={pstyles.numclass}>{props.orderno}</Text>
    </View>
  );
};

export default OrderNumber;
