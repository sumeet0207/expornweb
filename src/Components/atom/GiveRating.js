import React, { useState } from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const styles = StyleSheet.create({
    starWrap: {
      height: props.starHeight || 30,
    },
    starstyle: {
      height: '100%',
      width: '100%',
    },
  });
  return styles;
};

const GiveRating = (props) => {
  const [isFill, setIsFill] = useState(true);

  const styles = getStyle(props);

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          setIsFill(!isFill);
        }}
      >
        {isFill ? (
          <View style={styles.starWrap}>
            <Image
              source={require('../../assets/img/star.png')}
              resizeMode="contain"
              style={styles.starstyle}
            />
          </View>
        ) : (
          <View style={styles.starWrap}>
            <Image
              source={require('../../assets/img/fillStar.png')}
              resizeMode="contain"
              style={styles.starstyle}
            />
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

export default GiveRating;
