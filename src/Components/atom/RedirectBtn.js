import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Button,
} from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme, setFontStyle } from '../../Theme/theme';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const base = {
    cabinetButton: {
      elevation: 8,
      height: props.sbtnHeight || 40,
      width: props.sbtnWidth || '100%',
      alignSelf: 'center',
      justifyContent: 'center',
      borderTopLeftRadius: props.topleftradius || 5,
      borderTopRightRadius: props.toprightradius || 5,
      borderBottomLeftRadius: props.botmleftradius || 5,
      borderBottomRightRadius: props.botmrightradius || 5,
      backgroundColor: props.Colorbg || theme.HFColorText,
    },
    cabinetButtonText: {
      fontSize: props.sbtntxtSiz || theme.size14,
      fontFamily: props.titleFont || 'MontserratSemiBold',
      alignSelf: 'center',
      justifyContent: 'center',
      color: props.subBtncolor || theme.white,
      textAlignVertical: 'center',
      textTransform: props.fontTrans || 'uppercase',
      paddingHorizontal: 15,
    }
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        cabinetButton: {
          height: props.sbtnHeight || 30,
        },
        cabinetButtonText: {
          fontSize: props.sbtntxtSiz || theme.size11,
        }
      })
    )
  );
  return pstyles;
};

const RedirectBtn = (props) => {
  const pstyles = getStyle(props);

  return (
    <View>
      <TouchableOpacity
        style={pstyles.cabinetButton}
        onPress={props.onPress}
      >
        <Text style={pstyles.cabinetButtonText}>
          {props.rbtnTxt}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default RedirectBtn;
