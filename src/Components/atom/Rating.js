import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme } from '../../Theme/theme';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  const styles = StyleSheet.create({
    rateTxt: {
      color: theme.white,
      fontSize: theme.size14,
      alignSelf: 'center',
      fontFamily: 'MontserratRegular',
    },
    rateIcon: {
      color: theme.white,
      paddingLeft: 0,
      paddingRight: 4,
      alignSelf: 'center',
    },
    rateWrap: {
      position: props.rPos || 'absolute',
      top: props.rTop || 9,
      left: props.rLeft || null,
      right: props.rRight || 10,
      width: 55,
      height: 27,
      backgroundColor: theme.orange,
      zIndex: 1,
      alignItems: 'flex-start',
      justifyContent: 'center',
      flexDirection: 'row',
    },
  });
  return styles;
};

const Rating = (props) => {
  const { redText, greyText } = props;
  const styles = getStyle(props);

  return (
    <View style={styles.rateWrap}>
      <Icon name="star" style={[styles.icon, styles.rateIcon]} size={15} />
      <Text style={styles.rateTxt}>{props.rate}</Text>
    </View>
  );
};

export default Rating;
