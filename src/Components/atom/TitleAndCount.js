import React, { useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { theme } from '../../Theme/theme';
import { AntDesign } from '@expo/vector-icons';

const TitleAndCount = (props) => {

  const[isActive, setIsActive] = useState(true);
  const{isCheckVisible = false, isLeft=true,filtername='',checkarray=[]} = props;

  return (
    <View>
      <View style={isLeft && filtername!=props.title?styles.backColor:null}>
        <TouchableOpacity passid={props.passid} 
          onPress={isLeft?()=>props.handleClick(props.title):()=>props.handleClick(props.passid)}>
          {checkarray.includes(props.passid) ? (
            <View style={styles.contentWrap}>
              
              {isCheckVisible && (
              <AntDesign name="check" size={12} color={theme.orange} />
              )}
              <Text style={[styles.textContent,styles.activeText]}>{props.title}</Text> 
              <Text style={[styles.textContent, styles.activeText]}>{props.count}</Text> 
          </View>
          ) : (
            <View style={styles.contentWrap}>
              {isCheckVisible && (
              <AntDesign name="check" size={12} color={theme.Hash666} />
              )}
              <Text style={[styles.textContent,styles.inactiveText]}>{props.title}</Text> 
              <Text style={[styles.textCount, styles.inactiveText]}>{props.count}</Text> 
            </View>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  contentWrap:{
    flexDirection:'row',
    justifyContent:'space-between',
    borderBottomWidth: 0.5,
    borderColor: '#ccc',
    alignItems:'center',
    paddingHorizontal: 10
  },
  textContent:{
    paddingVertical: 10,
    fontSize: theme.size12,
    color: theme.ogBlack,  
    // wordBreak: 'break-word',
  },
  textCount:{
    fontSize: theme.size9,
    color: theme.Hash666, 
    textAlign: 'right',
  },
  activeText:{
    fontFamily: 'MontserratSemiBold',
  },
  inactiveText:{
    fontFamily: 'MontserratRegular',
  },
  backColor:{
    backgroundColor: theme.lightergrey,
  }
});

export default TitleAndCount
