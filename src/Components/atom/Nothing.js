import React from 'react';
import {View, Text, StyleSheet, Button, Platform,Image,TouchableOpacity} from 'react-native';
import { useHistory } from "../../Route/routing";

export default function Nothing(props) {
    const history = useHistory();
    const {passurl,heading,btnName} = props;
    return(<View style={styles.container}>
            <Image
             //source={require('../../assets/img/notfound.jpg')}
            //source={require(props.imgsrc)}
             source={{uri: props.imgsrc}} 
             style={{width:400,height:400}}/>
            <Text style={{marginBottom:15,fontWeight:'bold',fontSize:18}}>{heading}</Text>
            <TouchableOpacity style={[styles.cabinetButton]} onPress={()=>history.push(passurl)}>
    <Text style={[styles.cabinetButtonText]}>{props.btnName}</Text>
            </TouchableOpacity>
          </View>)
 }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  cabinetButton:{
    width:200,
    height:40,
    borderRadius:10,
    paddingVertical:15,
    justifyContent:'center',
    alignItems:'center',
    borderWidth:1,
    borderColor:'black'
    
 },
  cabinetButtonText: {
  fontSize: 15,
  fontWeight:"bold",
  justifyContent:"center",
  color:'black'
 },
  

});