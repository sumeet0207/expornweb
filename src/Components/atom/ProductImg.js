import React from 'react';
import { View, StyleSheet, Image, Dimensions } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { mix_delight_const } from '../constant';

const { width, height } = Dimensions.get('window');

const getStyle = (props) => {
  // const base = StyleSheet.create({
  const base = {
    imgWrap: {
      height: props.ImgHeight || 200, 
    },
    imgstyle: {
      height: '100%',
      width: '100%',
    },
  };

  const pstyles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(991, {
        imgWrap: {
          height: props.mb_ImgHeight || 200,
        },
      })
    )
  );
  return pstyles;
};

const ProductImg = (props) => {
  const { imgsrc } = props;
  const pstyles = getStyle(props);
  let finalImg = mix_delight_const.noImgpath;
  //let finalImg = 'https://helenbonnick.files.wordpress.com/2017/06/img_5579.jpg'

  finalImg = imgsrc?`${mix_delight_const.imgpath}${imgsrc}`:finalImg;
  //console.log(finalImg)
  return (
    <View style={pstyles.imgWrap}>
      <Image
        source={{ uri: finalImg }}
        resizeMode="cover"
        style={pstyles.imgstyle}
      />
    </View>
  );
};

export default ProductImg;
