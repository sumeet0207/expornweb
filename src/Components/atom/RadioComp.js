import React, { useState, useEffect } from "react";
import { View, Platform } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import Radiobutton from '../Uncommon/RadioButton';
const isWeb = Platform.OS === 'web';

export default function RadioComp(props) {
  const [toggleRadio, setToggleRadio] = useState('');
  const [radioProps, setRadioProps] = useState([]);
  let weightPrice = props.weightPrice;
  var abc=[];

  useEffect(() => {
    createProps();
  },[]);

  function createProps()
  {
    //setToggleRadio(weightPrice[0].weight.toString());
    props.setSelectedWeight(weightPrice[0].weight.toString());
    let finalProps=[];
    weightPrice.forEach(wp => {     
      let obj = { label : `${wp.weight.toString()} Kg     `, value:wp.weight.toString()}
      finalProps.push(obj)
    })
    setRadioProps(finalProps);
  }

  function getRadioval(val) {
    
    //setToggleRadio(val);
    props.setSelectedWeight(val);
    props.priceFilterFunction(val);
  }

  return (
    <View style={styles.container}>
      <Radiobutton
        types={radioProps}
        val={props.selectedWeight}
        onPress={getRadioval}
        row={true}
      />
    </View>
  );
}

const base = {
  container: {
    // flex: 1,
    flexDirection: 'row',
    display: 'flex',
    // flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: 'red',
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(400, {
      container: {
        // backgroundColor: 'blue',
      },
    })
  )
);
