import React, {useState, useEffect} from 'react';
import { View, Text, TouchableOpacity, Modal, Platform, Dimensions } from 'react-native';
import { theme } from '../../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import CloseModal from '../atom/CloseModal';
// import FilterAndSortMolecule from '../molecule/FilterAndSortMolecule';
import WebModal from 'react-modal';
import FilterAndSortMolecule from '../FilterAndSortMolecule';
import CloseApplyButton from '../atom/CloseApplyButton';
import SortingMolecule from '../molecule/SortingMolecule';
import { getApiurl } from "../../api/ApiKeys";
import { PostRequestFunction,GetRequestFunction } from "../../api/ApiHelper";

const isWeb = Platform.OS === 'web';

// isWeb?WebModal.setAppElement('#root'):null;
const { width, height } = Dimensions.get('window');


const customStyles = {
    content : {
        position: isWeb? 'none':'absolute',
        padding: 0,
        borderWidth:0
        // marginTop: height-280
    }
}

function FilterSortButton(props) {

    const [filterModal, openFilterModal] = useState(false);
    const [sortModal, openSortModal] = useState(false);
    const [flavours, setFlavour] = useState([]);
    const [shape, setShapes] = useState([]);
    const [weights, setWeight] = useState([]);
    const [vendors, setVendor] = useState([]);
    const [types, setTypes] = useState([]);
    const [whichfilter, setWhichFilter] = useState('');

    useEffect(() => {
        getAllFilterData();
       
      },[]);
    

    async function getAllFilterData()
    {
        let getfilterUrl = getApiurl("getFilterData");
        let getFilterBaseList = await GetRequestFunction(getfilterUrl, {}, {});
        console.log(getFilterBaseList)
        if(getFilterBaseList.status)
        {
          let data = getFilterBaseList.data[0];
          console.log(data)
          setFlavour(data.flavours);
          setShapes(data.shape);
          setVendor(data.vendors);
          setWeight(data.weights);
          setTypes(data.type);
        }

    }

    function closeSort()
    {
        openSortModal(false);
    }


    return (
        <View>
            {!filterModal?
                <View style={{ flexDirection: 'row'}}>
                <TouchableOpacity style={[styles.btnArea, styles.sortStyle]}
                onPress={() => openSortModal(true)}
                >
                    <MaterialCommunityIcons name="sort" size={24} color="black" />
                    <View style={styles.btnText}><Text>Sort</Text></View>
                </TouchableOpacity>
                <TouchableOpacity style={[styles.btnArea, styles.filterStyle]}
                    onPress={() => openFilterModal(true)}
                >
                    <MaterialCommunityIcons name="filter-outline" size={24} color="black" />
                    <View style={styles.btnText}><Text>Filter</Text></View>
                </TouchableOpacity>
            </View>:null}

            {/* Sort Modal Start */}
            {isWeb ? <WebModal isOpen={sortModal} style={customStyles}>                   
                <View>
                    <SortingMolecule closemodal={closeSort} getSortingData={props.getSortingData} />
                    <View style={{position:'absolute', right:0}}>
                    <CloseModal closeTop={14} closeRight={12} onPress={closeSort}/>
                    </View>
                    
                </View>
            </WebModal> : <Modal visible={sortModal} animationType="slide" style={customStyles}>
                    <View>
                        {/* <CloseModal closeTop={14} closeRight={12} onPress={()=>openSortModal(false)}/> */}
                        <SortingMolecule closemodal={closeSort} getSortingData={props.getSortingData}/>
                        <View style={{position:'absolute', right:0}}>
                            <CloseModal closeTop={14} closeRight={12} onPress={closeSort}/>
                        </View>
                    </View>
                </Modal>}
            {/* Sort Modal End */}

            {/* Filter MOdal Start */}
            {isWeb ? <WebModal isOpen={filterModal} style={base}>                   
                {/* <View style={{height: height-50}}> */}
                    <FilterAndSortMolecule 
                     flavours={flavours} shape={shape} 
                     vendors={vendors} weights={weights}
                     types={types}
                     onPress={() => openFilterModal(false)}
                     getfilterdata={props.getfilterdata}
                    />
                {/* </View> */}
                {/* <CloseApplyButton onPress={() => openFilterModal(false)} /> */}
            </WebModal> : <Modal visible={filterModal} animationType="slide" style={base}>
                    <View>
                        <View style={{  height:height }}>
                            {/* <View style={{height: height-72}}> */}
                                <FilterAndSortMolecule flavours={flavours} shape={shape} 
                                vendors={vendors} weights={weights}
                                types={types}
                                onPress={() => openFilterModal(false)}
                                getfilterdata={props.getfilterdata}/>
                            {/* </View> */}
                            {/* <CloseApplyButton onPress={() => openFilterModal(false)} /> */}
                        </View>
                    </View>
                </Modal>}
            {/* Filter MOdal End */}


        </View>
    );
}

const base = {
    content : {
        position: isWeb? 'none':'absolute',
        padding: 0,
        // height: height
        // margin: 'auto',
        // transform: 'translate(-50%, -50%)',
        // top:'50%',
        // left:'50%',
        // paddingTop: 20 
      },

    btnArea : {
        flex:0.5,
        backgroundColor:theme.white,
        height: 55,
        flexDirection:'row',
        alignItems: 'center',
        justifyContent:'center'
    },
    sortStyle:{
        borderWidth:0.4,
        borderRightWidth:0,
        borderLeftWidth:0,
        borderColor:'#ccc',
    },
    filterStyle:{
        borderWidth:0.4,
        borderRightWidth:0,
        borderColor:'#ccc',
    },
    btnText :{
        padding: 15,
        textAlign :' center',
        fontSize: theme.size16,
        color: theme.darkbg,
        fontFamily: 'MontserratRegular'
    },
  };
  
  const styles = createStyles(
    base,
    minWidth(
      100,
      maxWidth(1024, {
        main :{
            position:'sticky',
            bottom: 0
        }
        

      })
    )
  );
  

export default FilterSortButton
