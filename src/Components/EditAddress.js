import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform,
  BackHandler
} from "react-native";
import { useForm, Controller } from 'react-hook-form';
import { GetRequestFunction,PostRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import {getAsyncStorageFunction} from "./services/CookieModule";
import { CheckValidPincode } from "./services/CommonModule";
import { Link,useParams,useHistory } from '../Route/routing';
import { useShow } from "./services/AlertHook";
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { SafeAreaView } from "react-navigation";

const isWeb = Platform.OS === 'web';
const { height, width } = Dimensions.get('window');

const theme = {
   smallFont:10,
   largeFont:12,
   checkboxHW:13,
   backColor:'white',
   primaryFontColor:"white",
   primaryGrey: '#A5A7AB',
   primaryBlack:"black",
   primaryRed:"#FF1E1E",
}

export default function EditAddress(){
        const [pinError, setPincodeError] = useState('Currently we only deliver in Thane area better to check pincode');
        let { addressid } = useParams();
        let history = useHistory();
        const [datas,setDatas] = useState({});
        const { handleSubmit, control, errors,reset } = useForm({ });
        const { show, error, success } = useShow();

        useEffect(() => {
            getAllAdddress();
            handleBackHnadler();
        },[]);

        useEffect(() => {
            reset(datas)
        },[datas]);

        function handleBackHnadler()
        {
            const backAction = () => {
            history.goBack();
            return true;
            };
    
            const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            backAction
            );
            return () => backHandler.remove();
        }
       
        async function getAllAdddress()
        {
            let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
            console.log(mixdelight_token)
            if(mixdelight_token)
            {
              let header = { Token: mixdelight_token };
              let param = {address_id : addressid}
              let getAddressUrl = getApiurl("getShipping");
              let addressList = await GetRequestFunction(getAddressUrl,header, param);
             
              if(addressList.status && addressList.count > 0)
              {
                let data = addressList.data;
                //console.log(data)
                setDatas(data[0])
              }else
              {
               
                history.push('/addresses');
              }
            }else
            {
              history.push('/signup');
            }
        }

        const onSubmit = async(data) => 
        {
            let pincodeval = parseInt(data['pincode']);
            let checkResp = await CheckValidPincode(pincodeval);
            if(!checkResp)
            {
               error('We dont deliver on this pincode SORRY...!!')
               setPincodeError('We dont deliver on this pincode SORRY...!!')
               return false;
            }

            data['number'] = parseInt(data['number'])
            data['pincode'] = pincodeval;
            data['address_id'] = addressid;
            let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
            if(mixdelight_token)
            {
                let header = { Token: mixdelight_token };
                let param = data;
                let addAddressUrl = getApiurl("updateShipping");
                let addressList = await PostRequestFunction(addAddressUrl,header,param);
                if(addressList.status)
                {
                    success('Address Update successfully')
                    history.goBack();
                    //history.push('/addresses')
                }else{
                    error(addressList.message)
                    console.log("no data")
                }

            }else
            {
                history.push('/signup');
            }
            
            
        };
        return (

    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
            
            <ScrollView style={styles.SafeAreaViewstyle}>
                
                <View style={styles.formView}>
                    <View>
                        <TouchableOpacity
                            style={styles.otpButtonContainer}
                            onPress={null}
                        >
                        <Text style={styles.HeadingTxt}>
                            Edit Your Name and Address
                        </Text>
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.formViewBox,styles.borderWidthStyle]}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                        <TextInput style={[styles.input,styles.borderWidthStyle]}
                        onBlur={onBlur}
                        placeholder="Full Name"
                            onChangeText={(value) => onChange(value)} value={value}/>
                        )}
                        name="name"
                        //defaultValue={fullname}
                        rules={{ required: true }}
                        />
                        {errors.name && <Text style={styles.errorText}>This is required.</Text>}
                       <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                            placeholder="Mobile Number"
                                onChangeText={(value) => onChange(value)} value={value}/>
                            )}
                            name="number"
                            //defaultValue={fullname}
                            rules={{ required: "This is required.", pattern:[{pattern:/^\d+$/,message:'Must be 10 digit long'}], 
                            validate: {
                                    length: (value) => value && value.length === 10 || 'Must be 10 digit long.', 
                                    digits: (value) => value && /^[-+]?\d*$/.test(value) || 'Expected all integer.',
                                }
                            }  }
                        /> 
                        {errors.number && <Text style={styles.errorText}>{errors.number.message}</Text>}
                         <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                            placeholder="Pincode"
                                onChangeText={(value) => onChange(value)} value={value}/>
                            )}
                            name="pincode"
                            rules={{ required: "This is required.", pattern:[{pattern:/^\d+$/,message:'Must be 6 digit long'}], 
                            validate: {
                                    length: (value) => value && value.length === 6 || 'Must be 6 digit long.', 
                                    digits: (value) => value && /^[-+]?\d*$/.test(value) || 'Expected all integer.',
                                }
                            }  }
                        /> 
                        {errors.pincode && <Text style={styles.errorText}>{errors.pincode.message}</Text>}
                         <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                                placeholder="Address" 
                                onChangeText={(value) => onChange(value)} value={value}/>
                                )}
                            name="address"
                            rules={{ required: true }}
                        /> 
                        {errors.address && <Text style={styles.errorText}>This is required.</Text>}
                        
                        <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                                placeholder="Town/City" 
                                onChangeText={(value) => onChange(value)} value={value}/>
                                )}
                            name="city"
                            rules={{ required: true }}
                        />  
                        {errors.city && <Text style={styles.errorText}>This is required.</Text>}

                        <Text style={styles.instrHeading}>Add delivery instructions</Text>
                        <Text style={styles.instrDesc}>{pinError}</Text>
                    </View>
                    <TouchableOpacity style={[styles.cabinetButton,styles.cabinetBack]}
                        onPress={handleSubmit(onSubmit)}>
                        <Text style={[styles.cabinetButtonText,styles.callnowtext]}>
                          DELIVER TO THIS ADDRESS
                        </Text>
                    </TouchableOpacity>
                </View>
                
            </ScrollView>
            </View>
            <Footer />
            <MobileFooter />
            </View> 
               
        );
   
}

const styles = StyleSheet.create({

    pageArea:{
        flexGrow:1
    },
    
    viewWidth:{
        width:300
    },
    formView:{
      paddingLeft:15,
      paddingRight:15,
      marginBottom:45
    },
    borderWidthStyle:{
        borderWidth:1,
        borderStyle:'solid',
        borderColor:theme.primaryGrey
    },
    formViewBox:{
        padding:12,
        marginBottom:20,
    },
    SafeAreaViewstyle:{
        padding:10,
        marginTop:20,
        backgroundColor : theme.backColor,
    },
    input: {
        borderRadius: 4,
        height:40,
        marginBottom:10,
        paddingLeft:10
      },
    pickerBox: {
        paddingTop:0,
        borderRadius: 4,
        height:50,
        marginBottom:10,
    },      
    otpButtonContainer: {
        backgroundColor: theme.primaryBlack,
        height:45,
        borderTopLeftRadius:6,
        borderTopRightRadius:6,
        alignItems:'center',
        justifyContent:'center',
        paddingBottom:isWeb?0:20
        

    },
    HeadingTxt:{
        marginLeft:15,
        top:12,
        fontSize:14,
        color:theme.backColor,
        fontWeight: "bold"
    },
    instrHeading:{
        marginTop:5,
        fontWeight:"bold",
        color:theme.primaryBlack,
        fontSize:15
    },
    instrDesc:{
        fontSize:15,
        color:'red',

    },
    cabinetButton:{
        height:45,
        width:"100%",
        paddingVertical: 12,
        paddingLeft:15
     },
      cabinetButtonText: {
      fontSize: 15,
      fontWeight:"bold",
      justifyContent:"center",
      color:"white"
    },
    cabinetBack:{
        backgroundColor: theme.primaryRed,
      },
    errorText:{
        color:theme.primaryRed,
        marginBottom:5
    }  
  

});