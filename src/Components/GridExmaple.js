import React from 'react';
import { StyleSheet, Text, View, Modal } from 'react-native';
//import { Row, Column as Col, Grid} from 'react-native-responsive-grid'
import { Row, Col } from 'react-native-responsive-grid-system';
import { theme } from '../Theme/theme';

export default function GridExample() {
  return (
    <View>
      <Row>
        <Col xs={12} sm={12} md={4} lg={3}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: theme.maincolor,
            }}
          >
            <Text>Sumeet</Text>
          </View>
        </Col>
        <Col xs={12} sm={12} md={4} lg={3}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text>Sumeet</Text>
          </View>
        </Col>
        <Col xs={12} sm={12} md={4} lg={3}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text>Sumeet</Text>
          </View>
        </Col>
        <Col xs={12} sm={12} md={4} lg={3}>
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text>Sumeet</Text>
          </View>
        </Col>
      </Row>
    </View>
  );
}

const styles = StyleSheet.create({});
