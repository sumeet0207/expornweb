import React, { useState, useEffect } from "react";
import { Text, View, ScrollView, TouchableOpacity,FlatList,Dimensions, 
  SafeAreaView,BackHandler, Platform } from 'react-native';
import AddressBox from './atom/AddressBox';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { GetRequestFunction } from '../api/ApiHelper';
import { getApiurl } from "../api/ApiKeys";
import {getAsyncStorageFunction} from "./services/CookieModule";
import { Link,useParams,useHistory } from '../Route/routing';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const isWeb = Platform.OS === 'web';

const theme = {
  smallFont: 12,
  middleFont: 12,
  largeFont: 15,
  backColor: 'white',
  primaryRed: '#FF1E1E',
};

export default function SavedAddress(props) {
  const history = useHistory();
  const [allAddress, setAllAddress] = useState([]);
  const width = Dimensions.get('window').width;
  

  useEffect(() => {
    getAllAdddress();
    handleBackHnadler();
  },[]);

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }

  async function getAllAdddress()
  {
      let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
      if(mixdelight_token)
      {
        let header = { Token: mixdelight_token };
        let getAddressUrl = getApiurl("getShipping");
        let addressList = await GetRequestFunction(getAddressUrl,header, {});
        if(addressList.status)
        {
          let data = addressList.data;
          setAllAddress(data);
         
        }
      }else
      {
        history.push('/');
      }
  }

  function addAddress() {
    history.push('/addaddress');
  }

 
  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
        <View>
        {allAddress.length > 0?<View style={styles.myaccountBox}>
          <FlatList
            data={allAddress?allAddress:[]}
            scrollEnabled={false}
            initialNumToRender={10}
            numColumns={1}
            keyExtractor={item => item.id}
            numColumns={width>768?4:1}
            renderItem={({ item, index, separators }) => (
              <AddressBox
                id={item.id}
                setAllAddress={setAllAddress}
                title={item.name?item.name:''}
                contact={item.number?item.number:''}
                address={item.address?item.address:''}
                city={item.city?item.city:''}
                pincode={item.pincode?item.pincode:''}
              />
            )}
          /> 
          {/* <Row> */}
            {/* <Col xs={12} sm={12} md={4} lg={4}>
              <AddressBox
                title="Sumeet chavan"
                address="Shayadri rahivasi sangh, lokmanyanagar Thane west"
                pincode="400606"
              />
            </Col> */}
            {/* <Col xs={12} sm={12} md={4} lg={4}>
              <AddressBox
                title="Pravin sathe"
                address="Shayadri rahivasi sangh, lokmanyanagar Thane west"
                pincode="400606"
              />
            </Col>
            <Col xs={12} sm={12} md={4} lg={4}>
              <AddressBox
                title="Shreeraj sawant"
                address="Shayadri rahivasi sangh, lokmanyanagar Thane west"
                pincode="400606"
              />
            </Col>
            <Col xs={12} sm={12} md={4} lg={4}>
              <AddressBox
                title="Shreeraj sawant"
                address="Shayadri rahivasi sangh, lokmanyanagar Thane west"
                pincode="400606"
              />
            </Col> */}
          {/* </Row> */}
        </View>:null}
        <View style={styles.addaddress}>
          <TouchableOpacity
            style={[styles.cabinetButton, styles.cabinetBack]}
            onPress={addAddress}
          >
            <Text style={[styles.cabinetButtonText, styles.callnowtext]}>
              ADD ADDRESS
            </Text>
          </TouchableOpacity>
        </View>
        </View>
        </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  pageArea:{
    flexGrow:1,
    marginVertical: isWeb? 0:70,
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  addaddress: {
    
    marginLeft: 50,
    marginRight: 50,
    marginBottom: 50,
    alignItems: 'center',
  },
  myaccountBox: {
    marginTop:25,
    padding: 15,
    marginLeft: 50,
    marginRight: 50,
    borderWidth: 1,
    borderColor: '#cacaca',
    borderRadius: 8,
  },
  cabinetButton: {
    width: '30%',
    marginTop: 20,
    height: 50,
    paddingLeft: 15,
    paddingVertical: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cabinetButtonText: {
    fontSize: 15,
    fontWeight: 'bold',
    justifyContent: 'center',
    color: 'white',
  },
  cabinetBack: {
    backgroundColor: theme.primaryRed,
  },
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(1024, {
      myaccountBox: {
        marginLeft: 0,
        marginRight: 0,
        borderWidth: 0,
      },
      addaddress: {
        marginLeft: 0,
        marginRight: 0,
        padding: 15,
      },
      cabinetButton: {
        width: '100%',
        marginTop: 0,
      },
    })
  )
);
