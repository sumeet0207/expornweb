import React from 'react';
import { StyleSheet, Text, View, ScrollView, Dimensions, Image, Platform } from 'react-native';
import { Link, useLocation } from '../Route/routing';

import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import OrderMolecule from './molecule/OrderMolecule';
import TitleText from './atom/TitleText';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function AboutUS() {
  let location = useLocation();
  console.log(location.pathname);
  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View style={{ padding: 20, flexGrow:1, backgroundColor:theme.white }}>
              <View style={{ marginVertical:10 }}>
              <TitleText
                title="About Us"
                titleSize={theme.size30}
                mb_titleSize={theme.size20}
                titleFont={'MontserratSemiBold'}
                txtColor={theme.ogBlack}
              />
              </View>
              <View>
                <Text style={styles.termsTxt}>Mix delight is online website and app. To deliver the cake, pastry and many more sweets at your doorstep. To launching this app or website there is only one thought to offer the customer many more varieties of sweet under one roof and it will easily serve at your door.  In India every occasion or every celebration, start or end with the sweet so there is no need to go anywhere with the help of this app you can order any sweet easily at your home. We launch this website by understanding customer needs and with modern choice and techniques to satisfy with the right product. our main objective is to offer customer quality product with best value so that the customers can appreciate.</Text>
                <br/>
                <Text style={styles.termsTxt}>We source the products in-house and have partnered with various leading providers to bring you the best products at the best prices.</Text>
                <br/>
                <Text style={styles.termsTxt}>We will strive for:</Text>
              </View>
              <View style={styles.termsTxt}> 
                <ul>
                  <li style={styles.liBtm}>Going the Extra Mile for customer satisfaction.</li>
                  <li style={styles.liBtm}>Superior Quality of products.</li>
                  <li style={styles.liBtm}>Your profile is your identity on Mix Delight, so keep it real. </li>
                  <li style={styles.liBtm}>Creative Package Designs.</li>
                  <li style={styles.liBtm}>Value & Affordability</li>
                </ul>
              </View> 

              <View>
                <Text style={styles.termsTxt}>We also have a wide range of Pastries, Candles Decoration goods and gift hampers that is designed to suit your needs and keep your connections stronger. If preferred, you may call us to place orders in Mumbai, directly at +91-9892092484 or visit our website mixdelight.com and mobile application Mix Delight.</Text>
                <br/>
                <Text style={styles.termsTxt}>Our caring personnel are ready to assist you with placing orders for all local deliveries and any other queries. Not sure what to send? We’ll gladly help you with creative suggestions! For your convenience, we offer daily floral and delivery, to the destination of your choice, anywhere in Thane.</Text>
                <br/>
                <Text style={styles.termsTxt}>In our online shop, you will discover endless normal cakes adjusted to any sustenance motivations, narrow mindedness, and vegetarian. You can pick inventive and regular cakes to make recollections in the occasions with loved ones.</Text>
                <br/>
                <Text style={styles.termsTxt}>We examined and made numerous conveyance choices to be specific Standard/FREE, Express, On Time, Mid Night and Early Morning to enable individuals to benefit our administration whenever for the duration of the day.</Text>
                <br/>
                <Text style={styles.termsTxt}>We are presently working in Thane and going to grow to real urban areas like Mulund, Kalyan, Mumbai and Navi Mumbai soon. </Text>
              </View>

              {/* <View style={{marginVertical:20}}>
              <Row >
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                  <View style={{ marginVertical:10 }}>
                    <TitleText
                      title="Founder"
                      titleSize={theme.size30}
                      mb_titleSize={theme.size20}
                      titleFont={'MontserratSemiBold'}
                      txtColor={theme.orange}
                      titleAlign={'center'}
                    />
                    <Text style={styles.termsTxt}>Mix delight started by Mr. Pravin Sathe and his family to server cakes, sweets and many more easily delivered at your doorstep and give basic platform to homemade bakers in modern urbanization. Firstly, they start their business by social media publicity form June and then they launch the website and app oct 2020.</Text>
                  </View>
                </Col>
                <Col xs={12} sm={12} md={6} lg={6} xl={6}>
                  <View style={{height:225}}>
                    <Image
                      source={require('../assets/img/pic.jpeg')}
                      resizeMode="contain"
                      style={{width:'100%', height:'100%'}}
                    />
                  </View>
                </Col>
              </Row>
              </View> */}

              <View style={{ marginVertical:10 }}>
              <TitleText
                title="Founder"
                titleSize={theme.size30}
                mb_titleSize={theme.size20}
                titleFont={'MontserratSemiBold'}
                txtColor={theme.orange}
                titleAlign={'center'}
              />
              <Text style={styles.termsTxt}>Mix delight started by Mr. Pravin Sathe and his family to server cakes, sweets and many more easily delivered at your doorstep and give basic platform to homemade bakers in modern urbanization. Firstly, they start their business by social media publicity form June and then they launch the website and app oct 2020.</Text>
              </View>

            
              {/* <View style={{ marginVertical:10 }}>
              <TitleText
                title="OUR VISION"
                titleSize={theme.size30}
                mb_titleSize={theme.size20}
                titleFont={'MontserratSemiBold'}
                txtColor={theme.orange}
                titleAlign={'center'}
              />
              <Text style={styles.termsTxt}>Mix delight started by Mr. Pravin Sathe and his family to server cakes, sweets and many more easily delivered at your doorstep and give basic platform to homemade bakers in modern urbanization. Firstly, they start their business by social media publicity form June and then they launch the website and app oct 2020.</Text>
              </View> */}

              <View style={{ marginVertical:10 }}>
              <TitleText
                title="OUR MISSION"
                titleSize={theme.size30}
                mb_titleSize={theme.size20}
                titleFont={'MontserratSemiBold'}
                txtColor={theme.orange}
                titleAlign={'center'}
              />
              <Text style={styles.termsTxt}>Provide professional and courteous service to our customers with highest standard of freshness, quality and product value. Constantly focus and evolve through innovation and cutting-edge technology for superior product line. Establish beneficial relationship with our customers, supplier and franchised operations. Provide an exceptional and memorable experience for our customers through our graciousness, professionalism, integrity, service and products. Contribute to the society through our profits.</Text>
              </View>

             
              
            
          </View>
        </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  // container: {
  //   flex: 1,
  //   backgroundColor: '#fff',
  // },
  liBtm : {
    marginBottom: 15
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 200,
    flexGrow:1,
    backgroundColor: '#f9f9f9',
    zIndex: -15,
    marginVertical: isWeb? 0:60,
  },
  checkpin: {
    marginTop: 0,
  },
  termsTxt :{
    fontSize:theme.size16,
    color: theme.darkbg,
    fontFamily: 'MontserratRegular',
    textAlign:'left',
  },
  imgWrap:{
    height:200
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 0,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
    })
  ),
  minWidth(
    992,
    maxWidth(1199, {
      pageArea: {
        paddingHorizontal: 100,
      },
    })
  )
);
