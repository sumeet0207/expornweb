import React, { useState, useEffect } from "react";
import { StyleSheet, FlatList, Text, View, ScrollView,Button,SafeAreaView,Dimensions,Platform } from 'react-native';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import AccessoriesBox from './atom/AccessoriesBox';
import { Row, Col } from 'react-native-responsive-grid-system';
const { height, width } = Dimensions.get('window');
import {useParams,useHistory } from '../Route/routing';
import moment from 'moment';
import { PostRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import { setAsyncStorageFunction,getAsyncStorageFunction,flushAsyncStorageFunction } from "./services/CookieModule";
import { useShow } from "./services/AlertHook";
import CloseModal from './atom/CloseModal';
import SubmitButton from '../Components/atom/SubmitButton';


const isWeb = Platform.OS === 'web';

export default function AccessoriesList(props) {
  const { showw, error, success } = useShow();
  const [selectAcc, setSelectedAcc] = useState([]);
  const {deliverDate,Weight,slot,mszcake,isHShape,isEggless,weightPrice,deliverySlot}=props;
  let { cakeurl } = useParams();
  let history = useHistory();
  console.log(props.allaccessories)

  function removeFromAccessArray(id)
  {
    console.log(selectAcc)
    let rmfilteredArray = selectAcc.filter(function (itm) {
      return itm.accessory_id !== id
    });

    console.log(rmfilteredArray)
    setSelectedAcc(rmfilteredArray);
  }

  function pushAccess(acceessObj)
  {
    console.log(acceessObj);
    console.log(selectAcc);
    selectAcc.push(acceessObj);
    setSelectedAcc(selectAcc);
  }

  function getWeightId(weight)
  {
    let weightPriceFilter = weightPrice.filter(function (itm) {
      return itm.weight == weight
    });
    let weight_id = weightPriceFilter[0].weight_id;
    return weight_id;
  }

  async function getFinalArray()
  {
    let weightPriceFilter = weightPrice.filter(function (itm) {
      return itm.weight == Weight
    });

    let finalAccessArray = selectAcc.filter(function (acc) {
      return acc.quantity > 0
    });

    let weight_id = weightPriceFilter[0].weight_id;
    let dDate = deliverDate?moment(deliverDate).format('L'):'';
    console.log(dDate);
    if(weight_id)
    {
      let param={
        product_id: parseInt(cakeurl),
        weight_id:parseInt(weight_id),
        egg_type_id:isEggless?2:1,
        shape_id:isHShape?2:1,
        message:mszcake,
        slot:parseInt(deliverySlot),
        deliver_date:dDate,
        accessories:finalAccessArray
      }
      console.log(param)
      //return false;
      
      let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
      console.log(mixdelight_token)
      let header = { Token: mixdelight_token };
      let addtocartUrl = getApiurl("addtocart");
      let addedToCart = await PostRequestFunction(addtocartUrl,header, param);
      
      if(addedToCart.status)
      {
        success('Added to cart')
        history.push("/cart");
      }else
      {
        console.log(addedToCart.message)
        error(addedToCart.message)
        props.setIsOpen(false);
      }
    }else
    {
       error('Weight needed')
       console.log("weight needed");
       props.setIsOpen(false);
    }
    
    
  }

  function close()
  {
    console.log('close')
  }

  return (
    <View>
      <ScrollView>
        <View style={{ paddingTop: 20, paddingBottom: 20, marginBottom: 20, flex:1 }}>
          <CloseModal  onPress={()=>props.setIsOpen(false)}/>
          <View style={{ alignItems: 'center' }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginBottom: 5, flexWrap:'wrap', paddingHorizontal:30 }}>
              Add on something to make it extra special!
            </Text>
          </View> 
          
          <View style={styles.container}>
            <FlatList
              data={props.allaccessories}
              scrollEnabled={true}
              initialNumToRender={20}
              numColumns={width>768?6:2}
              keyExtractor={item => item.id}
              renderItem={({ item, index, separators }) => (
                <Col xs={6} sm={6} md={4} lg={2} xl={2}>
                    <AccessoriesBox accessBoxdata={item}
                    setSelectedAcc={setSelectedAcc}
                    pushAccess={pushAccess} selectAcc={selectAcc}
                    removeFromAccessArray={removeFromAccessArray}
                    />
                   
                </Col>
              )}
          /> 
          </View>

          
            <View style={styles.btmFixedBtn}>
              <SubmitButton onPress={getFinalArray}  btnTxt="continue"/>
            </View> 
          {/* <Button onPress={getFinalArray} title="Open" color="#841584"/> */}
          


          {/* <Row>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox 
               checkid={1}
               setSelectedAcc={setSelectedAcc}
               selectAcc={selectAcc}
               />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox
               setSelectedAcc={setSelectedAcc}
               selectAcc={selectAcc}
               checkid={2}
               />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox setSelectedAcc={setSelectedAcc} selectAcc={selectAcc} checkid={3}/>
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox setSelectedAcc={setSelectedAcc} selectAcc={selectAcc} checkid={4}/>
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox setSelectedAcc={setSelectedAcc} selectAcc={selectAcc} checkid={4}/>
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Col xs={6} sm={6} md={4} lg={2}>
              <AccessoriesBox />
            </Col>
            <Button onPress={getFinalArray} title="Open" color="#841584"/>
          </Row> */}

          {/* <FlatList
            data={data}
            initialNumToRender={5}
            numColumns={6}
            renderItem={({ item, index, separators }) => (
                
                <Col xs={6} sm={6} md={4} lg={2}>
                <AccessoriesBox/>
                </Col>
               
            )}
            /> */}
        </View>

        {/* <View style={styles.btmFixedBtn}>
              <SubmitButton onPress={getFinalArray}  btnTxt="continue"/>
            </View>  */}

        
      </ScrollView>
      {/* <FlatListMolecule /> */}
    </View>
  );
}


const base = {

  container:{
    marginBottom:20
  },

btmFixedBtn:{
  // position: isWeb? 'sticky':'absolute',
  bottom: 0,
  width:'100%',
  // display: 'flex',
},


}

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {})));
