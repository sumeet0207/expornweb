import React, { useState, useEffect } from "react";
import { StyleSheet, Text, View, ScrollView, Dimensions,FlatList, SafeAreaView,BackHandler,Platform } from 'react-native';
import { Link, useLocation,useHistory } from '../Route/routing';

import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import OrderMolecule from './molecule/OrderMolecule';
import TitleText from './atom/TitleText';
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import { GetRequestFunction } from "../api/ApiHelper";
import { getApiurl } from "../api/ApiKeys";
import { getAsyncStorageFunction } from "./services/CookieModule";
import NotFound from '../Components/NotFound';

const isWeb = Platform.OS === 'web';
const { width, height } = Dimensions.get('window');

export default function Order() {
  let location = useLocation();
  //console.log(location.pathname);
  let history  = useHistory();
  const [orderInfo, setOrders] = useState([]);
  const [isLogin, setIsLogin] = useState(false);
  const [orderCount, setOrdersCount] = useState(0);
 
  useEffect(() => {
    getOrderDetails();
    handleBackHnadler();
   
  },[]);

  function handleBackHnadler()
  {
    const backAction = () => {
      history.goBack();
      return true;
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );
    return () => backHandler.remove();
  }

  async function getOrderDetails()
  {
    let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
    //console.log(mixdelight_token)
    if(mixdelight_token)
    {
      setIsLogin(true);
      let header = { Token: mixdelight_token };
      let orderUrl = getApiurl("getOrders");
      let orderList = await GetRequestFunction(orderUrl,header, {});
      console.log(orderList)
      if(orderList.status && orderList.count > 0)
      {
          setOrdersCount(orderList.count);
          let data = orderList.data;
          setOrders(data);
      }else
      {
         console.log("no data")
      }
    }else
    {
      history.push('/')
    }
  }

  if (isLogin && orderCount < 1) {
    return(
        <View style={{height:'100%'}}>
          <NotFound heading={'Your Order List is empty !'}
            imgsrc={'https://www.onlinetestsindia.com/assets/images/empty_cart.jpg'}
            passurl={'/'}/>
        </View>

    )
  }

  return (
    <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
        <ScrollView>
          <View style={{ paddingHorizontal: 10, flexGrow:1 }}>
          <View style={styles.orderHeading}>
            <TitleText
              title="Your Orders"
              titleSize={theme.size22}
              mb_titleSize={theme.size18}
              titleAlign={'left'}
              txtColor={theme.ogBlack}
            />
            </View>
            <Row>
              <Col xs={12} sm={12} md={12} lg={12} xl={12}>
                <FlatList
                  data={orderInfo}
                  scrollEnabled={true}
                  initialNumToRender={5}
                  numColumns={1}
                  keyExtractor={item => item.id}
                  renderItem={({ item, index, separators }) => (
                    <OrderMolecule orderinfo={item}/>
                  )}
                /> 
              </Col>
            </Row>
          </View>
        </ScrollView>
      </View>
      <Footer />
      <MobileFooter />
      </View> 
  );
}

const base = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  mainImg: {},
  availFlex: {
    flex: 1,
    paddingRight: 12,
  },

  directionFlex: {
    flex: 0.5,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  leftWrap: {
    paddingLeft: 50,
    paddingRight: 10,
  },
  rightWrap: {
    paddingRight: 50,
    paddingLeft: 10,
  },
  borderBotm100: {
    borderColor: '#ddd',
    borderBottomWidth: 1.333,
    borderStyle: 'solid',
    width: '100%',
    marginBottom: 10,
  },
  pageArea: {
    paddingHorizontal: 200,
    flexGrow:1,
    zIndex: -15,
    marginVertical: isWeb? 0:80,
  },
  checkpin: {
    marginTop: 0,
  },
  orderHeading:{
    marginVertical:10,
  }
};

const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 0,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 0,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 0,
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      leftWrap: {
        paddingLeft: 0,
        paddingRight: 10,
      },
      rightWrap: {
        paddingRight: 0,
        paddingLeft: 10,
      },
      orderHeading:{
        marginVertical:2,
      }
    })
  ),
  minWidth(
    992,
    maxWidth(1199, {
      pageArea: {
        paddingHorizontal: 100,
      },
    })
  )
);
