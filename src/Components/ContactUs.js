import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  Platform,
  BackHandler
} from "react-native";
import { createStyles, minWidth, maxWidth } from 'react-native-media-queries';
import { theme } from '../Theme/theme';
import { Row, Col } from 'react-native-responsive-grid-system';
import { useForm, Controller } from 'react-hook-form';
import { getApiurl } from "../api/ApiKeys";
import { getAsyncStorageFunction } from "./services/CookieModule";
import { CheckValidPincode } from "./services/CommonModule";
import {useParams,useHistory } from '../Route/routing';
import { PostRequestFunction } from "../api/ApiHelper";
import { useShow } from "./services/AlertHook";
import MobileHeader from './molecule/MobileHeader';
import MobileFooter from './molecule/MobileFooter';
import Header from './molecule/Header';
import Footer from './molecule/Footer';
import TitleText from '../Components/atom/TitleText';
import { Feather } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

const isWeb = Platform.OS === 'web';
const { height, width } = Dimensions.get('window');

const themePage = {
   smallFont:10,
   largeFont:12,
   checkboxHW:13,
   backColor:'white',
   primaryFontColor:"white",
   primaryGrey: '#A5A7AB',
   primaryBlack:"black",
   primaryRed:"#FF1E1E",
}

export default function ContactUs(){
    const { show, error, success } = useShow();
    let history = useHistory();

    useEffect(() => {
        handleBackHnadler();
    },[]);

    function handleBackHnadler()
    {
        const backAction = () => {
        history.goBack();
        return true;
        };

        const backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        backAction
        );
        return () => backHandler.remove();
    }

    const [pinError, setPincodeError] = useState('Currently we only deliver in Thane area better to check pincode');
        const { register, setValue, handleSubmit, control, errors } = useForm({
            defaultValues: {
                name:"",
                number:"",
                email:"",      
              }
        });
        

        const onSubmit = async(data) => 
        {
                let header = {};
                let param = data;
                let addQueryUrl = getApiurl("addQuery");
                let qyerydata = await PostRequestFunction(addQueryUrl,header,param);
                if(qyerydata.status)
                {
                    success('Query added successfully');
                    history.push('/contactus')
                }else{
                    error(qyerydata.message);
                    
                }
        };

        return ( 
      <View style={{height:'100%'}}>
      <MobileHeader />
      <Header />
      <View style={styles.pageArea}>
            <ScrollView style={styles.SafeAreaViewstyle}>
            <View>
              <Row>
              <Col xs={12} sm={12} md={6} lg={6} xl={6}>
              <View style={styles.formView}>
                    <View>
                        <TouchableOpacity
                            style={styles.otpButtonContainer}
                            onPress={null}
                        >
                        <TitleText
                        title="Contact Us"
                        titleSize={theme.size30}
                        mb_titleSize={theme.size20}
                        titleAlign={'left'}
                        titleFont={'MontserratSemiBold'}
                        txtColor={theme.ogBlack}
                      />
                        </TouchableOpacity>

                    </View>
                    <View style={[styles.formViewBox,styles]}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                        <TextInput style={[styles.input,styles.borderWidthStyle]}
                        onBlur={onBlur}
                        placeholder="Full Name"
                            onChangeText={(value) => onChange(value)} value={value}/>
                        )}
                        name="name"
                        defaultValue=""
                        rules={{ required: true }}
                        />
                        {errors.name && <Text style={styles.errorText}>This is required.</Text>}
                       

                        <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                            <TextInput style={[styles.input,styles.borderWidthStyle]} onBlur={onBlur}
                            placeholder="Email Address"
                                onChangeText={(value) => onChange(value)} value={value}/>
                            )}
                            name="email"
                            rules={{ required: "This is required.",
                            validate: {
                                pattern: (value) => value && /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value) || 'The Email must be a valid email address',
                            }}
                        }
                        /> 
                        {errors.email && <Text style={styles.errorText}>{errors.email.message}</Text>}

                        <Controller
                            control={control}
                            render={({ onChange, onBlur, value }) => (
                              <TextInput
                              style={{
                                borderColor: theme.grey2,
                                borderWidth: 1,
                                width: null,
                                borderRadius: 5,
                                padding: 5,
                                backgroundColor:theme.white
                              }}
                              multiline
                              numberOfLines={4}
                              editable
                              minLength={10}
                              maxLength={400}
                              placeholder="Your Message/Query"
                                onChangeText={(value) => onChange(value)} value={value}/>
                            )}
                            name="query"
                            rules={{ required: "This is required."}} 
                        /> 
                        {errors.query && <Text style={styles.errorText}>{errors.query.message}</Text>}
                         
                    </View>
                    <View style={{paddingHorizontal:15}}>
                    <TouchableOpacity style={[styles.cabinetButton,styles.cabinetBack]}
                        onPress={handleSubmit(onSubmit)}>
                        <Text style={[styles.cabinetButtonText,styles.callnowtext]}>
                          SUBMIT
                        </Text>
                    </TouchableOpacity>
                    </View>
                </View>
              </Col>

                <Col xs={12} sm={12} md={6} lg={5} xl={5}>
                  <View syle={{marginBottom:50}}>
                    <View style={styles.contactHeading}>
                      <TitleText
                        title="Other ways to connect"
                        titleSize={theme.size22}
                        mb_titleSize={theme.size18}
                        titleAlign={'left'}
                        titleFont={'MontserratSemiBold'}
                        txtColor={theme.ogBlack}
                      />
                    </View>
                    <View style={styles.iconwrap}>
                    <Feather name="mail"
                      style={styles.contIcon}
                      size={24}
                    />
                    <Text style={styles.contText}>mix.delightt@gmail.com</Text>
                    </View>

                    <View style={[styles.iconwrap]}>
                    <AntDesign name="phone"
                      style={styles.contIcon}
                      size={24}
                    />
                    <Text style={styles.contText}>+91-7710833814</Text>
                    </View>

                    <View style={[styles.iconwrap]}>
                    <Feather name="home"
                      style={styles.contIcon}
                      size={24}
                    />
                    <Text style={styles.contText}>302, siddhi yogeshwar krupa bld Lokmanya Nagar, Thane 400606</Text>
                    </View>
                    

                  </View>
                </Col>
              </Row>
            </View>
              
            </ScrollView>
            </View>
      <Footer />
      <MobileFooter />
      </View> 
        );
   
}

const base = {

  iconwrap:{
    flexDirection:'row',
    alignItems:'center',
    paddingVertical:15
  },
    contIcon:{
      color: theme.orange,
    },
    contText:{
      fontSize:theme.size16,
      color: theme.ogBlack,
      fontFamily: 'MontserratRegular',
      textAlign:'center',
      paddingHorizontal: 10
    },
    viewWidth:{
        width:300
    },
    formView:{
      paddingLeft:15,
      paddingRight:15,
      marginBottom:45,   
      marginHorizontal:50,
      elevation: isWeb? 8:1,
      shadowOffset: { width: 1, height: 1 },
      shadowOpacity: 0.5,
      shadowRadius: 5,
      borderRadius:3,
      paddingVertical: 20
    },
    borderWidthStyle:{
        borderWidth:1,
        borderStyle:'solid',
        borderColor:themePage.primaryGrey
    },
    formViewBox:{
        padding:12,
        marginBottom:10,
    },
    SafeAreaViewstyle:{
        padding:10,
        marginTop:20,
        backgroundColor : themePage.backColor,
    },
    input: {
        borderRadius: 4,
        height:40,
        marginBottom:10,
        paddingLeft:10
      },
    pickerBox: {
        paddingTop:0,
        borderRadius: 4,
        height:50,
        marginBottom:10,
    },      
    otpButtonContainer: {
        backgroundColor: theme.white,
        height:45,
        paddingLeft:15
    },
    HeadingTxt:{
        marginLeft:15,
        top:12,
        fontSize:14,
        color:theme.ogBlack,
        fontWeight: "bold"
    },
    instrHeading:{
        marginTop:5,
        fontWeight:"bold",
        color:themePage.primaryBlack,  
        fontSize:15
    },
    instrDesc:{
        fontSize:15,
        color:'red',

    },
    cabinetButton:{
        height:45,
        width:"100%",
        paddingVertical: 12,
        paddingLeft:15,
     },
      cabinetButtonText: {
      fontSize: 15,
      fontWeight:"bold",
      justifyContent:"center",
      color:"white"
    },
    cabinetBack:{
        backgroundColor: theme.orange,
      },
    errorText:{
        color:themePage.primaryRed,
        marginBottom:5
    },
    pageArea: {
        flexGrow:1,
        zIndex: -15,
        marginVertical: isWeb? 0:60,
    }, 
};



const styles = createStyles(
  base,
  minWidth(
    100,
    maxWidth(575, {
      pageArea: {
        paddingHorizontal: 15,
      },
      formView:{
        marginHorizontal:0
      },
      iconwrap:{
        paddingVertical:7
      },
    })
  ),
  minWidth(
    576,
    maxWidth(991, {
      pageArea: {
        paddingHorizontal: 30,
      },
      formView:{
        marginHorizontal:0
      },
      iconwrap:{
        paddingVertical:7
      },
    })
  )
);