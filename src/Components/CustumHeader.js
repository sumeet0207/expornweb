import React from 'react';
import {View, Text, StyleSheet, Button, Platform} from 'react-native';
import {Link} from '@react-navigation/web';
const isWeb = Platform.OS === 'web';

export default function custumHeader(){
   return(
     <View style={styles.header}>
         <View>
             <Text style={styles.headerText}>custum Header Component</Text>
         </View>
     </View>

   )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header:{
        width:'100%',
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        backgroundColor:'orange'
    },
    headerText:{
        fontWeight:'bold',
        fontSize:20,
        color:'black'

    }
})