// import React, { useState, useEffect } from "react";
// import { Route, Redirect } from '../Route/routing';
// import { getAsyncStorageFunction } from "../Components/services/CookieModule";


// var IsLogin = false;
// // let abc = getAsyncStorageFunction("mixdelight_token");
// // abc.then(function(x)
// // {
// //     IsLogin = true;
// // })

// export const PrivateRoute = ({isAuthenticated=IsLogin, component: Component,...rest}) => {
   
//     // const [islog, setIsLog] = useState(true);
//     // var isAuthenticated = false;
//     useEffect(() => {
//         checkLogin();
       
//       },[]);

//     async function checkLogin()
//     {
//         let mixdelight_token = await getAsyncStorageFunction("mixdelight_token");
//         if(mixdelight_token)
//         {
//             IsLogin= true;
//             setIsLog(true);
//             // isAuthenticated = true;
//         }
//     }  

//     return <Route {...rest} component={(props) => (
//         isAuthenticated ? (
//          <Component/>
//         ): (
//             <Redirect to="/login" />
//         )
//     )} />
// }

// export default PrivateRoute;

import React, { useState, useEffect } from "react";
import { Route, Redirect } from '../Route/routing';



export const PrivateRoute = ({isAuthenticated=true, component: Component,...rest}) => {

    return <Route {...rest} component={(props) => (
        isAuthenticated ? (
         <Component/>
        ): (
            <Redirect to="/login" />
        )
    )} />
}

export default PrivateRoute;

