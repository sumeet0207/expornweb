import React from 'react';
import { Route, Redirect } from '../Route/routing';

export const PublicRoute = ({isAuthenticated=false, component: Component,...rest}) => (
    <Route {...rest} component={(props) => (
        isAuthenticated ? (
            <Redirect to="/" />
        ): (
            <Component/>
        )
    )} />
)

export default PublicRoute;
