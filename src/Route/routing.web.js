export {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useParams,
    useLocation,
    Redirect,
    useHistory,
    withRouter 
  } from 'react-router-dom';