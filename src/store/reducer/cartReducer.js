import { SETCARTQUANTITY } from "../types";

let countObj = { cartQuantity: 0 };
const initialState = countObj;

export default function (state = initialState, action) {
  switch (action.type) {
    case SETCARTQUANTITY:
      return action.payload;
    default:
      return state;
  }
}
