import { SETSORTING } from "../types";

const initialState = { sortOpt: '' };

export default function (state = initialState, action) {


    switch (action.type) {
        case SETSORTING:
            return action.payload;
        default:
            return state;
    }

}