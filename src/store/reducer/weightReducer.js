import { SETWEIGHTFILTER } from "../types";

const initialState = { weights : [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case SETWEIGHTFILTER:
      return action.payload;
    default:
      return state;
  }
}