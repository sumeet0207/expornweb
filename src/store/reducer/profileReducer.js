import { SETNAME } from "../types";

let basicInfo = { fname: "", lname: "" };
const initialState = basicInfo;

export default function (state = initialState, action) {
  switch (action.type) {
    case SETNAME:
      return action.payload;
    default:
      return state;
  }
}
