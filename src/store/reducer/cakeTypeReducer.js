import { SETTYPESFILTER } from "../types";

const initialState = { types: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case SETTYPESFILTER:
      return action.payload;
    default:
      return state;
  }
}