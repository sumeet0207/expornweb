import { SETFLAVOURFILTER } from "../types";

const initialState = { flavoured: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case SETFLAVOURFILTER:
      return action.payload;
    default:
      return state;
  }
}