import { ADDALERT, REMOVEALERT } from "../types";

const initialState = { isAlert: false, text: '', options: { type: '' } };

export default function (state = initialState, action) {


    switch (action.type) {
        case ADDALERT:
            return action.payload;
        case REMOVEALERT:
            return {}
        default:
            return state;
    }

}