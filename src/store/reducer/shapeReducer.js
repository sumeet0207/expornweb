import { SETSHAPEFILTER } from "../types";

const initialState = { shaped: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case SETSHAPEFILTER:
      return action.payload;
    default:
      return state;
  }
}