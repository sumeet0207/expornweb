import { combineReducers } from "redux";
import LoginOverlapReducer from "./LoginOverlapReducer";
import cartReducer from "./cartReducer";
import profileReducer from "./profileReducer";
import alertReducer from "./alertReducer";
import flavourReducer from "./flavourReducer";
import shapeReducer from "./shapeReducer";
import vendorReducer from "./vendorReducer";
import weightReducer from "./weightReducer";
import sortReducer from "./sortReducer";
import cakeTypeReducer from "./cakeTypeReducer";



const rootReducer = combineReducers({
    'LoginOverlapReducer' : LoginOverlapReducer,
    'cartReducer' : cartReducer,
    'profileReducer' : profileReducer,
    'alertReducer' : alertReducer,
    'flavourReducer' : flavourReducer,
    'shapeReducer' : shapeReducer,
    'vendorReducer' : vendorReducer,
    'weightReducer' : weightReducer,
    'sortReducer' : sortReducer,
    'cakeTypeReducer' : cakeTypeReducer,

});

export default rootReducer;

