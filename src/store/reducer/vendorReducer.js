import { SETVENDORFILTER } from "../types";

const initialState = { vendors: [] };

export default function (state = initialState, action) {
  switch (action.type) {
    case SETVENDORFILTER:
      return action.payload;
    default:
      return state;
  }
}