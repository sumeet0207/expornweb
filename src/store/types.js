export const INCREMENT = "INCREMENT";
export const DECREMENT = "DECREMENT";
export const RESET = "RESET";
export const SET = "SET";
export const GET = "GET";
export const TYPE = "TYPE";
export const LOGINFLAG = "LOGINFLAG";
export const SETCARTQUANTITY = "SETCARTQUANTITY";
export const SETNAME = "SETNAME";
export const CLEANALERT = "CLEANALERT";
export const ADDALERT = "ADDALERT";
export const REMOVEALERT = "REMOVEALERT";

export const SETFLAVOURFILTER = "SETFLAVOURFILTER";
export const SETSHAPEFILTER = "SETSHAPEFILTER";
export const SETVENDORFILTER = "SETVENDORFILTER";
export const SETWEIGHTFILTER = "SETWEIGHTFILTER";

export const SETSORTING = "SETSORTING";
export const SETTYPESFILTER = "SETTYPESFILTER";




