import { SETNAME } from "../types";

export function setProfileName(nameObject) {
  return function (dispatch) {
    dispatch({ type: SETNAME, payload: { ...nameObject } });
  };
}
