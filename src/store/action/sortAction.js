import { SETSORTING } from "../types";

export function setSortingOption(selectsort) {
    return function (dispatch) {
        dispatch({ type: SETSORTING, payload: { sortOpt: selectsort } });
    };
}