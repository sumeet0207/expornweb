import { SETFLAVOURFILTER } from "../types";

export function setToFlavourFilter(flavourArray) {
  return function (dispatch) {
    dispatch({ type: SETFLAVOURFILTER, payload: { flavoured: flavourArray } });
    
  };
}
