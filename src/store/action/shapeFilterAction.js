import { SETSHAPEFILTER } from "../types";

export function setToShapeFilter(shapeArray) {
  return function (dispatch) {
    dispatch({ type: SETSHAPEFILTER, payload: { shaped: shapeArray } });
    
  };
}