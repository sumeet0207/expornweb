import { CLEANALERT, ADDALERT, REMOVEALERT } from "../types";

export function addAlert(OBJ) {
    return function (dispatch) {
        dispatch({ type: ADDALERT, payload: OBJ });
    };
}

export function removeAlert() {
    return function (dispatch) {
        dispatch({ type: REMOVEALERT });
    };
}

export function cleanAlert() {
    return function (dispatch) {
        dispatch({ type: CLEANALERT });
    };
}

