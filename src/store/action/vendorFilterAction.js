import { SETVENDORFILTER } from "../types";

export function setToVendorFilter(vendorArray) {
  return function (dispatch) {
    dispatch({ type: SETVENDORFILTER, payload: { vendors: vendorArray } });
    
  };
}