import { SETWEIGHTFILTER } from "../types";

export function setToWeightFilter(weightArray) {
  return function (dispatch) {
    dispatch({ type: SETWEIGHTFILTER, payload: { weights: weightArray } });
    
  };
}