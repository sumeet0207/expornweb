import { SETCARTQUANTITY } from "../types";

export function setCartCount(quantity) {
  return function (dispatch) {
    dispatch({ type: SETCARTQUANTITY, payload: { cartQuantity: quantity } });
  };
}
