import { SETTYPESFILTER } from "../types";

export function setToTypeFilter(typeArray) {
  return function (dispatch) {
    dispatch({ type: SETTYPESFILTER, payload: { types: typeArray } });
    
  };
}