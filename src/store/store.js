import { createStore, applyMiddleware } from "redux";
//import AsyncStorage from '@react-native-community/async-storage';
import AsyncStorage from '@callstack/async-storage';
import { persistStore, persistReducer } from "redux-persist";
import rootReducer from "./reducer/index";
import thunk from "redux-thunk";


// import { ThemeContextProvider } from './theme/themeProvider';


const persistConfig = {
  key: "mixDelightrRoot",
  storage: AsyncStorage,
  whitelist: ["LoginOverlapReducer","cartReducer","profileReducer",
  "flavourReducer","shapeReducer","vendorReducer","weightReducer",
  "sortReducer","cakeTypeReducer"]
};
const persistedReduce = persistReducer(persistConfig, rootReducer);
const store = createStore(persistedReduce, applyMiddleware(thunk));
const persistor = persistStore(store);

export { store, persistor }
