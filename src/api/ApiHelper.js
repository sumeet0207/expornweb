import axios from 'axios';




export async function GetRequestFunction(apiurl, header = {}, parameter = {}) {
	const config = {
		method: 'get',
		url: apiurl,
		params: parameter,
		headers: header
	}

	try {
		const data = await axios(config)
		let isValid = await checkResponse(data?.data?.message)

		return data?.data;
	} catch (error) {
		let response = error && error.response
		let isValid = await checkResponse(response?.data?.message)

		let data = response && response?.data || [];

		return data;
	}
}

export async function PostRequestFunction(apiurl, header = {}, data = {}) {
	try {
		const getdata = await axios.post(apiurl, data, {
			headers: header,
		});
		let isValid = await checkResponse(getdata?.data?.message)

		return getdata?.data;

	} catch (error) {

		let isValid = await checkResponse(error?.response?.message)
		let response = error && error?.response
		let data = response && response?.data || [];
		return data;
	}
}

export async function DeleteRequestFunction(apiurl, header = {}, data = {}) {
	try {
		const deletdata = await axios.delete(apiurl, { headers: header }, { params: data });
		let isValid = await checkResponse(deletdata.data.message)

		return deletdata.data;

	} catch (error) {

		let response = error && error.response
		let data = response && response.data || [];
		return data;
	}
}


const checkResponse = (text) => {
	let arr = ["Unauthorized access,Invalid token"]

	if (arr.includes(text)) {
		//clearLogout();
		//let abc = clearLogout();
		console.log('logout here')
	}
	else
		return true
}

// export async function DeleteRequestFunction(apiurl, header = {}, data = {}) {
//   try {
//     const deletdata = await axios.delete(
//       apiurl,
//       { params: data },
//       {
//         headers: header,
//       }
//     );
//     return deletdata.data;
//   } catch (error) {
//     let response = error && error.response;
//     let data = (response && response.data) || [];
//     return data;
//   }
// }
