export function getApiurl(apikey) {
  //const PATH = 'http://localhost/mix_delight_api';
  //const PATH = 'http://cake-env.eba-skppivzp.ap-south-1.elasticbeanstalk.com';
  const PATH = 'https://api.mixdelight.com';
  var allapiurl = {
    googleLogin: `${PATH}/user/google_login`,
    facebookLogin: `${PATH}/user/fb_login`,
    getallproduct: `${PATH}/product`, //use
    getallcategory: `${PATH}/category`,
    getallhomedetails: `${PATH}/category/home`,
    createUser: `${PATH}/user/create`,//use
    loginWithEmail: `${PATH}/user/login`,
    productDetails: `${PATH}/product/detail`, //use
    getlistOfAccessories : `${PATH}/product/getlistOfAccessories`, //use
    loginBasicInfo: `${PATH}/user/info`,
    forgetPassword: `${PATH}/user/forgot_password`,
    submitotp: `${PATH}/user/submitotp`,
    createPassword: `${PATH}/user/createpassword`,
    addShipping: `${PATH}/user/add_shipping`,//use 
    getShipping: `${PATH}/user/get_shipping`, //use
    updateShipping: `${PATH}/user/update_shipping`,//use
    deleteShipping: `${PATH}/user/delete_shipping`,
    addtocart: `${PATH}/cart/add`,//use
    updateCart: `${PATH}/cart/update`,
    cartDelete: `${PATH}/cart/delete`,
    getCartDetails: `${PATH}/cart`,
    updateAccessoriesCart: `${PATH}/cart/updateAccessoriesCart`,//use
    itemdelete: `${PATH}/cart/itemdelete`,//use
    loginBasicInfo: `${PATH}/user/info`,
    updateUserProfile: `${PATH}/user/update`,
    changePassword: `${PATH}/user/update_password`,
    getdiyproductdetail: `${PATH}/diy/detail`,
    orderAdd: `${PATH}/order/add`,
    orderCapture: `${PATH}/order/capture`,
    orderAbort: `${PATH}/order/abort`,
    cancelOrder: `${PATH}/order/cancelOrder`,
    getOrders: `${PATH}/order`,
    getPromoCodeDiscount: `${PATH}/order/getPromoCodeDiscount`,
    logout: `${PATH}/user/logout`,
    contactUs: `${PATH}/cms/contactus`,
    enquiry: `${PATH}/enquiry/add`,
    getFAQ: `${PATH}/faq`,
    getCollectionDetails: `${PATH}/Collection/detail`,
    getAboutUsDetails: `${PATH}/Cms/aboutus`,
    subscribeWebsite: `${PATH}/cms/add_subscriber`,
    getstories: `${PATH}/cms/stories`,
    getPayPalOrderCapture: `${PATH}/order/capture_paypal`,
    returnOrder: `${PATH}/returnExchange/addTempExcRetOrder`,
    orderInvoice: `${PATH}/invoice/view`,
    pincodeValidation: `${PATH}/order/callPincodeValidateApi`,
    getInstantDeliverProduct: `${PATH}/product/getInstantDeliverProduct`,
    getCartAmount: `${PATH}/cart/getCartAmount`,
    getFilterData: `${PATH}/product/getFilterData`,
    getOrderInfo: `${PATH}/order/getOrderInfo`,
    addQuery: `${PATH}/user/addQuery`,
    checkWithRazor: `${PATH}/order/checkWithRazor`,

  };

  let requestedapiurl = allapiurl[apikey];
  return requestedapiurl;
}


