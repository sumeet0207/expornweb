import { StyleSheet } from 'react-native';

export const theme = {
  maincolor: 'red',
  primaryColor: '#009bff',
  primaryColorLight: '#b1e1ff',
  backgroundColor: '#fff',

  black: '#293241',
  black03: '#2932414d',
  darkbg: '#1b222b',

  grey: '#969faa',
  darkgrey: '#636363',
  lightgrey: '#eef0f3',
  lightergrey: '#f2f2f2',
  grey2: '#a8a5a5',
  Hash666: '#666666',

  white: '#fff',
  ogBlack: '#000',
  yellow: '#ffbc00',
  red: '#ff1e1e',
  lightred: '#facccc',

  green: '#f77800',
  lightGreen: '#0f8f00d9',
  lighterGreen: '#cfe9cc',
  extraLightGreen: '#dfffdb',
  lightGreenBg: '#f1fff0',
  lightGreenBorder: '#b8dbb6',
  orange: '#ea123a',
  blue: '#007bff',

  // mbHFColor: '#f0e68c',
  HFColorText: '#f77800',
  themepink: '#ed143d',
  mbHFColor: '#f77800',

  error: '#ff0000',
  success: '#000',
  // BebasNeueRegular: 'BebasNeue-Regular',
  // MontserratRegular: 'Montserrat-Regular',
  // MontserratSemiBold: 'Montserrat-SemiBold',
  // MontserratBold: 'Montserrat-Bold',
  // MontserratMedium: 'Montserrat-Medium',
  // PoppinsBold: 'Poppins-Bold',
  size5: 5,
  size9: 9,
  size8: 8,
  size10: 10,
  size11: 11,
  size12: 12,
  size13: 13,
  size14: 14,
  size15: 15,
  size16: 16,
  size17: 17,
  size18: 18,
  size20: 20,
  size22: 22,
  size23: 23,
  size24: 24,
  size25: 25,
  size28: 28,
  size30: 30,
  size36: 36,
};

export const setFontStyle = (
  fontSize,
  fontFamily = 'MontserratRegular',
  color = Color.black
) => {
  const fontStyleObject = StyleSheet.create({
    fontStyle: {
      fontSize,
      fontFamily,
      color,
    },
  });
  return fontStyleObject.fontStyle;
};
