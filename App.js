import React, { useState } from 'react';
import HybridApp from './src/App';
import { useFonts } from 'expo-font';
import { AppLoading } from 'expo';

export default function App() {

  
  let [fontsLoaded] = useFonts({
    Dancing: require('./assets/fonts/DancingScript-basic.ttf'),
    BebasNeueRegular: require('./assets/fonts/BebasNeue-Regular.ttf'),
    MontserratRegular: require('./assets/fonts/Poppins-Regular.ttf'),
    MontserratSemiBold: require('./assets/fonts/Poppins-SemiBold.ttf'),
    MontserratBold: require('./assets/fonts/Poppins-Bold.ttf'),
    MontserratMedium: require('./assets/fonts/Poppins-Medium.ttf'),
    YellowMagician: require('./assets/fonts/YellowMagician.ttf'),
  });
  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return <HybridApp />;
  }
}
